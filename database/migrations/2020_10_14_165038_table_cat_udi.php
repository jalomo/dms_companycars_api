<?php

use App\Models\Financiamientos\CatUdiModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatUdi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatUdiModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatUdiModel::ID);
            $table->string(CatUdiModel::UDI);
            $table->float(CatUdiModel::VALOR);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatUdiModel::getTableName());
    }
}
