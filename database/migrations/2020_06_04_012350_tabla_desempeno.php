<?php

use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDesempeno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DesempenoModel::getTableName(), function (Blueprint $table) {
            $table->increments(DesempenoModel::ID);
            $table->string(DesempenoModel::MOTORES)->nullable();
            $table->string(DesempenoModel::CLUTCH)->nullable();
            $table->string(DesempenoModel::CAJA_CAMBIOS)->nullable();
            $table->string(DesempenoModel::ARBOL_TRANSMISIONES)->nullable();
            $table->string(DesempenoModel::DIFERENCIAL)->nullable();
            $table->string(DesempenoModel::DIFERENCIAL_TRACCION)->nullable();
            $table->string(DesempenoModel::CAJA_REDUCTORA)->nullable();
            $table->string(DesempenoModel::CAJA_TRANSFERENCIA)->nullable();
            $table->string(DesempenoModel::SISTEMA_DINAMICO)->nullable();
            $table->string(DesempenoModel::DIRECCION)->nullable();
            $table->string(DesempenoModel::SUSPENSION)->nullable();
            $table->string(DesempenoModel::TOMA_PODER)->nullable();
            $table->unsignedInteger(DesempenoModel::SALIDA_UNIDAD_ID);
            $table->foreign(DesempenoModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DesempenoModel::getTableName());
    }
}
