<?php

use App\Models\Refacciones\CatCfdiModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCatCfdi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatCfdiModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatCfdiModel::ID);
            $table->string(CatCfdiModel::CLAVE);
            $table->string(CatCfdiModel::DESCRIPCION);
            $table->boolean(CatCfdiModel::PERSONA_FISICA);
            $table->boolean(CatCfdiModel::PERSONA_MORAL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatCfdiModel::getTableName());
    }
}
