<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\VentasRealizadasModel;
use App\Models\Refacciones\CatTipoPrecioModel;

class AddRelationTipoPrecioVentas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(VentasRealizadasModel::getTableName(), function (Blueprint $table) {
            $table->unsignedInteger(VentasRealizadasModel::TIPO_PRECIO_ID);
            $table->foreign(VentasRealizadasModel::TIPO_PRECIO_ID)->references(CatTipoPrecioModel::ID)->on(CatTipoPrecioModel::getTableName());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentasRealizadasModel::getTableName());
    }
}
