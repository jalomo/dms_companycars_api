<?php

use App\Models\Facturas\EmisorFactura;
use App\Models\Facturas\Factura;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearEmisorFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EmisorFactura::getTableName(), function (Blueprint $table) {
            $table->increments(EmisorFactura::ID);
            $table->string(EmisorFactura::NOMBRE);
            $table->string(EmisorFactura::RFC);
            $table->string(EmisorFactura::REGIMEN_FISCAL);
            $table->unsignedInteger(EmisorFactura::FACTURA_ID);
            $table->foreign(EmisorFactura::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EmisorFactura::getTableName());
    }
}
