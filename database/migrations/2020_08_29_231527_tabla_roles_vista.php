<?php

use App\Models\Usuarios\MenuRolesVistasModel;
use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRolesVista extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        // 
        Schema::create(MenuRolesVistasModel::getTableName(), function (Blueprint $table) {
            $table->increments(MenuRolesVistasModel::ID);
            $table->unsignedInteger(MenuRolesVistasModel::ROL_ID);
            $table->foreign(MenuRolesVistasModel::ROL_ID)
                ->references(RolModel::ID)
                ->on(RolModel::getTableName());

            $table->unsignedInteger(MenuRolesVistasModel::SECCION_ID);
            $table->foreign(MenuRolesVistasModel::SECCION_ID)
                ->references(MenuSeccionesModel::ID)
                ->on(MenuSeccionesModel::getTableName());

            $table->unsignedInteger(MenuRolesVistasModel::SUBMENU_ID);
            $table->foreign(MenuRolesVistasModel::SUBMENU_ID)
                ->references(MenuSubmenuModel::ID)
                ->on(MenuSubmenuModel::getTableName());

            $table->unsignedInteger(MenuRolesVistasModel::VISTA_ID);
            $table->foreign(MenuRolesVistasModel::VISTA_ID)
                ->references(MenuVistasModel::ID)
                ->on(MenuVistasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MenuRolesVistasModel::getTableName());
    }
}
