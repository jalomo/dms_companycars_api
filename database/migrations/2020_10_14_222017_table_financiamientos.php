<?php

use App\Models\Financiamientos\catComisionModel;
use App\Models\Financiamientos\catCompaniaSeguroModel;
use App\Models\Financiamientos\catEstatusPlanPisoModel;
use App\Models\Financiamientos\CatFinancierasModel;
use App\Models\Financiamientos\CatPerfilFinanciamientoModel;
use App\Models\Financiamientos\CatPropuestasModel;
use App\Models\Financiamientos\catTipoSeguroModel;
use App\Models\Financiamientos\catUdiModel;
use App\Models\Financiamientos\catUsoSeguroModel;
use App\Models\Financiamientos\EstatusFinanciamientosModel;
use App\Models\Financiamientos\FinanciamientosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableFinanciamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FinanciamientosModel::getTableName(), function (Blueprint $table) {
            $table->increments(FinanciamientosModel::ID);
            $table->unsignedInteger(FinanciamientosModel::ID_ASESOR_VENTAS);
            $table->foreign(FinanciamientosModel::ID_ASESOR_VENTAS)->references(User::ID)->on(User::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_CLIENTE);
            $table->foreign(FinanciamientosModel::ID_CLIENTE)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->date(FinanciamientosModel::FECHA_BURO);
            $table->integer(FinanciamientosModel::SE_INGRESA);
            $table->unsignedInteger(FinanciamientosModel::ID_PROPUESTA);
            $table->foreign(FinanciamientosModel::ID_PROPUESTA)->references(CatPropuestasModel::ID)->on(CatPropuestasModel::getTableName());
            $table->date(FinanciamientosModel::FECHA_INGRESO);
            $table->integer(FinanciamientosModel::HAY_UNIDAD);
            $table->unsignedInteger(FinanciamientosModel::ID_FINANCIERA1);
            $table->foreign(FinanciamientosModel::ID_FINANCIERA1)->references(CatFinancierasModel::ID)->on(CatFinancierasModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_FINANCIERA2);
            $table->foreign(FinanciamientosModel::ID_FINANCIERA2)->references(CatFinancierasModel::ID)->on(CatFinancierasModel::getTableName());
            $table->integer(FinanciamientosModel::ID_UNIDAD);
            $table->integer(FinanciamientosModel::ID_CATALOGO);
            $table->float(FinanciamientosModel::PRECIO_UNIDAD);
            $table->float(FinanciamientosModel::ENGANCHE);
            $table->string(FinanciamientosModel::MF);
            $table->string(FinanciamientosModel::PLAN);
            $table->float(FinanciamientosModel::COMISION_FINANCIERA);
            $table->float(FinanciamientosModel::COMISION_ASESOR);
            $table->unsignedInteger(FinanciamientosModel::ID_ESTATUS)->nullable();
            $table->foreign(FinanciamientosModel::ID_ESTATUS)->references(EstatusFinanciamientosModel::ID)->on(EstatusFinanciamientosModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_PERFIL)->nullable();
            $table->foreign(FinanciamientosModel::ID_PERFIL)->references(CatPerfilFinanciamientoModel::ID)->on(CatPerfilFinanciamientoModel::getTableName());
            $table->integer(FinanciamientosModel::SCORE);
            $table->float(FinanciamientosModel::CUENTA_MAS_ALTA);
            $table->string(FinanciamientosModel::TIEMPO_CUENTA);
            $table->string(FinanciamientosModel::BONO);
            $table->string(FinanciamientosModel::PLAZO);
            $table->float(FinanciamientosModel::MENSUALIDAD);
            $table->float(FinanciamientosModel::EGRESO_BRUTO);
            $table->float(FinanciamientosModel::TOTAL);
            $table->float(FinanciamientosModel::TOTAL2);
            $table->float(FinanciamientosModel::INGRESO_INDEP);
            $table->float(FinanciamientosModel::INGRESO_NOMINA);
            $table->float(FinanciamientosModel::NIVEL_ENDEUDAMIENTO);
            $table->string(FinanciamientosModel::ARRAIGO_CASA);
            $table->string(FinanciamientosModel::ARRAIGO_EMPLEO);
            $table->text(FinanciamientosModel::COMENTARIO)->nullable();
            $table->float(FinanciamientosModel::MONTO_FINANCIAR);
            $table->unsignedInteger(FinanciamientosModel::ID_COMISION);
            $table->foreign(FinanciamientosModel::ID_COMISION)->references(catComisionModel::ID)->on(catComisionModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_TIPO_SEGURO);
            $table->foreign(FinanciamientosModel::ID_TIPO_SEGURO)->references(catTipoSeguroModel::ID)->on(catTipoSeguroModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_COMPANIA);
            $table->foreign(FinanciamientosModel::ID_COMPANIA)->references(catCompaniaSeguroModel::ID)->on(catCompaniaSeguroModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_UDI);
            $table->foreign(FinanciamientosModel::ID_UDI)->references(catUdiModel::ID)->on(catUdiModel::getTableName());
            $table->float(FinanciamientosModel::COMISION_AGENCIA);
            $table->integer(FinanciamientosModel::NO_POLIZA);
            $table->date(FinanciamientosModel::INICIO_VIGENCIA);
            $table->date(FinanciamientosModel::VENCIMIENTO_ANUAL);
            $table->float(FinanciamientosModel::PRIMA_TOTAL);
            $table->float(FinanciamientosModel::IVA);
            $table->float(FinanciamientosModel::EMISION_POLIZA);
            $table->float(FinanciamientosModel::PRIMA_NETA);
            $table->float(FinanciamientosModel::UDI_AGENCIA);
            $table->float(FinanciamientosModel::COMISION_ASESOR_PRIMA);
            $table->unsignedInteger(FinanciamientosModel::ID_TIPO_SEGURO_FC);
            $table->foreign(FinanciamientosModel::ID_TIPO_SEGURO_FC)->references(catTipoSeguroModel::ID)->on(catTipoSeguroModel::getTableName());
            $table->unsignedInteger(FinanciamientosModel::ID_COMPANIA_FC);
            $table->foreign(FinanciamientosModel::ID_COMPANIA_FC)->references(catCompaniaSeguroModel::ID)->on(catCompaniaSeguroModel::getTableName());
            $table->integer(FinanciamientosModel::NO_POLIZA_FC);
            $table->date(FinanciamientosModel::INICIO_VIGENCIA_FC);
            $table->date(FinanciamientosModel::VENCIMIENTO_ANUAL_FC);
            $table->float(FinanciamientosModel::PRIMA_TOTAL_FC);
            $table->float(FinanciamientosModel::IVA_FC);
            $table->float(FinanciamientosModel::EMISION_POLIZA_FC);
            $table->float(FinanciamientosModel::PRIMA_NETA_FC);
            $table->float(FinanciamientosModel::UDI_AGENCIA_FC);
            $table->float(FinanciamientosModel::COMISION_ASESOR_PRIMA_FC);
            $table->unsignedInteger(FinanciamientosModel::ID_UDI_FC);
            $table->foreign(FinanciamientosModel::ID_UDI_FC)->references(catUdiModel::ID)->on(catUdiModel::getTableName());

            $table->date(FinanciamientosModel::FECHA_CONDICIONAMIENTO);
            $table->time(FinanciamientosModel::HORA_CONDICIONAMIENTO);
            $table->date(FinanciamientosModel::FECHA_CUMPLIMIENTO);
            $table->time(FinanciamientosModel::HORA_CUMPLIMIENTO);
            $table->date(FinanciamientosModel::FECHA_APROBACION);
            $table->time(FinanciamientosModel::HORA_APROBACION);
            $table->date(FinanciamientosModel::FECHA_CONTRATO);
            $table->time(FinanciamientosModel::HORA_CONTRATO);
            $table->unsignedInteger(FinanciamientosModel::ID_ESTATUS_PISO)->nullable();
            $table->foreign(FinanciamientosModel::ID_ESTATUS_PISO)->references(catEstatusPlanPisoModel::ID)->on(catEstatusPlanPisoModel::getTableName());
            $table->date(FinanciamientosModel::FECHA_COMPRA);
            $table->time(FinanciamientosModel::HORA_COMPRA);
            $table->text(FinanciamientosModel::COMENTARIOS_COMPRA)->nullable();

            $table->date(FinanciamientosModel::FECHA_ENVIO)->nullable();
            $table->integer(FinanciamientosModel::ANIO)->nullable();
            $table->string(FinanciamientosModel::SERIE)->nullable();
            $table->unsignedInteger(FinanciamientosModel::ID_USO_SEGURO)->nullable();
            $table->foreign(FinanciamientosModel::ID_USO_SEGURO)->references(catUsoSeguroModel::ID)->on(catUsoSeguroModel::getTableName());
            $table->string(FinanciamientosModel::ACCESORIOS)->nullable();
            $table->string(FinanciamientosModel::EXTENSION_GARANTIA)->nullable();
            $table->text(FinanciamientosModel::OBSERVACIONES)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FinanciamientosModel::getTableName());
    }
}
