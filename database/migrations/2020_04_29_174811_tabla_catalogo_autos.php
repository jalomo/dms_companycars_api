<?php

use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\CategoriaCatAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCatalogoAutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatalogoAutosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatalogoAutosModel::ID);
            $table->string(CatalogoAutosModel::NOMBRE);
            $table->string(CatalogoAutosModel::CLAVE);
            $table->integer(CatalogoAutosModel::TIEMPO_LAVADO);
            $table->integer(CatalogoAutosModel::CATEGORIA_ID);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatalogoAutosModel::getTableName());
    }
}
