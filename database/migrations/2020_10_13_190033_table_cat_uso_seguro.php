<?php

use App\Models\Financiamientos\catUsoSeguro;
use App\Models\Financiamientos\CatUsoSeguroModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatUsoSeguro extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatUsoSeguroModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatUsoSeguroModel::ID);
            $table->string(CatUsoSeguroModel::USO_SEGURO);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatUsoSeguroModel::getTableName());
    }
}
