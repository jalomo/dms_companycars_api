<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\CuentasMorosasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;

class CrearTablaCuentasMorosas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CuentasMorosasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CuentasMorosasModel::ID);
            $table->unsignedInteger(CuentasMorosasModel::CUENTA_POR_COBRAR_ID);
            $table->foreign(CuentasMorosasModel::CUENTA_POR_COBRAR_ID)->references(CuentasPorCobrarModel::ID)->on(CuentasPorCobrarModel::getTableName());
            $table->unsignedInteger(CuentasMorosasModel::ABONO_ID);
            $table->unsignedInteger(CuentasMorosasModel::DIAS_MORATORIOS)->nullable();
            $table->float(CuentasMorosasModel::MONTO_MORATORIO)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CuentasMorosasModel::getTableName());
    }
}
