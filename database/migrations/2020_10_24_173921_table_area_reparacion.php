<?php

use App\Models\Logistica\CatAreaReparacionModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableAreaReparacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatAreaReparacionModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatAreaReparacionModel::ID);
            $table->string(CatAreaReparacionModel::AREA_REPARACION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatAreaReparacionModel::getTableName());
    }
}
