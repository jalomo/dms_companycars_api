<?php

use App\Models\Refacciones\MesesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoMeses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MesesModel::getTableName(), function (Blueprint $table) {
            $table->increments(MesesModel::ID);
            $table->string(MesesModel::NOMBRE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MesesModel::getTableName());
    }
}
