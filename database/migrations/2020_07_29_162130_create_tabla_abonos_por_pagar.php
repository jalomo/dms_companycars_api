<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorPagar\AbonosPorPagarModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\CuentasPorPagar\CatEstatusAbonoModel;
use App\Models\Refacciones\CatCfdiModel;

class CreateTablaAbonosPorPagar extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AbonosPorPagarModel::getTableName(), function (Blueprint $table) {
            $table->increments(AbonosPorPagarModel::ID);
            $table->unsignedInteger(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID);
            $table->foreign(AbonosPorPagarModel::CUENTA_POR_PAGAR_ID)->references(CuentasPorPagarModel::ID)->on(CuentasPorPagarModel::getTableName());
            $table->unsignedInteger(AbonosPorPagarModel::TIPO_ABONO_ID);
            $table->foreign(AbonosPorPagarModel::TIPO_ABONO_ID)->references(CatTipoAbonoModel::ID)->on(CatTipoAbonoModel::getTableName());
            $table->unsignedInteger(AbonosPorPagarModel::CFDI_ID)->nullable();
            $table->foreign(AbonosPorPagarModel::CFDI_ID)->references(CatCfdiModel::ID)->on(CatCfdiModel::getTableName());
            $table->unsignedInteger(AbonosPorPagarModel::TIPO_PAGO_ID)->nullable();
            $table->foreign(AbonosPorPagarModel::TIPO_PAGO_ID)->references(CatTipoPagoModel::ID)->on(CatTipoPagoModel::getTableName());
            $table->unsignedInteger(AbonosPorPagarModel::ESTATUS_ABONO_ID)->nullable();
            $table->foreign(AbonosPorPagarModel::ESTATUS_ABONO_ID)->references(CatEstatusAbonoModel::ID)->on(CatEstatusAbonoModel::getTableName());
            $table->float(AbonosPorPagarModel::TOTAL_ABONO);
            $table->date(AbonosPorPagarModel::FECHA_VENCIMIENTO);
            $table->float(AbonosPorPagarModel::TOTAL_PAGO)->nullable();
            $table->date(AbonosPorPagarModel::FECHA_PAGO)->nullable();
            $table->unsignedInteger(AbonosPorPagarModel::DIAS_MORATORIOS)->nullable();
            $table->float(AbonosPorPagarModel::MONTO_MORATORIO)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(AbonosPorPagarModel::getTableName());

    }
}
