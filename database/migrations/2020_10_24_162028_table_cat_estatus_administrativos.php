<?php

use App\Models\Logistica\CatEstatusAdmvoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatEstatusAdministrativos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatEstatusAdmvoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatEstatusAdmvoModel::ID);
            $table->string(CatEstatusAdmvoModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatEstatusAdmvoModel::getTableName());
    }
}
