<?php

use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaConfort extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ConfortModel::getTableName(), function (Blueprint $table) {
            $table->increments(ConfortModel::ID);
            $table->string(ConfortModel::ACCIONAMIENTO_MANUAL)->nullable();
            $table->string(ConfortModel::ACCIONAMIENTO_ELECTRICO)->nullable();
            $table->string(ConfortModel::ASIENTO_DELANTERO_MASAJE)->nullable();
            $table->string(ConfortModel::ASIENTO_CALEFACCION)->nullable();
            $table->string(ConfortModel::EASY_POWER_FOLD)->nullable();
            $table->string(ConfortModel::MEMORIA_ASIENTOS)->nullable();
            $table->unsignedInteger(ConfortModel::SALIDA_UNIDAD_ID);
            $table->foreign(ConfortModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ConfortModel::getTableName());
    }
}
