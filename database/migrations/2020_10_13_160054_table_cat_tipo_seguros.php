<?php

use App\Models\Financiamientos\CatTipoSeguroModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatTipoSeguros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoSeguroModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoSeguroModel::ID);
            $table->string(CatTipoSeguroModel::SEGURO);
            $table->float(CatTipoSeguroModel::VALOR);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoSeguroModel::getTableName());
    }
}
