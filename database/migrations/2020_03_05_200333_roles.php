<?php

use App\Models\Usuarios\RolModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Roles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RolModel::getTableName(), function (Blueprint $table) {
            $table->increments(RolModel::ID);
            $table->string(RolModel::ROL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RolModel::getTableName());
    }
}
