<?php

use App\Models\Autos\CategoriaCatAutosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaCategoriaAutos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CategoriaCatAutosModel::getTableName(), function (Blueprint $table) {
            $table->increments(CategoriaCatAutosModel::ID);
            $table->string(CategoriaCatAutosModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CategoriaCatAutosModel::getTableName());
    }
}
