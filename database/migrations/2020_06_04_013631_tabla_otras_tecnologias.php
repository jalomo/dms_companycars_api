<?php

use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaOtrasTecnologias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OtrasTecnologiasModel::getTableName(), function (Blueprint $table) {
            $table->increments(OtrasTecnologiasModel::ID);
            $table->string(OtrasTecnologiasModel::EASY_FUEL)->nullable();
            $table->string(OtrasTecnologiasModel::TRACK_APPS)->nullable();
            $table->string(OtrasTecnologiasModel::AUTO_START_STOP)->nullable();
            $table->string(OtrasTecnologiasModel::SISTEMA_CTR_TERRENO)->nullable();
            $table->unsignedInteger(OtrasTecnologiasModel::SALIDA_UNIDAD_ID)->nullable();
            $table->foreign(OtrasTecnologiasModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(OtrasTecnologiasModel::getTableName());
    }
}
