<?php

use App\Models\Financiamientos\EstatusFinanciamientosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableEstatusFinanciamientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusFinanciamientosModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusFinanciamientosModel::ID);
            $table->string(EstatusFinanciamientosModel::ESTATUS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusFinanciamientosModel::getTableName());
    }
}
