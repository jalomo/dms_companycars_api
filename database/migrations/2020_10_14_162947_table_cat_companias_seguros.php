<?php

use App\Models\Financiamientos\CatCompaniaSeguroModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatCompaniasSeguros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatCompaniaSeguroModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatCompaniaSeguroModel::ID);
            $table->string(CatCompaniaSeguroModel::COMPANIA);
            $table->float(CatCompaniaSeguroModel::VALOR);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatCompaniaSeguroModel::getTableName());
    }
}
