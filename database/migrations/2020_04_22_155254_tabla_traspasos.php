<?php

use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\Traspasos;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaTraspasos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Traspasos::getTableName(), function (Blueprint $table) {
            $table->increments(Traspasos::ID);
            
            $table->unsignedInteger(Traspasos::ALMACEN_ORIGEN_ID);
            $table->foreign(Traspasos::ALMACEN_ORIGEN_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());

            $table->unsignedInteger(Traspasos::ALMACEN_DESTINO_ID);
            $table->foreign(Traspasos::ALMACEN_DESTINO_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());
            
            $table->unsignedInteger(Traspasos::FOLIO_ID);
            $table->foreign(Traspasos::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->integer(Traspasos::ESTATUS)->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Traspasos::getTableName());
    }
}
