<?php

use App\Models\Refacciones\ReVentasVentanillaTallerModel;
use App\Models\Refacciones\VentasRealizadasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaReVentaTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReVentasVentanillaTallerModel::getTableName(), function (Blueprint $table) {
            $table->increments(ReVentasVentanillaTallerModel::ID);
            $table->unsignedInteger(ReVentasVentanillaTallerModel::VENTA_ID);
            $table->foreign(ReVentasVentanillaTallerModel::VENTA_ID)->references(VentasRealizadasModel::ID)->on(VentasRealizadasModel::getTableName());
            $table->integer(ReVentasVentanillaTallerModel::NUMERO_ORDEN);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReVentasVentanillaTallerModel::getTableName());
    }
}
