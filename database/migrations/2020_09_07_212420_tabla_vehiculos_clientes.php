<?php

use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\CatModelosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\ClienteVehiculosModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVehiculosClientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ClienteVehiculosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ClienteVehiculosModel::ID);
            $table->text(ClienteVehiculosModel::UNIDAD_DESCRIPCION)->nullable();
            $table->string(ClienteVehiculosModel::ULTIMO_KILOMETRAJE)->nullable();
            
            $table->unsignedInteger(ClienteVehiculosModel::MARCA_ID);
            $table->foreign(ClienteVehiculosModel::MARCA_ID)->references(CatalogoMarcasModel::ID)->on(CatalogoMarcasModel::getTableName());

            $table->unsignedInteger(ClienteVehiculosModel::MODELO_ID);
            $table->foreign(ClienteVehiculosModel::MODELO_ID)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());

            
            $table->unsignedInteger(ClienteVehiculosModel::ANIO_ID);
            $table->foreign(ClienteVehiculosModel::ANIO_ID)->references(CatalogoAnioModel::ID)->on(CatalogoAnioModel::getTableName());

            $table->unsignedInteger(ClienteVehiculosModel::COLOR_ID);
            $table->foreign(ClienteVehiculosModel::COLOR_ID)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());

            $table->unsignedInteger(ClienteVehiculosModel::CATALOGO_ID);
            $table->foreign(ClienteVehiculosModel::CATALOGO_ID)->references(CatalogoAutosModel::ID)->on(CatalogoAutosModel::getTableName());
            
            $table->string(ClienteVehiculosModel::N_MOTOR)->nullable();
            $table->string(ClienteVehiculosModel::MOTOR)->nullable();
            $table->string(ClienteVehiculosModel::TRANSMISION)->nullable();
            $table->string(ClienteVehiculosModel::COMBUSTIBLE)->nullable();
            $table->string(ClienteVehiculosModel::VIN)->nullable();
            $table->string(ClienteVehiculosModel::SERIE_CORTA)->nullable();
            $table->string(ClienteVehiculosModel::PLACAS)->nullable();
            $table->integer(ClienteVehiculosModel::NUMERO_CILINDROS)->nullable();
            $table->string(ClienteVehiculosModel::CAPACIDAD)->nullable();
            $table->integer(ClienteVehiculosModel::NUMERO_PUERTAS)->nullable();
            $table->string(ClienteVehiculosModel::NOTAS)->nullable();

            $table->unsignedInteger(ClienteVehiculosModel::CLIENTE_ID);
            $table->foreign(ClienteVehiculosModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ClienteVehiculosModel::getTableName());
    }
}
