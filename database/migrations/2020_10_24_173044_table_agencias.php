<?php

use App\Models\Logistica\CatAgenciasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableAgencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatAgenciasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatAgenciasModel::ID);
            $table->string(CatAgenciasModel::AGENCIA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatAgenciasModel::getTableName());
    }
}
