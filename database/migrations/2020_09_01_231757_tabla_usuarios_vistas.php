<?php

use App\Models\Usuarios\MenuSeccionesModel;
use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuUsuariosVistasModel;
use App\Models\Usuarios\MenuVistasModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaUsuariosVistas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuUsuariosVistasModel::getTableName(), function (Blueprint $table) {
            $table->increments(MenuUsuariosVistasModel::ID);
            $table->unsignedInteger(MenuUsuariosVistasModel::USUARIO_ID);
            $table->foreign(MenuUsuariosVistasModel::USUARIO_ID)
                ->references(User::ID)
                ->on(User::getTableName());

            $table->unsignedInteger(MenuUsuariosVistasModel::SECCION_ID);
            $table->foreign(MenuUsuariosVistasModel::SECCION_ID)
                ->references(MenuSeccionesModel::ID)
                ->on(MenuSeccionesModel::getTableName());

            $table->unsignedInteger(MenuUsuariosVistasModel::SUBMENU_ID);
            $table->foreign(MenuUsuariosVistasModel::SUBMENU_ID)
                ->references(MenuSubmenuModel::ID)
                ->on(MenuSubmenuModel::getTableName());

            $table->unsignedInteger(MenuUsuariosVistasModel::VISTA_ID);
            $table->foreign(MenuUsuariosVistasModel::VISTA_ID)
                ->references(MenuVistasModel::ID)
                ->on(MenuVistasModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MenuUsuariosVistasModel::getTableName());
    }
}
