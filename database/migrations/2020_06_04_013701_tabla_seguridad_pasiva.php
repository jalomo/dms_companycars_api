<?php

use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSeguridadPasiva extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SeguridadPasivaModel::getTableName(), function (Blueprint $table) {
            $table->increments(SeguridadPasivaModel::ID);
            $table->string(SeguridadPasivaModel::CINTURON_SEGURIDAD)->nullable();
            $table->string(SeguridadPasivaModel::BOLSAS_AIRE)->nullable();
            $table->string(SeguridadPasivaModel::CHASIS_CARROCERIA)->nullable();
            $table->string(SeguridadPasivaModel::CARROSERIA_HIDROFORMA)->nullable();
            $table->string(SeguridadPasivaModel::CRISTALES)->nullable();
            $table->string(SeguridadPasivaModel::CABECERAS)->nullable();
            $table->string(SeguridadPasivaModel::SISTEMA_ALERTA)->nullable();
            $table->string(SeguridadPasivaModel::TECLADO_PUERTA)->nullable();
            $table->string(SeguridadPasivaModel::ALARMA_VOLUMETRICA)->nullable();
            $table->string(SeguridadPasivaModel::ALARMA_PERIMETRAL)->nullable();
            $table->string(SeguridadPasivaModel::MYKEY)->nullable();
            $table->unsignedInteger(SeguridadPasivaModel::SALIDA_UNIDAD_ID);
            $table->foreign(SeguridadPasivaModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SeguridadPasivaModel::getTableName());
    }
}
