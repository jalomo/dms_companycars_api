<?php

use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\CatModelosModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Refacciones\CatalogoAnioModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UnidadesModel::getTableName(), function (Blueprint $table) {
            $table->increments(UnidadesModel::ID);
            $table->text(UnidadesModel::UNIDAD_DESCRIPCION)->nullable();
            
            $table->unsignedInteger(UnidadesModel::MARCA_ID);
            $table->foreign(UnidadesModel::MARCA_ID)->references(CatalogoMarcasModel::ID)->on(CatalogoMarcasModel::getTableName());

            $table->unsignedInteger(UnidadesModel::MODELO_ID);
            $table->foreign(UnidadesModel::MODELO_ID)->references(CatModelosModel::ID)->on(CatModelosModel::getTableName());

            
            $table->unsignedInteger(UnidadesModel::ANIO_ID);
            $table->foreign(UnidadesModel::ANIO_ID)->references(CatalogoAnioModel::ID)->on(CatalogoAnioModel::getTableName());

            $table->unsignedInteger(UnidadesModel::COLOR_ID);
            $table->foreign(UnidadesModel::COLOR_ID)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            
            $table->string(UnidadesModel::MOTOR)->nullable();
            $table->string(UnidadesModel::TRANSMISION)->nullable();
            $table->string(UnidadesModel::COMBUSTIBLE)->nullable();
            $table->string(UnidadesModel::VIN)->nullable();
            $table->double(UnidadesModel::PRECIO_COSTO)->nullable();
            $table->double(UnidadesModel::PRECIO_VENTA)->nullable();
            $table->string(UnidadesModel::PLACAS)->nullable();
            $table->string(UnidadesModel::NO_SERIE);
            // 
            $table->integer(UnidadesModel::ID_ESTADO)->nullable();
            $table->string(UnidadesModel::KILOMETRAJE)->nullable();
            $table->integer(UnidadesModel::NUMERO_CILINDROS)->nullable();
            $table->string(UnidadesModel::CAPACIDAD)->nullable();
            $table->integer(UnidadesModel::NUMERO_PUERTAS)->nullable();
            $table->string(UnidadesModel::SERIE_CORTA)->nullable();
            $table->integer(UnidadesModel::ID_UBICACION)->nullable();
            $table->integer(UnidadesModel::ID_UBICACION_LLAVES)->nullable();
            $table->string(UnidadesModel::NUMERO_ECONOMICO)->nullable();
            $table->string(UnidadesModel::USUARIO_RECIBE)->nullable();
            $table->date(UnidadesModel::FECHA_RECEPCION)->nullable();
            // 
            $table->string(UnidadesModel::N_PRODUCCION)->nullable();
            $table->string(UnidadesModel::N_REMISION)->nullable();
            $table->string(UnidadesModel::N_FOLIO_REMISION)->nullable();
            $table->string(UnidadesModel::PDTO)->nullable();
            $table->string(UnidadesModel::FOLIO_PEDIDO)->nullable();

            $table->integer(UnidadesModel::VESTIDURA_ID)->nullable();
            $table->string(UnidadesModel::N_MOTOR)->nullable();
            $table->unsignedInteger(UnidadesModel::CATALOGO_ID);
            $table->foreign(UnidadesModel::CATALOGO_ID)->references(CatalogoAutosModel::ID)->on(CatalogoAutosModel::getTableName());

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(UnidadesModel::getTableName());
    }
}
