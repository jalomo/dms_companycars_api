<?php

use App\Models\Financiamientos\CatPropuestasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatPropuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatPropuestasModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatPropuestasModel::ID);
            $table->string(CatPropuestasModel::PROPUESTA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatPropuestasModel::getTableName());
    }
}
