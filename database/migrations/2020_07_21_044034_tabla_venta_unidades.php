<?php

use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\CatTipoPagoModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\EstatusCompra;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVentaUnidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(VentasAutosModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentasAutosModel::ID);
            $table->unsignedInteger(VentasAutosModel::ID_UNIDAD);
            $table->foreign(VentasAutosModel::ID_UNIDAD)->references(UnidadesModel::ID)->on(UnidadesModel::getTableName());

            $table->unsignedInteger(VentasAutosModel::ID_FOLIO);
            $table->foreign(VentasAutosModel::ID_FOLIO)->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->unsignedInteger(VentasAutosModel::ID_CLIENTE);
            $table->foreign(VentasAutosModel::ID_CLIENTE)->references(ClientesModel::ID)->on(ClientesModel::getTableName());
            $table->unsignedInteger(VentasAutosModel::ID_ESTATUS);
            $table->foreign(VentasAutosModel::ID_ESTATUS)->references(EstatusVentaAutosModel::ID)->on(EstatusVentaAutosModel::getTableName());
            $table->integer(VentasAutosModel::ID_TIPO_AUTO);
            $table->unsignedInteger(VentasAutosModel::VENDEDOR_ID);
            $table->foreign(VentasAutosModel::VENDEDOR_ID)->references(User::ID)->on(User::getTableName());
            $table->string(VentasAutosModel::MONEDA);
            $table->integer(VentasAutosModel::SERVICIO_CITA_ID)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentasAutosModel::getTableName());
    }
}
