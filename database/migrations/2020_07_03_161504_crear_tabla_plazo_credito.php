<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\CuentasPorCobrar\PlazoCreditoModel;

class CrearTablaPlazoCredito extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PlazoCreditoModel::getTableName(), function (Blueprint $table) {
            $table->increments(PlazoCreditoModel::ID);
            $table->unsignedInteger(PlazoCreditoModel::CANTIDAD_MES);
            $table->string(PlazoCreditoModel::NOMBRE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PlazoCreditoModel::getTableName());

    }
}
