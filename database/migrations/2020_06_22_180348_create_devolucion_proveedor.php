<?php

use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\DevolucionProveedorModel;
use App\Models\Refacciones\OrdenCompraModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevolucionProveedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DevolucionProveedorModel::getTableName(), function (Blueprint $table) {
            $table->increments(DevolucionProveedorModel::ID);
            $table->unsignedInteger(DevolucionProveedorModel::ORDEN_COMPRA_ID);
            $table->foreign(DevolucionProveedorModel::ORDEN_COMPRA_ID)->references(OrdenCompraModel::ID)->on(OrdenCompraModel::getTableName());
            $table->string(DevolucionProveedorModel::FACTURA);
            $table->text(DevolucionProveedorModel::OBSERVACIONES);
            $table->unsignedInteger(DevolucionProveedorModel::FOLIO_ID);
            $table->foreign(DevolucionProveedorModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());
            $table->date(DevolucionProveedorModel::FECHA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DevolucionProveedorModel::getTableName());
    }
}
