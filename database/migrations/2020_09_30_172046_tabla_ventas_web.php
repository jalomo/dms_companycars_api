<?php

use App\Models\Telemarketing\VentasWebModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaVentasWeb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(VentasWebModel::getTableName(), function (Blueprint $table) {
            $table->increments(VentasWebModel::ID);
            $table->integer(VentasWebModel::ID_CLIENTE);
            $table->integer(VentasWebModel::ID_UNIDAD);
            $table->integer(VentasWebModel::ID_USUARIO_CREO);
            $table->integer(VentasWebModel::ID_ASESOR_WEB_ASIGNADO)->nullable();
            $table->integer(VentasWebModel::ID_USUARIO_TELEMARKETING)->nullable();
            $table->date(VentasWebModel::FECHA_CITA_PROGRAMADA)->nullable();
            $table->integer(VentasWebModel::INTENTOS)->nullable();
            $table->boolean(VentasWebModel::NO_CONTACTAR)->nullable()->default(0);
            $table->integer(VentasWebModel::ID_ORIGEN);
            $table->boolean(VentasWebModel::VENDIDO)->nullable();
            $table->text(VentasWebModel::COMENTARIO_VENTA)->nullable();
            $table->timestamp(VentasWebModel::FECHA_COMENTARIO)->nullable();
            $table->boolean(VentasWebModel::INFORMACION_ERRONEA)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(VentasWebModel::getTableName());
    }
}
