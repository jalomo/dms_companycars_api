<?php

use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaSeguridad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SeguridadModel::getTableName(), function (Blueprint $table) {
            $table->increments(SeguridadModel::ID);
            $table->string(SeguridadModel::SISTEMA_FRENADO)->nullable();
            $table->string(SeguridadModel::CONTROL_TRACCION)->nullable();
            $table->string(SeguridadModel::ESC)->nullable();
            $table->string(SeguridadModel::RSC)->nullable();
            $table->string(SeguridadModel::CONTROL_TORQUE_CURVAS)->nullable();
            $table->string(SeguridadModel::CONTROL_CURVAS)->nullable();
            $table->string(SeguridadModel::CONTROL_BALANCE_REMOLQUE)->nullable();
            $table->string(SeguridadModel::FRENOS_ELECTRONICOS)->nullable();
            $table->string(SeguridadModel::CONTROL_ELECTRONICO_DES)->nullable();
            $table->unsignedInteger(SeguridadModel::SALIDA_UNIDAD_ID);
            $table->foreign(SeguridadModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(SeguridadModel::getTableName());
    }
}
