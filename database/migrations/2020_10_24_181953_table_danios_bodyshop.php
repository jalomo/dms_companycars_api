<?php

use App\Models\Logistica\CatAgenciasModel;
use App\Models\Logistica\CatAreaReparacionModel;
use App\Models\Logistica\CatEstatusAdmvoModel;
use App\Models\Logistica\CatEstatusDaniosBodyshopModel;
use App\Models\Logistica\DaniosBodyshopModel;
use App\Models\Refacciones\CatalogoColoresModel;
use App\Models\Refacciones\UbicacionLLavesModel;
use App\Models\Telemarketing\CatalogoOrigenesModel;
use App\Models\Usuarios\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableDaniosBodyshop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DaniosBodyshopModel::getTableName(), function (Blueprint $table) {
            $table->increments(DaniosBodyshopModel::ID);
            $table->date(DaniosBodyshopModel::FECHA_ARRIBO);
            $table->unsignedInteger(DaniosBodyshopModel::ID_ESTATUS_ADMVO)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_ESTATUS_ADMVO)->references(CatEstatusAdmvoModel::ID)->on(CatEstatusAdmvoModel::getTableName());
            $table->unsignedInteger(DaniosBodyshopModel::ID_ASESOR)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_ASESOR)->references(User::ID)->on(User::getTableName());
            $table->string(DaniosBodyshopModel::SERIE);
            $table->unsignedInteger(DaniosBodyshopModel::ID_COLOR)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->string(DaniosBodyshopModel::DANIOS);
            $table->unsignedInteger(DaniosBodyshopModel::ID_UBICACION_LLAVES)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_UBICACION_LLAVES)->references(UbicacionLLavesModel::ID)->on(UbicacionLLavesModel::getTableName());
            $table->text(DaniosBodyshopModel::DESCRIPCION);
            $table->string(DaniosBodyshopModel::FALTANTES);
            $table->unsignedInteger(DaniosBodyshopModel::ID_ESTATUS)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_ESTATUS)->references(CatEstatusDaniosBodyshopModel::ID)->on(CatEstatusDaniosBodyshopModel::getTableName());
            $table->date(DaniosBodyshopModel::FECHA_INGRESO);
            $table->date(DaniosBodyshopModel::FECHA_ENTREGA);
            $table->text(DaniosBodyshopModel::OBSERVACIONES);
            $table->unsignedInteger(DaniosBodyshopModel::ID_AGENCIA)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_AGENCIA)->references(CatAgenciasModel::ID)->on(CatAgenciasModel::getTableName());
            $table->unsignedInteger(DaniosBodyshopModel::ID_ORIGEN)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_ORIGEN)->references(CatalogoOrigenesModel::ID)->on(CatalogoOrigenesModel::getTableName());
            $table->time(DaniosBodyshopModel::HORA_RECEPCION);
            $table->string(DaniosBodyshopModel::CARGO_A);
            $table->string(DaniosBodyshopModel::COBRO_PROVEEDORES);
            $table->string(DaniosBodyshopModel::MEDIO_TRANSPORTACION);
            $table->unsignedInteger(DaniosBodyshopModel::ID_AREA_REPARACION)->nullable();
            $table->foreign(DaniosBodyshopModel::ID_AREA_REPARACION)->references(CatAreaReparacionModel::ID)->on(CatAreaReparacionModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DaniosBodyshopModel::getTableName());
    }
}
