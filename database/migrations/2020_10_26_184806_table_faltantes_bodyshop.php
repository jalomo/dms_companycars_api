<?php

use App\Models\Autos\UnidadesModel;
use App\Models\Logistica\FaltantesModel;
use App\Models\Refacciones\CatalogoColoresModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableFaltantesBodyshop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FaltantesModel::getTableName(), function (Blueprint $table) {
            $table->increments(FaltantesModel::ID);
            $table->string(FaltantesModel::FOLIO);
            $table->unsignedInteger(FaltantesModel::ID_UNIDAD)->nullable();
            $table->foreign(FaltantesModel::ID_UNIDAD)->references(UnidadesModel::ID)->on(UnidadesModel::getTableName());
            $table->unsignedInteger(FaltantesModel::ID_COLOR)->nullable();
            $table->foreign(FaltantesModel::ID_COLOR)->references(CatalogoColoresModel::ID)->on(CatalogoColoresModel::getTableName());
            $table->string(FaltantesModel::SERIE);
            $table->text(FaltantesModel::OBSERVACIONES);
            $table->string(FaltantesModel::SOLICITUD);
            $table->string(FaltantesModel::RECIBOS);
            $table->date(FaltantesModel::FECHA_ENTREGA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FaltantesModel::getTableName());
    }
}
