<?php

use App\Models\Facturas\Factura;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Factura::getTableName(), function (Blueprint $table) {
            $table->increments(Factura::ID);
            $table->string(Factura::LUGAR_EXPEDICION)->nullable();
            $table->string(Factura::FORMA_PAGO)->nullable();
            $table->string(Factura::METODO_PAGO)->nullable();
            $table->string(Factura::FOLIO)->nullable();
            $table->string(Factura::SERIE)->nullable();
            $table->string(Factura::TIPO_COMPROBANTE)->nullable();
            $table->string(Factura::TIPO_CAMBIO)->nullable();
            $table->string(Factura::MONEDA)->nullable();
            $table->string(Factura::TOTAL)->nullable();
            $table->string(Factura::SUB_TOTAL)->nullable();
            $table->string(Factura::FECHA)->nullable();
            $table->string(Factura::NO_CERTIFICADO)->nullable();
            $table->string(Factura::UUID);
            $table->text(Factura::CERTIFICADO)->nullable();
            $table->text(Factura::SELLO)->nullable();

            $table->string(Factura::RUTA_XML_FACTURA);
            $table->string(Factura::FILE_NAME);
            $table->string(Factura::RUTA_PDF_FACTURA)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Factura::getTableName());
    }
}
