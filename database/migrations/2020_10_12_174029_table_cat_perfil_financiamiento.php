<?php

use App\Models\Financiamientos\CatPerfilFinanciamientoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatPerfilFinanciamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatPerfilFinanciamientoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatPerfilFinanciamientoModel::ID);
            $table->string(CatPerfilFinanciamientoModel::PERFIL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatPerfilFinanciamientoModel::getTableName());
    }
}
