<?php

use App\Models\Autos\UnidadesModel;
use App\Models\Refacciones\RecepcionUnidadesCostosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaRecepcionUnidadesCostos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(RecepcionUnidadesCostosModel::getTableName(), function (Blueprint $table) {
            $table->increments(RecepcionUnidadesCostosModel::ID);
            $table->float(RecepcionUnidadesCostosModel::VALOR_UNIDAD_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::VALOR_UNIDAD_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::OTROS_GASTOS)->nullable();
            $table->float(RecepcionUnidadesCostosModel::EQUIPO_BASE_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::EQUIPO_BASE_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::GASTOS_ACONDI)->nullable();
            $table->float(RecepcionUnidadesCostosModel::SEG_TRASLADO_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::SEG_TRASLADO_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::AP_FONDOS_PUB)->nullable();
            $table->float(RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::IMPUESTO_IMPORT_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::AP_PROG_CIVIL)->nullable();
            $table->float(RecepcionUnidadesCostosModel::FLETES_IMPORT_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::FLETES_IMPORT_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::CUOTA_AMDA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::GASTOS_TRASLADO_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::GASTOS_TRASLADO_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::CUOTA_COPARMEX)->nullable();
            $table->float(RecepcionUnidadesCostosModel::DEDUCCION_FORD_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::DEDUCCION_FORD_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::CUOTA_ASOCIACION_FORD)->nullable();
            $table->float(RecepcionUnidadesCostosModel::BONIFICACION_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::BONIFICACION_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::SUBTOTAL_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::SUBTOTAL_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::IVA_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::IVA_VENTA)->nullable();
            $table->float(RecepcionUnidadesCostosModel::TOTAL_COSTO)->nullable();
            $table->float(RecepcionUnidadesCostosModel::TOTAL_VENTA)->nullable();

            $table->unsignedInteger(RecepcionUnidadesCostosModel::UNIDAD_ID);
            $table->foreign(RecepcionUnidadesCostosModel::UNIDAD_ID)
            ->references(UnidadesModel::ID)->on(UnidadesModel::getTableName());
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(RecepcionUnidadesCostosModel::getTableName());
    }
}
