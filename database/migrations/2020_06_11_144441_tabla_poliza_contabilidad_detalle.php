<?php

use App\Models\Contabilidad\PolizaContabilidadDetalleModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaPolizaContabilidadDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PolizaContabilidadDetalleModel::getTableName(), function (Blueprint $table) {
            $table->increments(PolizaContabilidadDetalleModel::ID);
            $table->string(PolizaContabilidadDetalleModel::CONCEPTO);
            $table->string(PolizaContabilidadDetalleModel::COSTO);
            $table->unsignedInteger(PolizaContabilidadDetalleModel::ID_CUENTA);
            $table->unsignedInteger(PolizaContabilidadDetalleModel::ID_SUBCUENTA);
            $table->unsignedInteger(PolizaContabilidadDetalleModel::ID_POLIZA);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(PolizaContabilidadDetalleModel::getTableName());
    }
}
