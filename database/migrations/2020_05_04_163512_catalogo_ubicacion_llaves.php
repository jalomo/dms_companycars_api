<?php

use App\Models\Refacciones\UbicacionLLavesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CatalogoUbicacionLlaves extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UbicacionLLavesModel::getTableName(), function (Blueprint $table) {
            $table->increments(UbicacionLLavesModel::ID);
            $table->string(UbicacionLLavesModel::NOMBRE);
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(UbicacionLLavesModel::getTableName());
    }
}
