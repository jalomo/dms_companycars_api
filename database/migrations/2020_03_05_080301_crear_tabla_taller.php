<?php

use App\Models\Refacciones\Talleres;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaTaller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Talleres::getTableName(), function (Blueprint $table) {
            $table->increments(Talleres::ID);
            $table->string(Talleres::NOMBRE);
            $table->string(Talleres::UBICACION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(Talleres::getTableName());
    }
}
