<?php

use App\Models\Facturas\Factura;
use App\Models\Facturas\ReceptorFactura;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearReceptorFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReceptorFactura::getTableName(), function (Blueprint $table) {
            $table->increments(ReceptorFactura::ID);
            $table->string(ReceptorFactura::NOMBRE);
            $table->string(ReceptorFactura::RFC);
            $table->string(ReceptorFactura::USOCFDI);
            $table->unsignedInteger(ReceptorFactura::FACTURA_ID);
            $table->foreign(ReceptorFactura::FACTURA_ID)->references(Factura::ID)->on(Factura::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ReceptorFactura::getTableName());
    }
}
