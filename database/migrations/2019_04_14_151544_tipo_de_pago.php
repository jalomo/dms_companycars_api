<?php

use App\Models\Refacciones\CatTipoPagoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipoDePago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoPagoModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoPagoModel::ID);
            $table->string(CatTipoPagoModel::NOMBRE);
            $table->string(CatTipoPagoModel::CLAVE);
            $table->boolean(CatTipoPagoModel::ACTIVO)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoPagoModel::getTableName());
    }
}
