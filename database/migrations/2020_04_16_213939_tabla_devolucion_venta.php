<?php

use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Refacciones\DevolucionVentasModel;
use App\Models\Refacciones\Precios;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\VendedorModel;
use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaDevolucionVenta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DevolucionVentasModel::getTableName(), function (Blueprint $table) {
            $table->increments(DevolucionVentasModel::ID);
            

            $table->unsignedInteger(DevolucionVentasModel::CLIENTE_ID);
            $table->foreign(DevolucionVentasModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());

            $table->unsignedInteger(DevolucionVentasModel::FOLIO_ID);
            $table->foreign(DevolucionVentasModel::FOLIO_ID)->references(FoliosModel::ID)->on(FoliosModel::getTableName());

            $table->unsignedInteger(DevolucionVentasModel::VENTA_ID);
            $table->foreign(DevolucionVentasModel::VENTA_ID)->references(VentaProductoModel::ID)->on(VentaProductoModel::getTableName());
            $table->longText(DevolucionVentasModel::OBSERVACIONES);
            $table->float(DevolucionVentasModel::TOTAL_DEVOLUCION);
            
            $table->unsignedInteger(DevolucionVentasModel::VENDEDOR_ID);
            $table->foreign(DevolucionVentasModel::VENDEDOR_ID)->references(VendedorModel::ID)->on(VendedorModel::getTableName());
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(DevolucionVentasModel::getTableName());
    }
}
