<?php

use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaFuncionamientoVehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FuncionamientoVehiculoModel::getTableName(), function (Blueprint $table) {
            $table->increments(FuncionamientoVehiculoModel::ID);
            $table->string(FuncionamientoVehiculoModel::ASISTENTE_ARRANQUE)->nullable();
            $table->string(FuncionamientoVehiculoModel::PRESERVACION_CARRIL)->nullable();
            $table->string(FuncionamientoVehiculoModel::SENSORES_DEL_LAT)->nullable();
            $table->string(FuncionamientoVehiculoModel::BLIS)->nullable();
            $table->string(FuncionamientoVehiculoModel::ALERTA_TRAFICO_CRUZADO)->nullable();
            $table->string(FuncionamientoVehiculoModel::ASISTENCIA_ACTIVA_ESTACI)->nullable();
            $table->string(FuncionamientoVehiculoModel::CAMARA_REVERSA)->nullable();
            $table->string(FuncionamientoVehiculoModel::CAMARA_FRONTAL)->nullable();
            $table->string(FuncionamientoVehiculoModel::CAMARA_360_GRADOS)->nullable();
            $table->string(FuncionamientoVehiculoModel::SITEMA_MON_PLL_TMS)->nullable();
            $table->unsignedInteger(FuncionamientoVehiculoModel::SALIDA_UNIDAD_ID);
            $table->foreign(FuncionamientoVehiculoModel::SALIDA_UNIDAD_ID)->references(SalidaUnidadesModel::ID)->on(SalidaUnidadesModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(FuncionamientoVehiculoModel::getTableName());
    }
}
