<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Refacciones\CatTipoPrecioModel;


class AddTableCatalogoTipoPrecio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatTipoPrecioModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatTipoPrecioModel::ID);
            $table->string(CatTipoPrecioModel::NOMBRE);
            $table->boolean(CatTipoPrecioModel::ACTIVO)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatTipoPrecioModel::getTableName());
    }
}
