<?php

use App\Models\Autos\EquipoOpcionalModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Refacciones\ProductosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEquipoOpcional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EquipoOpcionalModel::getTableName(), function (Blueprint $table) {
            $table->increments(EquipoOpcionalModel::ID);
            $table->unsignedInteger(EquipoOpcionalModel::VENTA_AUTO_ID);
            $table->foreign(EquipoOpcionalModel::VENTA_AUTO_ID)
                ->references(VentasAutosModel::ID)
                ->on(VentasAutosModel::getTableName());
            $table->integer(EquipoOpcionalModel::CANTIDAD);
            $table->double(EquipoOpcionalModel::TOTAL);
            $table->double(EquipoOpcionalModel::PRECIO);
            $table->unsignedInteger(EquipoOpcionalModel::PRODUCTO_ID);
            $table->foreign(EquipoOpcionalModel::PRODUCTO_ID)
                ->references(ProductosModel::ID)
                ->on(ProductosModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EquipoOpcionalModel::getTableName());
    }
}
