<?php

use App\Models\Usuarios\Menu\ModulosModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaModulosMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ModulosModel::getTableName(), function (Blueprint $table) {
            $table->increments(ModulosModel::ID);
            $table->string(ModulosModel::NOMBRE);
            $table->string(ModulosModel::ICONO)->nullable();
            $table->integer(ModulosModel::ORDEN)->nullable();
            $table->boolean(ModulosModel::DEFAULT)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ModulosModel::getTableName());
    }
}
