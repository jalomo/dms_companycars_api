<?php

use App\Models\Refacciones\EstatusTraspasoModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaEstatusTraspasos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(EstatusTraspasoModel::getTableName(), function (Blueprint $table) {
            $table->increments(EstatusTraspasoModel::ID);
            $table->string(EstatusTraspasoModel::NOMBRE);
            // $table->boolean(EstatusTraspasoModel::ACTIVO)->default(1);
            // $table->foreign(EstatusTraspasoModel::ESTATUS_FACTURA_ID)->references(EstatusFacturaModel::ID)->on(EstatusFacturaModel::getTableName());
            // $table->unsignedInteger(EstatusTraspasoModel::USER_ID);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(EstatusTraspasoModel::getTableName());
    }
}
