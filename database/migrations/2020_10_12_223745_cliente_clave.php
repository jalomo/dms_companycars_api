<?php

use App\Models\Refacciones\CatalogoClaveClienteModel;
use App\Models\Refacciones\ClaveClienteModel;
use App\Models\Refacciones\ClientesModel;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClienteClave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create(ClaveClienteModel::getTableName(), function (Blueprint $table) {
            $table->increments(ClaveClienteModel::ID);

            $table->unsignedInteger(ClaveClienteModel::CLIENTE_ID);
            $table->foreign(ClaveClienteModel::CLIENTE_ID)->references(ClientesModel::ID)->on(ClientesModel::getTableName());

            $table->unsignedInteger(ClaveClienteModel::CLAVE_ID);
            $table->foreign(ClaveClienteModel::CLAVE_ID)->references(CatalogoClaveClienteModel::ID)->on(CatalogoClaveClienteModel::getTableName());
            
            $table->timestamps();
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(ClaveClienteModel::getTableName());
    }
}
