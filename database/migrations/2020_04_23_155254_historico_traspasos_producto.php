<?php

use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\TraspasoProductoAlmacenModel;
use App\Models\Refacciones\Traspasos;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class HistoricoTraspasosProducto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TraspasoProductoAlmacenModel::getTableName(), function (Blueprint $table) {
            $table->increments(TraspasoProductoAlmacenModel::ID);
            
            $table->unsignedInteger(TraspasoProductoAlmacenModel::PRODUCTO_ID);
            $table->foreign(TraspasoProductoAlmacenModel::PRODUCTO_ID)->references(ProductosModel::ID)->on(ProductosModel::getTableName());

            $table->unsignedInteger(TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID);
            $table->foreign(TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());

            $table->unsignedInteger(TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID);
            $table->foreign(TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID)->references(Almacenes::ID)->on(Almacenes::getTableName());
            

            $table->unsignedInteger(TraspasoProductoAlmacenModel::TRASPASO_ID);
            $table->foreign(TraspasoProductoAlmacenModel::TRASPASO_ID)->references(Traspasos::ID)->on(Traspasos::getTableName());

            $table->boolean(TraspasoProductoAlmacenModel::ACTIVO)->default(1);
            $table->float(TraspasoProductoAlmacenModel::TOTAL);
            $table->float(TraspasoProductoAlmacenModel::VALOR_UNITARIO);
            $table->integer(TraspasoProductoAlmacenModel::CANTIDAD);
            $table->integer(TraspasoProductoAlmacenModel::DESCONTADO_A_PRODUCTO)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(TraspasoProductoAlmacenModel::getTableName());
    }
}
