<?php

use App\Models\Logistica\CatCarrilesModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableCatCarriles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CatCarrilesModel::getTableName(), function (Blueprint $table) {
            $table->increments(CatCarrilesModel::ID);
            $table->string(CatCarrilesModel::CARRIL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(CatCarrilesModel::getTableName());
    }
}
