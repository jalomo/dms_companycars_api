<?php

use App\Models\Usuarios\MenuSubmenuModel;
use App\Models\Usuarios\MenuVistasModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TablaMenuVistas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuVistasModel::getTableName(), function (Blueprint $table) {
            $table->increments(MenuVistasModel::ID);
            $table->string(MenuVistasModel::NOMBRE);
            $table->boolean(MenuVistasModel::ES_EXTERNA)->nullable();
            $table->string(MenuVistasModel::LINK)->nullable();
            $table->string(MenuVistasModel::CONTROLADOR)->nullable();
            $table->string(MenuVistasModel::MODULO)->nullable();
            $table->unsignedInteger(MenuVistasModel::SUBMENU_ID);
            $table->foreign(MenuVistasModel::SUBMENU_ID)
                ->references(MenuSubmenuModel::ID)
                ->on(MenuSubmenuModel::getTableName());
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(MenuVistasModel::getTableName());
    }
}
