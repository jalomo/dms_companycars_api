<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTurnosImssModel as Model;

class CaTurnosImssSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID => 1,Model::Descripcion => 'Matutino'],
            [Model::ID => 2,Model::Descripcion => 'Vespertino'],
            [Model::ID => 3,Model::Descripcion => 'Noctuno'],
            [Model::ID => 4,Model::Descripcion => 'Mixto']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
