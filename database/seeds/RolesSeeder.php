<?php

use App\Models\Usuarios\RolModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                RolModel::ID => 1,
                RolModel::ROL => 'Administrador',

            ],
            [
                RolModel::ID => 2,
                RolModel::ROL => 'Dirección',
            ],
            [
                RolModel::ID => 3,
                RolModel::ROL => 'Gerencia',
            ],
            [
                RolModel::ID => 4,
                RolModel::ROL => 'Administrativos',
            ],
            [
                RolModel::ID => 5,
                RolModel::ROL => 'Gestor Cobranza',
            ],
            [
                RolModel::ID => 6,
                RolModel::ROL => 'usuario nivel 1',
            ],
            [
                RolModel::ID => 7,
                RolModel::ROL => 'usuario nivel 2',
            ],
            [
                RolModel::ID => 8,
                RolModel::ROL => 'usuario nivel 3',
            ],
            [
                RolModel::ID => 9,
                RolModel::ROL => 'usuario nivel 4',
            ],
            [
                RolModel::ID => 10,
                RolModel::ROL => 'Usuario generico',
            ],
            [
                RolModel::ID => 11,
                RolModel::ROL => 'Asesor prospectos ventas',
            ],
            [
                RolModel::ID => 12,
                RolModel::ROL => 'Asesor Telemarketing',
            ],
            [
                RolModel::ID => 13,
                RolModel::ROL => 'Asesor web ventas',
            ]
        ];

        foreach ($roles as $key => $value) {
            DB::table(RolModel::getTableName())->insert([
                RolModel::ID => $value[RolModel::ID],
                RolModel::ROL => $value[RolModel::ROL],
            ]);
        }
    }
}
