<?php

use App\Models\Refacciones\CatalogoAnioModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoAniosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoAnioModel::NOMBRE => 1950],
            [CatalogoAnioModel::NOMBRE => 1951],
            [CatalogoAnioModel::NOMBRE => 1952],
            [CatalogoAnioModel::NOMBRE => 1953],
            [CatalogoAnioModel::NOMBRE => 1954],
            [CatalogoAnioModel::NOMBRE => 1955],
            [CatalogoAnioModel::NOMBRE => 1956],
            [CatalogoAnioModel::NOMBRE => 1957],
            [CatalogoAnioModel::NOMBRE => 1958],
            [CatalogoAnioModel::NOMBRE => 1959],
            [CatalogoAnioModel::NOMBRE => 1960],
            [CatalogoAnioModel::NOMBRE => 1961],
            [CatalogoAnioModel::NOMBRE => 1962],
            [CatalogoAnioModel::NOMBRE => 1963],
            [CatalogoAnioModel::NOMBRE => 1964],
            [CatalogoAnioModel::NOMBRE => 1965],
            [CatalogoAnioModel::NOMBRE => 1966],
            [CatalogoAnioModel::NOMBRE => 1967],
            [CatalogoAnioModel::NOMBRE => 1968],
            [CatalogoAnioModel::NOMBRE => 1969],
            [CatalogoAnioModel::NOMBRE => 1970],
            [CatalogoAnioModel::NOMBRE => 1971],
            [CatalogoAnioModel::NOMBRE => 1972],
            [CatalogoAnioModel::NOMBRE => 1973],
            [CatalogoAnioModel::NOMBRE => 1974],
            [CatalogoAnioModel::NOMBRE => 1975],
            [CatalogoAnioModel::NOMBRE => 1976],
            [CatalogoAnioModel::NOMBRE => 1977],
            [CatalogoAnioModel::NOMBRE => 1978],
            [CatalogoAnioModel::NOMBRE => 1979],
            [CatalogoAnioModel::NOMBRE => 1980],
            [CatalogoAnioModel::NOMBRE => 1981],
            [CatalogoAnioModel::NOMBRE => 1982],
            [CatalogoAnioModel::NOMBRE => 1983],
            [CatalogoAnioModel::NOMBRE => 1984],
            [CatalogoAnioModel::NOMBRE => 1985],
            [CatalogoAnioModel::NOMBRE => 1986],
            [CatalogoAnioModel::NOMBRE => 1987],
            [CatalogoAnioModel::NOMBRE => 1988],
            [CatalogoAnioModel::NOMBRE => 1989],
            [CatalogoAnioModel::NOMBRE => 1990],
            [CatalogoAnioModel::NOMBRE => 1991],
            [CatalogoAnioModel::NOMBRE => 1992],
            [CatalogoAnioModel::NOMBRE => 1993],
            [CatalogoAnioModel::NOMBRE => 1994],
            [CatalogoAnioModel::NOMBRE => 1995],
            [CatalogoAnioModel::NOMBRE => 1995],
            [CatalogoAnioModel::NOMBRE => 1996],
            [CatalogoAnioModel::NOMBRE => 1997],
            [CatalogoAnioModel::NOMBRE => 1998],
            [CatalogoAnioModel::NOMBRE => 1999],
            [CatalogoAnioModel::NOMBRE => 2001],
            [CatalogoAnioModel::NOMBRE => 2002],
            [CatalogoAnioModel::NOMBRE => 2003],
            [CatalogoAnioModel::NOMBRE => 2004],
            [CatalogoAnioModel::NOMBRE => 2005],
            [CatalogoAnioModel::NOMBRE => 2006],
            [CatalogoAnioModel::NOMBRE => 2007],
            [CatalogoAnioModel::NOMBRE => 2008],
            [CatalogoAnioModel::NOMBRE => 2009],
            [CatalogoAnioModel::NOMBRE => 2010],
            [CatalogoAnioModel::NOMBRE => 2011],
            [CatalogoAnioModel::NOMBRE => 2012],
            [CatalogoAnioModel::NOMBRE => 2013],
            [CatalogoAnioModel::NOMBRE => 2014],
            [CatalogoAnioModel::NOMBRE => 2015],
            [CatalogoAnioModel::NOMBRE => 2016],
            [CatalogoAnioModel::NOMBRE => 2017],
            [CatalogoAnioModel::NOMBRE => 2018],
            [CatalogoAnioModel::NOMBRE => 2019],
            [CatalogoAnioModel::NOMBRE => 2020],
            [CatalogoAnioModel::NOMBRE => 2021],
            [CatalogoAnioModel::NOMBRE => 2022]
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoAnioModel::getTableName())->insert($items);
        }
    }
}
