<?php

use App\Models\Autos\CatModelosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoModelosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [
                CatModelosModel::NOMBRE => 'FIGO 4PTS IMPULSE TM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PT IMPULSE TA A/C',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PTS IMPULSE TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS IMPULSE TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PTS IMPULSE TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS ENERGY TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PTS ENERGY TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO TITANIMUN TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PT IMPULSE TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PT IMPULSE TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PT IMPULSE TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PT IMPULSE TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS ENERGY TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PTS ENERGY TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO 4PT IMPULSE TM AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIGO TITANIUM TA AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 5-DR  SE A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 5-DR SE M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 4DR SE A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA SE M/T 4-DR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 5 DR S M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA SEDAN S A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA S SEDAN M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 4 DR TITANIUM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FIESTA 5 DR  ST',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS ST 5-DR HATCH M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS DOOR SEDAN S M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DOOR SPORT SE A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HACTCHBACK SE A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK SE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCH SE APPR A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HACTCH SE APPR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN S M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 4-DOOR PLUS TITAN A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK ST',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS AMBIENTE 4-DR SEDAN M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS TREND 4-DR SEDAN A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS TREND SPORT 4-DR SEDAN A/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK RS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FOCUS TITANIUM 4-DR SEDAN A/TS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE LUX PLUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE LUXURY',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE NAVI',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE HYBRID LUX',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SE HYBRID',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SEL HYBRID',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION SEL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION S',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'FUSION TITANIUM PLUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT COUPE PREM M/T',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG V6 COUPE TM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE TA',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG BULLITT ',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'MUSTANG I4 ECOBOOST PREMIUM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'SHELBY GT350 COUPE',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ECOSPORT TITANIUM AT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ECOSPORT TREND AT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ECOSPORT TREND MANUAL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ECOSPORT ',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE SEL PQ. TITANIUM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE SE PLUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE TITANIUM 2.5L',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE ADVANCE 2.5',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE ADVANCE 2.0 ECOBOOST',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE S PLUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE S',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE FWD SE HYBRIDA ',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE TITANIUM FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'ESCAPE S',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE SEL FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE SE FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE SEL FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE SEL FWD PLUS',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE LIMITED FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EDGE SPORT FWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER LIMITED 4WD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR LTD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER FWD 4DR XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPLORER 4DR4WD SPORT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPEDITION EXP LIMITED MAX 4X2',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPEDITION PLATINUM MAX 4X4',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPEDITION PLATINUM 4X4',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'EXPEDITION LIMITED 4X2',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB ',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW XLT 4X2',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB LTD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB LTD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SAFR CREW CB 4X2 XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SAFR CRW CAB 4X4 XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X4 XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'RANGER SA CREW CAB XL',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F150 XL 4X2 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F150 XL 4X2 SUPERCREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F150 XL 4X4 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-150 XL 4X2 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-150 XL 4X4 SUPERCREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-150 XL 4X2 SUPERCREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F150 XL 4X2 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F250 SUPERDUTY SD PICK UP XLT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F350 XL PLUS CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-350 SUPER DUTY XL PLUS CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-350 XL PLUS CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F350 XL CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F450 XL REG CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-450 XL REG CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-550 XL REG CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'F-550 XL REG CHAS CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT VAN SWB AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT VAN MWB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT PASS MWB AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CCMWBAC RWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CC LWB AC RWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CC LWB AC RWD',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT BUS ELWB AC',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT 350 MR WAGON',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT 250 MR VAN',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN SWB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CUST PASS LWB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO XLT 4X2 SUPERCREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO TREMOR 4X4 C REG',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO LRT 4X2 SUPERCREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO LARIAT 4X4 CREW',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 PLATINUM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 KING RANCH',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 LIMITED',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 PLATINUM',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ],
            [
                CatModelosModel::NOMBRE => 'LOBO 4X4 RAPTOR',
                CatModelosModel::TIEMPO_LAVADO => 0
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(CatModelosModel::getTableName())->insert($items);
        }
    }
}


/* */
