<?php

use App\Models\Autos\CatalogoAutosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoAutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $autos = [
            [CatalogoAutosModel::CLAVE => 'Y4D', CatalogoAutosModel::NOMBRE => 'FIGO 4PTS IMPULSE TM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y5A', CatalogoAutosModel::NOMBRE => 'FIGO 5PT IMPULSE TA A/C', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y5B', CatalogoAutosModel::NOMBRE => 'FIGO 4PTS IMPULSE TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y5C', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS IMPULSE TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y5D', CatalogoAutosModel::NOMBRE => 'FIGO 4PTS IMPULSE TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y6A', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS ENERGY TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y6B', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y6C', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y6D', CatalogoAutosModel::NOMBRE => 'FIGO 4PTS ENERGY TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'Y7A', CatalogoAutosModel::NOMBRE => 'FIGO TITANIMUN TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X5A', CatalogoAutosModel::NOMBRE => 'FIGO 5PT IMPULSE TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X5B', CatalogoAutosModel::NOMBRE => 'FIGO 4PT IMPULSE TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X5C', CatalogoAutosModel::NOMBRE => 'FIGO 5PT IMPULSE TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X5D', CatalogoAutosModel::NOMBRE => 'FIGO 4PT IMPULSE TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X6A', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS ENERGY TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X6B', CatalogoAutosModel::NOMBRE => 'FIGO 5PTS ENERGY TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X6C', CatalogoAutosModel::NOMBRE => 'FIGO 4PTS ENERGY TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X6D', CatalogoAutosModel::NOMBRE => 'FIGO 4PT IMPULSE TM AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],
            [CatalogoAutosModel::CLAVE => 'X7A', CatalogoAutosModel::NOMBRE => 'FIGO TITANIUM TA AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 1],

            [CatalogoAutosModel::CLAVE => 'F2A', CatalogoAutosModel::NOMBRE => 'FIESTA 5-DR  SE A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F2B', CatalogoAutosModel::NOMBRE => 'FIESTA 5-DR SE M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F2C', CatalogoAutosModel::NOMBRE => 'FIESTA 4DR SE A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F2D', CatalogoAutosModel::NOMBRE => 'FIESTA SE M/T 4-DR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F3B', CatalogoAutosModel::NOMBRE => 'FIESTA 5 DR S M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F3C', CatalogoAutosModel::NOMBRE => 'FIESTA SEDAN S A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F3D', CatalogoAutosModel::NOMBRE => 'FIESTA S SEDAN M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F4C', CatalogoAutosModel::NOMBRE => 'FIESTA 4 DR TITANIUM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],
            [CatalogoAutosModel::CLAVE => 'F5B', CatalogoAutosModel::NOMBRE => 'FIESTA 5 DR  ST', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 2],

            [CatalogoAutosModel::CLAVE => 'Z0A', CatalogoAutosModel::NOMBRE => 'FOCUS ST 5-DR HATCH M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z1B', CatalogoAutosModel::NOMBRE => 'FOCUS DOOR SEDAN S M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z2A', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DOOR SPORT SE A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z2B', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HACTCHBACK SE A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z2D', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z2E', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN SE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z2F', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK SE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z3A', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HATCH SE APPR A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z3B', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z3C', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DR SEDAN SE APPR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z3H', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HACTCH SE APPR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z3F', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DOOR SEDAN S M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z4C', CatalogoAutosModel::NOMBRE => 'FOCUS 4-DOOR PLUS TITAN A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z5J', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK ST', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z6D', CatalogoAutosModel::NOMBRE => 'FOCUS AMBIENTE 4-DR SEDAN M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z7H', CatalogoAutosModel::NOMBRE => 'FOCUS TREND 4-DR SEDAN A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z8E', CatalogoAutosModel::NOMBRE => 'FOCUS TREND SPORT 4-DR SEDAN A/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'ZRS', CatalogoAutosModel::NOMBRE => 'FOCUS 5-DR HATCHBACK RS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],
            [CatalogoAutosModel::CLAVE => 'Z9B', CatalogoAutosModel::NOMBRE => 'FOCUS TITANIUM 4-DR SEDAN A/TS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 3],

            [CatalogoAutosModel::CLAVE => 'C2C',  CatalogoAutosModel::NOMBRE => 'FUSION SE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2E',  CatalogoAutosModel::NOMBRE => 'FUSION SE LUX PLUS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2F',  CatalogoAutosModel::NOMBRE => 'FUSION SE LUXURY', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2G',  CatalogoAutosModel::NOMBRE => 'FUSION SE NAVI', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2J',  CatalogoAutosModel::NOMBRE => 'FUSION SE HYBRID LUX', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2K',  CatalogoAutosModel::NOMBRE => 'FUSION SE HYBRID', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C2P',  CatalogoAutosModel::NOMBRE => 'FUSION SEL HYBRID', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C1A',  CatalogoAutosModel::NOMBRE => 'FUSION SEL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C3B',  CatalogoAutosModel::NOMBRE => 'FUSION S', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],
            [CatalogoAutosModel::CLAVE => 'C4A',  CatalogoAutosModel::NOMBRE => 'FUSION TITANIUM PLUS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 4],

            [CatalogoAutosModel::CLAVE => 'MAC', CatalogoAutosModel::NOMBRE => 'MUSTANG GT COUPE PREM M/T', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MBD', CatalogoAutosModel::NOMBRE => 'MUSTANG V6 COUPE TM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MGE', CatalogoAutosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE TA', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MGF', CatalogoAutosModel::NOMBRE => 'MUSTANG GT CONVERTIBLE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MGL', CatalogoAutosModel::NOMBRE => 'MUSTANG BULLITT ', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MPB', CatalogoAutosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MPD', CatalogoAutosModel::NOMBRE => 'MUSTANG', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MPF', CatalogoAutosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MPG', CatalogoAutosModel::NOMBRE => 'MUSTANG GT PREMIUM COUPE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MPJ', CatalogoAutosModel::NOMBRE => 'MUSTANG I4 ECOBOOST PREMIUM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],
            [CatalogoAutosModel::CLAVE => 'MT7', CatalogoAutosModel::NOMBRE => 'SHELBY GT350 COUPE', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 5],

            [CatalogoAutosModel::CLAVE => 'W7B', CatalogoAutosModel::NOMBRE => 'ECOSPORT TITANIUM AT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 6],
            [CatalogoAutosModel::CLAVE => 'W8B', CatalogoAutosModel::NOMBRE => 'ECOSPORT TREND AT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 6],
            [CatalogoAutosModel::CLAVE => 'W8D', CatalogoAutosModel::NOMBRE => 'ECOSPORT TREND MANUAL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 6],
            [CatalogoAutosModel::CLAVE => 'W9D', CatalogoAutosModel::NOMBRE => 'ECOSPORT ', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 6],

            [CatalogoAutosModel::CLAVE => 'H1J', CatalogoAutosModel::NOMBRE => 'ESCAPE SEL PQ. TITANIUM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H2A', CatalogoAutosModel::NOMBRE => 'ESCAPE SE PLUS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H1H', CatalogoAutosModel::NOMBRE => 'ESCAPE TITANIUM 2.5L', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H2H', CatalogoAutosModel::NOMBRE => 'ESCAPE ADVANCE 2.5', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H2Y', CatalogoAutosModel::NOMBRE => 'ESCAPE ADVANCE 2.0 ECOBOOST', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H3E', CatalogoAutosModel::NOMBRE => 'ESCAPE S PLUS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H3F', CatalogoAutosModel::NOMBRE => 'ESCAPE S', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H5A', CatalogoAutosModel::NOMBRE => 'ESCAPE FWD SE HYBRIDA ', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H5T', CatalogoAutosModel::NOMBRE => 'ESCAPE TITANIUM FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],
            [CatalogoAutosModel::CLAVE => 'H3D', CatalogoAutosModel::NOMBRE => 'ESCAPE S', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 7],

            [CatalogoAutosModel::CLAVE => 'D1B', CatalogoAutosModel::NOMBRE => 'EDGE SEL FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],
            [CatalogoAutosModel::CLAVE => 'D2B', CatalogoAutosModel::NOMBRE => 'EDGE SE FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],
            [CatalogoAutosModel::CLAVE => 'D3B', CatalogoAutosModel::NOMBRE => 'EDGE SEL FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],
            [CatalogoAutosModel::CLAVE => 'D3C', CatalogoAutosModel::NOMBRE => 'EDGE SEL FWD PLUS', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],
            [CatalogoAutosModel::CLAVE => 'D5A', CatalogoAutosModel::NOMBRE => 'EDGE LIMITED FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],
            [CatalogoAutosModel::CLAVE => 'D6S', CatalogoAutosModel::NOMBRE => 'EDGE SPORT FWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 8],

            [CatalogoAutosModel::CLAVE => 'E1A', CatalogoAutosModel::NOMBRE => 'EXPLORER LIMITED 4WD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],
            [CatalogoAutosModel::CLAVE => 'E1B', CatalogoAutosModel::NOMBRE => 'EXPLORER FWD 4DR LTD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],
            [CatalogoAutosModel::CLAVE => 'E2C', CatalogoAutosModel::NOMBRE => 'EXPLORER FWD 4DR XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],
            [CatalogoAutosModel::CLAVE => 'E2D', CatalogoAutosModel::NOMBRE => 'EXPLORER FWD 4DR XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],
            [CatalogoAutosModel::CLAVE => 'E6S', CatalogoAutosModel::NOMBRE => 'EXPLORER', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],
            [CatalogoAutosModel::CLAVE => 'E5S', CatalogoAutosModel::NOMBRE => 'EXPLORER 4DR4WD SPORT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 9],

            [CatalogoAutosModel::CLAVE => 'V2Z', CatalogoAutosModel::NOMBRE => 'EXPEDITION EXP LIMITED MAX 4X2', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 10],
            [CatalogoAutosModel::CLAVE => 'V6A', CatalogoAutosModel::NOMBRE => 'EXPEDITION PLATINUM MAX 4X4', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 10],
            [CatalogoAutosModel::CLAVE => 'V6B', CatalogoAutosModel::NOMBRE => 'EXPEDITION PLATINUM 4X4', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 10],
            [CatalogoAutosModel::CLAVE => 'V1Z', CatalogoAutosModel::NOMBRE => 'EXPEDITION LIMITED 4X2', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 10],

            [CatalogoAutosModel::CLAVE => 'J1D', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1E', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1F', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1L', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1H', CatalogoAutosModel::NOMBRE => 'RANGER', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1R', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1Q', CatalogoAutosModel::NOMBRE => 'RANGER SA REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J1W', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB ', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J2E', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW XLT 4X2', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J3F', CatalogoAutosModel::NOMBRE => 'RANGER SA REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J5E', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB LTD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J5D', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB LTD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J9A', CatalogoAutosModel::NOMBRE => 'RANGER SAFR CREW CB 4X2 XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J9B', CatalogoAutosModel::NOMBRE => 'RANGER SAFR CRW CAB 4X4 XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J9D', CatalogoAutosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X4 XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J9F', CatalogoAutosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'J9G', CatalogoAutosModel::NOMBRE => 'RANGER SAFR CREW CAB 4X2 XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],
            [CatalogoAutosModel::CLAVE => 'JV5', CatalogoAutosModel::NOMBRE => 'RANGER SA CREW CAB XL', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 11],

            [CatalogoAutosModel::CLAVE => 'J7F', CatalogoAutosModel::NOMBRE => 'F150 XL 4X2 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J6B', CatalogoAutosModel::NOMBRE => 'F150 XL 4X2 SUPERCREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J6E', CatalogoAutosModel::NOMBRE => 'F150 XL 4X4 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J6F', CatalogoAutosModel::NOMBRE => 'F-150 XL 4X2 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J8A', CatalogoAutosModel::NOMBRE => 'F-150 XL 4X4 SUPERCREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J8B', CatalogoAutosModel::NOMBRE => 'F-150 XL 4X2 SUPERCREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],
            [CatalogoAutosModel::CLAVE => 'J8F', CatalogoAutosModel::NOMBRE => 'F150 XL 4X2 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 12],

            [CatalogoAutosModel::CLAVE => 'K2A', CatalogoAutosModel::NOMBRE => 'F250 SUPERDUTY SD PICK UP XLT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 13],

            [CatalogoAutosModel::CLAVE => 'K6D', CatalogoAutosModel::NOMBRE => 'F350 XL PLUS CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 14],
            [CatalogoAutosModel::CLAVE => 'K6C', CatalogoAutosModel::NOMBRE => 'F-350 SUPER DUTY XL PLUS CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 14],
            [CatalogoAutosModel::CLAVE => 'K6E', CatalogoAutosModel::NOMBRE => 'F-350 XL PLUS CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 14],
            [CatalogoAutosModel::CLAVE => 'K6F', CatalogoAutosModel::NOMBRE => 'F350 XL CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 14],

            [CatalogoAutosModel::CLAVE => 'K5E', CatalogoAutosModel::NOMBRE => 'F450 XL REG CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 15],
            [CatalogoAutosModel::CLAVE => 'K5T', CatalogoAutosModel::NOMBRE => 'F-450 XL REG CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 15],
            
            [CatalogoAutosModel::CLAVE => 'K8E', CatalogoAutosModel::NOMBRE => 'F-550 XL REG CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 16],
            [CatalogoAutosModel::CLAVE => 'K8T', CatalogoAutosModel::NOMBRE => 'F-550 XL REG CHAS CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 16],

            [CatalogoAutosModel::CLAVE => 'M1F', CatalogoAutosModel::NOMBRE => 'TRANSIT VAN SWB AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M2A', CatalogoAutosModel::NOMBRE => 'TRANSIT VAN MWB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M2E', CatalogoAutosModel::NOMBRE => 'TRANSIT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M2G', CatalogoAutosModel::NOMBRE => 'TRANSIT PASS MWB AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M3J', CatalogoAutosModel::NOMBRE => 'TRANSIT CCMWBAC RWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M3K', CatalogoAutosModel::NOMBRE => 'TRANSIT CC LWB AC RWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M3S', CatalogoAutosModel::NOMBRE => 'TRANSIT CC LWB AC RWD', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M4A', CatalogoAutosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M4M', CatalogoAutosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M4B', CatalogoAutosModel::NOMBRE => 'TRANSIT BUS ELWB AC', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M4H', CatalogoAutosModel::NOMBRE => 'TRANSIT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M6B', CatalogoAutosModel::NOMBRE => 'TRANSIT 350 MR WAGON', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'M7C', CatalogoAutosModel::NOMBRE => 'TRANSIT 250 MR VAN', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'N1A', CatalogoAutosModel::NOMBRE => 'TRANSIT CUSTOM VAN SWB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'N2A', CatalogoAutosModel::NOMBRE => 'TRANSIT CUST PASS LWB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'N2B', CatalogoAutosModel::NOMBRE => 'TRANSIT', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'N3A', CatalogoAutosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],
            [CatalogoAutosModel::CLAVE => 'N3B', CatalogoAutosModel::NOMBRE => 'TRANSIT CUSTOM VAN WB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 17],

            [CatalogoAutosModel::CLAVE => 'G2B', CatalogoAutosModel::NOMBRE => 'LOBO XLT 4X2 SUPERCREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G2A', CatalogoAutosModel::NOMBRE => 'LOBO', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G2E', CatalogoAutosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G2F', CatalogoAutosModel::NOMBRE => 'LOBO XLT 4X4 REG CAB', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G3J', CatalogoAutosModel::NOMBRE => 'LOBO TREMOR 4X4 C REG', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G4B', CatalogoAutosModel::NOMBRE => 'LOBO LRT 4X2 SUPERCREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G4J', CatalogoAutosModel::NOMBRE => 'LOBO LARIAT 4X4 CREW', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G4K', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 PLATINUM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G4S', CatalogoAutosModel::NOMBRE => 'LOBO', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5J', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 KING RANCH', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5L', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 LIMITED', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5N', CatalogoAutosModel::NOMBRE => 'LOBO', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G6J', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 PLATINUM', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5P', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5R', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18],
            [CatalogoAutosModel::CLAVE => 'G5T', CatalogoAutosModel::NOMBRE => 'LOBO 4X4 RAPTOR', CatalogoAutosModel::TIEMPO_LAVADO => 0, CatalogoAutosModel::CATEGORIA_ID => 18] 
        ]; 

        foreach ($autos as $key => $item) {
            DB::table(CatalogoAutosModel::getTableName())->insert([
                CatalogoAutosModel::NOMBRE => $item[CatalogoAutosModel::NOMBRE],
                CatalogoAutosModel::CLAVE => $item[CatalogoAutosModel::CLAVE],
                CatalogoAutosModel::TIEMPO_LAVADO => $item[CatalogoAutosModel::TIEMPO_LAVADO],
                CatalogoAutosModel::CATEGORIA_ID => $item[CatalogoAutosModel::CATEGORIA_ID]
            ]);
        }
    }
}
