<?php

use App\Models\Logistica\CatCarrilesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatCarrilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatCarrilesModel::ID => 1,
                CatCarrilesModel::CARRIL => 'Carril 1'
            ],
            [
                CatCarrilesModel::ID => 2,
                CatCarrilesModel::CARRIL => 'Carril 2'
            ],
            [
                CatCarrilesModel::ID => 3,
                CatCarrilesModel::CARRIL => 'Carril 3'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatCarrilesModel::getTableName())->insert($items);
        }
    }
}
