<?php

use App\Models\Telemarketing\CatalogoOrigenesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrigenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatalogoOrigenesModel::ID => 1,
                CatalogoOrigenesModel::ORIGEN => 'ORIGEN 1',
            ],
            [
                CatalogoOrigenesModel::ID => 2,
                CatalogoOrigenesModel::ORIGEN => 'ORIGEN 2',
            ],
            [
                CatalogoOrigenesModel::ID => 3,
                CatalogoOrigenesModel::ORIGEN => 'ORIGEN 3',
            ],
        );
        foreach ($data as $key => $items) {
            DB::table(CatalogoOrigenesModel::getTableName())->insert($items);
        }
    }
}
