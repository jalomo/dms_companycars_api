<?php

use App\Models\CuentasPorPagar\CatTipoAbonoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoAbonosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos_formas_pagos = [
            [
                CatTipoAbonoModel::ID => 1,
                CatTipoAbonoModel::NOMBRE => 'Enganche'
            ],
            [
                CatTipoAbonoModel::ID => 2,
                CatTipoAbonoModel::NOMBRE => 'Abono'
            ],
            [
                CatTipoAbonoModel::ID => 3,
                CatTipoAbonoModel::NOMBRE => 'Pago una sola exhibición'
            ]
           
        ];

        foreach ($tipos_formas_pagos as $value) {
            DB::table(CatTipoAbonoModel::getTableName())->insert([
                CatTipoAbonoModel::ID => $value[CatTipoAbonoModel::ID],
                CatTipoAbonoModel::NOMBRE => $value[CatTipoAbonoModel::NOMBRE]
            ]);
        }
    }
}
