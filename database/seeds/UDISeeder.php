<?php

use App\Models\Financiamientos\CatUdiModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UDISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatUdiModel::ID => 1,
                CatUdiModel::UDI => 'UDI1',
                CatUdiModel::VALOR => '0'
            ],
            [
                CatUdiModel::ID => 2,
                CatUdiModel::UDI => 'UDI2',
                CatUdiModel::VALOR => '0'
            ],
            [
                CatUdiModel::ID => 3,
                CatUdiModel::UDI => 'UDI3',
                CatUdiModel::VALOR => '0'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatUdiModel::getTableName())->insert($value);
        }
    }
}
