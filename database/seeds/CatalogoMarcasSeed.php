<?php

use App\Models\Refacciones\CatalogoMarcasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoMarcasSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoMarcasModel::NOMBRE => 'FORD'],
            [CatalogoMarcasModel::NOMBRE => 'MERCEDES BENZ'],
            [CatalogoMarcasModel::NOMBRE => 'HYUNDAI'],
            [CatalogoMarcasModel::NOMBRE => 'VOLVO'],
            [CatalogoMarcasModel::NOMBRE => 'TOYOTA'],
            [CatalogoMarcasModel::NOMBRE => 'VOLKSWAGEN'],
            [CatalogoMarcasModel::NOMBRE => 'NISSAN'],
            [CatalogoMarcasModel::NOMBRE => 'INTERNACIONAL'],
            [CatalogoMarcasModel::NOMBRE => 'AUDI'],
            [CatalogoMarcasModel::NOMBRE => 'CHEVROLET'],
            [CatalogoMarcasModel::NOMBRE => 'JEEP'],
            [CatalogoMarcasModel::NOMBRE => 'BMW'],
            [CatalogoMarcasModel::NOMBRE => 'MASA'],
            [CatalogoMarcasModel::NOMBRE => 'HIUNDAY']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoMarcasModel::getTableName())->insert($items);
        }
    }
}
