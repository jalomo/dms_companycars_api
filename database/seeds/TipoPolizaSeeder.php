<?php

use App\Models\Polizas\CatTipoPolizasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoPolizaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatTipoPolizasModel::ID => 1,
                CatTipoPolizasModel::NOMBRE => 'POLIZA VENTAS'
            ],
            // [
            //     CatTipoPolizasModel::ID => 2,
            //     CatTipoPolizasModel::NOMBRE => 'POLIZA DEVOLUCION PROVEEDOR'
            // ],
            // [
            //     CatTipoPolizasModel::ID => 3,
            //     CatTipoPolizasModel::NOMBRE => 'POLIZA OTRAS SALIDAS'
            // ],
            [
                CatTipoPolizasModel::ID => 4,
                CatTipoPolizasModel::NOMBRE => 'POLIZA COMPRAS'
            ],
            [
                CatTipoPolizasModel::ID => 5,
                CatTipoPolizasModel::NOMBRE => 'POLIZA DEVOLUCION MOSTRADOR'
            ],
            [
                CatTipoPolizasModel::ID => 6,
                CatTipoPolizasModel::NOMBRE => 'POLIZA TRASPASOS'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatTipoPolizasModel::getTableName())->insert($items);
        }
    }
}
