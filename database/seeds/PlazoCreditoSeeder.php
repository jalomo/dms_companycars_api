<?php

use App\Models\CuentasPorCobrar\PlazoCreditoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlazoCreditoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                PlazoCreditoModel::ID => 1,
                PlazoCreditoModel::CANTIDAD_MES => 1,
                PlazoCreditoModel::NOMBRE => '1 MES'
            ],
            [
                PlazoCreditoModel::ID => 2,
                PlazoCreditoModel::CANTIDAD_MES => 3,
                PlazoCreditoModel::NOMBRE => '3 MESES'
            ],
            [
                PlazoCreditoModel::ID => 3,
                PlazoCreditoModel::CANTIDAD_MES => 6,
                PlazoCreditoModel::NOMBRE => '6 MESES'
            ],
            [
                PlazoCreditoModel::ID => 4,
                PlazoCreditoModel::CANTIDAD_MES => 9,
                PlazoCreditoModel::NOMBRE => '9 MESES'
            ],
            [
                PlazoCreditoModel::ID => 5,
                PlazoCreditoModel::CANTIDAD_MES => 12,
                PlazoCreditoModel::NOMBRE => '12 MESES'
            ],
            [
                PlazoCreditoModel::ID => 6,
                PlazoCreditoModel::CANTIDAD_MES => 18,
                PlazoCreditoModel::NOMBRE => '18 MESES'
            ],
            [
                PlazoCreditoModel::ID => 7,
                PlazoCreditoModel::CANTIDAD_MES => 24,
                PlazoCreditoModel::NOMBRE => '24 MESES'
            ],
            [
                PlazoCreditoModel::ID => 8,
                PlazoCreditoModel::CANTIDAD_MES => 36,
                PlazoCreditoModel::NOMBRE => '36 MESES'
            ],
            [
                PlazoCreditoModel::ID => 9,
                PlazoCreditoModel::CANTIDAD_MES => 48,
                PlazoCreditoModel::NOMBRE => '48 MESES'
            ],
            [
                PlazoCreditoModel::ID => 10,
                PlazoCreditoModel::CANTIDAD_MES => 60,
                PlazoCreditoModel::NOMBRE => '60 MESES'
            ],
            [
                PlazoCreditoModel::ID => 11,
                PlazoCreditoModel::CANTIDAD_MES => 72,
                PlazoCreditoModel::NOMBRE => '72 MESES'
            ],
            [
                PlazoCreditoModel::ID => 12,
                PlazoCreditoModel::CANTIDAD_MES => 84,
                PlazoCreditoModel::NOMBRE => '84 MESES'
            ],
            [
                PlazoCreditoModel::ID => 13,
                PlazoCreditoModel::CANTIDAD_MES => 96,
                PlazoCreditoModel::NOMBRE => '96 MESES'
            ],
            [
                PlazoCreditoModel::ID => 14,
                PlazoCreditoModel::CANTIDAD_MES => 0,
                PlazoCreditoModel::NOMBRE => 'UNA SOLA EXHIBICIÓN'
            ],
           
        );

        foreach ($data as $key => $items) {
            DB::table(PlazoCreditoModel::getTableName())->insert($items);
        }
    }
}
