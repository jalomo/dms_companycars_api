<?php

use App\Models\Usuarios\UsuariosModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuModulosUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 1
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 2
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 3
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 4
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 5
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 6
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 7
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 8
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 9
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 10
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 11
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 12
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 13
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 14
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 15
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 16
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 17
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 18
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 19
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 20
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 1,
                UsuariosModulosModel::MODULO_ID => 21
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 1
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 2
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 3
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 4
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 5
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 6
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 7
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 8
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 9
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 10
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 11
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 12
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 13
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 14
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 15
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 16
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 17
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 18
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 19
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 20
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 3,
                UsuariosModulosModel::MODULO_ID => 21
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 1
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 2
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 3
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 4
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 5
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 6
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 7
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 8
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 9
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 10
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 11
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 12
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 13
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 14
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 15
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 16
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 17
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 18
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 19
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 20
            ],
            [
                UsuariosModulosModel::USUARIO_ID => 4,
                UsuariosModulosModel::MODULO_ID => 21
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(UsuariosModulosModel::getTableName())->insert($items);
        }
    }
}
