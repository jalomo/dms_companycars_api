<?php

use App\Models\Refacciones\EstatusFacturaModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusFacturaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                EstatusFacturaModel::ID => 1,
                EstatusFacturaModel::NOMBRE => 'CREADA'
            ],
            [
                EstatusFacturaModel::ID => 2,
                EstatusFacturaModel::NOMBRE => 'CANCELADA'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(EstatusFacturaModel::getTableName())->insert($items);
        }
    }
}
