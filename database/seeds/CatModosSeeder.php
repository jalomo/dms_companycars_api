<?php

use App\Models\Logistica\CatModosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatModosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatModosModel::ID => 1,
                CatModosModel::MODO => 'Entrega'
            ],
            [
                CatModosModel::ID => 2,
                CatModosModel::MODO => 'Demostración'
            ],
            [
                CatModosModel::ID => 3,
                CatModosModel::MODO => 'Exhibición'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatModosModel::getTableName())->insert($items);
        }
    }
}
