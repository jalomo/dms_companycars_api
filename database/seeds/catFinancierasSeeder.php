<?php

use App\Models\Financiamientos\CatFinancierasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class catFinancierasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatFinancierasModel::ID => 1,
                CatFinancierasModel::FINANCIERA => 'FINANCIERA 1'
            ],
            [
                CatFinancierasModel::ID => 2,
                CatFinancierasModel::FINANCIERA => 'FINANCIERA 2'
            ]
           
        );

        foreach ($data as $key => $items) {
            DB::table(CatFinancierasModel::getTableName())->insert($items);
        }
    }
}
