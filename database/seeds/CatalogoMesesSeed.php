<?php

use App\Models\Refacciones\MesesModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoMesesSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [MesesModel::NOMBRE => 'Enero'],
            [MesesModel::NOMBRE => 'Febrero'],
            [MesesModel::NOMBRE => 'Marzo'],
            [MesesModel::NOMBRE => 'Abril'],
            [MesesModel::NOMBRE => 'Mayo'],
            [MesesModel::NOMBRE => 'Junio'],
            [MesesModel::NOMBRE => 'Julio'],
            [MesesModel::NOMBRE => 'Agosto'],
            [MesesModel::NOMBRE => 'Septiembre'],
            [MesesModel::NOMBRE => 'Octubre'],
            [MesesModel::NOMBRE => 'Noviembre'],
            [MesesModel::NOMBRE => 'Diciembre']
        ];
       
        foreach ($data as $key => $items) {
            DB::table(MesesModel::getTableName())->insert($items);
        }
    }
}
