<?php

use App\Models\Financiamientos\EstatusFinanciamientosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusFinanciamientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                EstatusFinanciamientosModel::ID => 1,
                EstatusFinanciamientosModel::ESTATUS => 'ANÁLISIS'
            ],
            [
                EstatusFinanciamientosModel::ID => 2,
                EstatusFinanciamientosModel::ESTATUS => 'ANÁLISIS VIAB'
            ],
            [
                EstatusFinanciamientosModel::ID => 3,
                EstatusFinanciamientosModel::ESTATUS => 'APROBADO'
            ],
            [
                EstatusFinanciamientosModel::ID => 4,
                EstatusFinanciamientosModel::ESTATUS => 'APROBADO CANCELADO'
            ],
            [
                EstatusFinanciamientosModel::ID => 5,
                EstatusFinanciamientosModel::ESTATUS => 'BURÓ'
            ],
            [
                EstatusFinanciamientosModel::ID => 6,
                EstatusFinanciamientosModel::ESTATUS => 'COMPRADA'
            ],
            [
                EstatusFinanciamientosModel::ID => 7,
                EstatusFinanciamientosModel::ESTATUS => 'CONDICIONADO'
            ],
            [
                EstatusFinanciamientosModel::ID => 8,
                EstatusFinanciamientosModel::ESTATUS => 'CONTRADO'
            ],
            [
                EstatusFinanciamientosModel::ID => 9,
                EstatusFinanciamientosModel::ESTATUS => 'FACTURADO'
            ],
            [
                EstatusFinanciamientosModel::ID => 10,
                EstatusFinanciamientosModel::ESTATUS => 'RECHAZO'
            ]
        ];
        foreach ($data as $key => $items) {
            DB::table(EstatusFinanciamientosModel::getTableName())->insert($items);
        }
    }
}
