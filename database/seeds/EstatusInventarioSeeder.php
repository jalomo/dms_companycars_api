<?php

use App\Models\Refacciones\EstatusInventarioModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusInventarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                EstatusInventarioModel::ID => 1,
                EstatusInventarioModel::NOMBRE => 'PROCESO'
            ],
            [
                EstatusInventarioModel::ID => 2,
                EstatusInventarioModel::NOMBRE => 'FINALIZADO'
            ],
            [
                EstatusInventarioModel::ID => 3,
                EstatusInventarioModel::NOMBRE => 'CANCELADO'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(EstatusInventarioModel::getTableName())->insert($items);
        }
    }
}
