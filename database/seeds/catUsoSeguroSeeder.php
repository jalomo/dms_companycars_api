<?php

use App\Models\Financiamientos\CatUsoSeguroModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatUsoSeguroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatUsoSeguroModel::ID => 1,
                CatUsoSeguroModel::USO_SEGURO => 'PARTICULAR'
            ],
            [
                CatUsoSeguroModel::ID => 2,
                CatUsoSeguroModel::USO_SEGURO => 'UBER'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatUsoSeguroModel::getTableName())->insert($items);
        }
    }
}
