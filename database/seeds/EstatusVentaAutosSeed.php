<?php

use App\Models\Autos\EstatusVentaAutosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusVentaAutosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                EstatusVentaAutosModel::ID => 1,
                EstatusVentaAutosModel::NOMBRE => 'PRE PEDIDO'
            ],
            [
                EstatusVentaAutosModel::ID => 2,
                EstatusVentaAutosModel::NOMBRE => 'FINALIZADO'
            ],
            [
                EstatusVentaAutosModel::ID => 3,
                EstatusVentaAutosModel::NOMBRE => 'PAGADO'
            ],
            [
                EstatusVentaAutosModel::ID => 4,
                EstatusVentaAutosModel::NOMBRE => 'CANCELADO'
            ],
            [
                EstatusVentaAutosModel::ID => 5,
                EstatusVentaAutosModel::NOMBRE => 'DEVOLUCIÓN'
            ]
        ];

        foreach ($data as $key => $items) {
            DB::table(EstatusVentaAutosModel::getTableName())
                ->insert($items);
        }
    }
}
