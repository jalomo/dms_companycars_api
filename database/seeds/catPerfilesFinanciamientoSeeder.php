<?php

use App\Models\Financiamientos\CatPerfilFinanciamientoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatPerfilesFinanciamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatPerfilFinanciamientoModel::ID => 1,
                CatPerfilFinanciamientoModel::PERFIL => 'BUENO'
            ],
            [
                CatPerfilFinanciamientoModel::ID => 2,
                CatPerfilFinanciamientoModel::PERFIL => 'MALO'
            ],
            [
                CatPerfilFinanciamientoModel::ID => 3,
                CatPerfilFinanciamientoModel::PERFIL => 'POCO HISTORIAL'
            ],
            [
                CatPerfilFinanciamientoModel::ID => 4,
                CatPerfilFinanciamientoModel::PERFIL => 'REGULAR'
            ],
            [
                CatPerfilFinanciamientoModel::ID => 5,
                CatPerfilFinanciamientoModel::PERFIL => 'SIN HISTORIAL'
            ],
           
        );

        foreach ($data as $key => $items) {
            DB::table(CatPerfilFinanciamientoModel::getTableName())->insert($items);
        }
    }
}
