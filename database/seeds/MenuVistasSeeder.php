<?php

use App\Models\Usuarios\MenuVistasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuVistasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::NOMBRE => "Admin modulos",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8,
            ],
            [
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::NOMBRE => "Ver permisos",
                MenuVistasModel::LINK => "verpermisos",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8,
            ],
            [
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::NOMBRE => "Roles",
                MenuVistasModel::LINK => "roles",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8,
            ],
            [
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::NOMBRE => "Usuarios",
                MenuVistasModel::LINK => "administrarUsuarios",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8,
            ],
            [
                MenuVistasModel::MODULO => 'catalogos',
                MenuVistasModel::NOMBRE => "catalogos",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'catalogos',
                MenuVistasModel::SUBMENU_ID  => 7,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Productos",
                MenuVistasModel::LINK => "listado",
                MenuVistasModel::CONTROLADOR => 'productos',
                MenuVistasModel::SUBMENU_ID  => 1,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Stock productos",
                MenuVistasModel::LINK => "stock",
                MenuVistasModel::CONTROLADOR => 'productos',
                MenuVistasModel::SUBMENU_ID  => 1,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Traspaso almacen",
                MenuVistasModel::LINK => "generarTraspaso",
                MenuVistasModel::CONTROLADOR => 'almacenes',
                MenuVistasModel::SUBMENU_ID  => 1,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Reemplazos",
                MenuVistasModel::LINK => "listadoremplazos",
                MenuVistasModel::CONTROLADOR => 'almacenes',
                MenuVistasModel::SUBMENU_ID  => 1,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Listado de ventas",
                MenuVistasModel::LINK => "listadoVentas",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Ventas mostrador",
                MenuVistasModel::LINK => "ventasMostrador",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Otras Salidas Serv. Exce.",
                MenuVistasModel::LINK => "otrasSalidas",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Devolución. A Prov.",
                MenuVistasModel::LINK => "devolucion",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Devoluciones realizadas.",
                MenuVistasModel::LINK => "listadoDevoluciones",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza Ventas",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=1',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza Dev.Prov",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=2',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza Otras Salidas",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas',
                MenuVistasModel::SUBMENU_ID  => 2,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Listado de compras",
                MenuVistasModel::LINK => "listadoEntradas",
                MenuVistasModel::CONTROLADOR => 'entradas',
                MenuVistasModel::SUBMENU_ID  => 6,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Autorizar pedidos",
                MenuVistasModel::LINK => "autorizarpedido",
                MenuVistasModel::CONTROLADOR => 'entradas',
                MenuVistasModel::SUBMENU_ID  => 6,
            ],
            // [
            //     MenuVistasModel::MODULO => 'refacciones',
            //     MenuVistasModel::NOMBRE => "Carga de facturas",
            //     MenuVistasModel::LINK => "ordencompra",
            //     MenuVistasModel::CONTROLADOR => 'entradas',
            //     MenuVistasModel::SUBMENU_ID  => 3,
            // ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Devolucion ventas",
                MenuVistasModel::LINK => "crearDevolucion",
                MenuVistasModel::CONTROLADOR => 'entradas',
                MenuVistasModel::SUBMENU_ID  => 3,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Lista devoluciones",
                MenuVistasModel::LINK => "listadoDevoluciones",
                MenuVistasModel::CONTROLADOR => 'entradas',
                MenuVistasModel::SUBMENU_ID  => 3,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza de Compras",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=4',
                MenuVistasModel::SUBMENU_ID  => 3,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza Dev. Mostrador",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=5',
                MenuVistasModel::SUBMENU_ID  => 3,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Poliza Traspasos",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=6',
                MenuVistasModel::SUBMENU_ID  => 3,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Consultar",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'polizas?tipo_poliza=1',
                MenuVistasModel::SUBMENU_ID  => 101,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Kardex",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'kardex',
                MenuVistasModel::SUBMENU_ID  => 5,
            ],
            [
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::NOMBRE => "Busqueda por folios",
                MenuVistasModel::LINK => "busquedaFoliosTipoCuenTa",
                MenuVistasModel::CONTROLADOR => 'Productos',
                MenuVistasModel::SUBMENU_ID  => 5,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Catalogo autos",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'Unidades',
                MenuVistasModel::SUBMENU_ID  => 11,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Tabla Isan",
                MenuVistasModel::LINK => "entradaTabla",
                MenuVistasModel::CONTROLADOR => 'TablaIsan',
                MenuVistasModel::SUBMENU_ID  => 11,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Recepción",
                MenuVistasModel::LINK => "alta",
                MenuVistasModel::CONTROLADOR => 'Recepcion',
                MenuVistasModel::SUBMENU_ID  => 12,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Listado",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'Recepcion',
                MenuVistasModel::SUBMENU_ID  => 12,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Unidades",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'EquipoAdicional',
                MenuVistasModel::SUBMENU_ID  => 13,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Listado salidas",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'Salidas',
                MenuVistasModel::SUBMENU_ID  => 14,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Nuevo Registro",
                MenuVistasModel::LINK => "agregarOrden",
                MenuVistasModel::CONTROLADOR => 'OrdenEquipo',
                MenuVistasModel::SUBMENU_ID  => 15,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Listado",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'OrdenEquipo',
                MenuVistasModel::SUBMENU_ID  => 15,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Unidades",
                MenuVistasModel::LINK => "unidades",
                MenuVistasModel::CONTROLADOR => 'ventas',
                MenuVistasModel::SUBMENU_ID  => 16,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Pre-pedidos",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'ventas',
                MenuVistasModel::SUBMENU_ID  => 16,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Notas de Cre.",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'NotaCredito',
                MenuVistasModel::SUBMENU_ID  => 16,
            ],
            // [
            //     MenuVistasModel::MODULO => 'autos',
            //     MenuVistasModel::NOMBRE => "Entrega de Unidades",
            //     MenuVistasModel::LINK => "entrega_unidad",
            //     MenuVistasModel::CONTROLADOR => 'ventas',
            //     MenuVistasModel::SUBMENU_ID  => 16,
            // ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Ventas diarias",
                MenuVistasModel::LINK => "reporteventas",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Póliza de ventas",
                MenuVistasModel::LINK => "reportepolizasventas",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Póliza de compras",
                MenuVistasModel::LINK => "reportepolizacompras",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Comisiones",
                MenuVistasModel::LINK => "reportecomisiones",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Existencias",
                MenuVistasModel::LINK => "reporteexistencias",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::NOMBRE => "Utilidad",
                MenuVistasModel::LINK => "reporteutilidad",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 17,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Alta",
                MenuVistasModel::LINK => "alta",
                MenuVistasModel::CONTROLADOR => 'Unidades',
                MenuVistasModel::SUBMENU_ID  => 18,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Listado",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'Unidades',
                MenuVistasModel::SUBMENU_ID  => 18,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Consultas",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'Consultas',
                MenuVistasModel::SUBMENU_ID  => 19,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Consultas",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'ventas',
                MenuVistasModel::SUBMENU_ID  => 20,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Pre-pedidos",
                MenuVistasModel::LINK => "preventa_formulario",
                MenuVistasModel::CONTROLADOR => 'ventas',
                MenuVistasModel::SUBMENU_ID  => 20,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Salidas",
                MenuVistasModel::LINK => "salida_unidad",
                MenuVistasModel::CONTROLADOR => 'ventas',
                MenuVistasModel::SUBMENU_ID  => 20,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Inventario",
                MenuVistasModel::LINK => "reporteinventario",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Pendientes por fac.",
                MenuVistasModel::LINK => "reportependientes",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Ventas Diarias",
                MenuVistasModel::LINK => "reporteventadiarias",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Ventas x Vendedor",
                MenuVistasModel::LINK => "reporteventavendedor",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Poliza de Ventas",
                MenuVistasModel::LINK => "reportepolizasventas",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Poliza de Compras",
                MenuVistasModel::LINK => "reportepolizacompras",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'seminuevos',
                MenuVistasModel::NOMBRE => "Comisiones",
                MenuVistasModel::LINK => "reportecomisiones",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 21,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Cuentas",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'catalogos',
                MenuVistasModel::SUBMENU_ID  => 22,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Estado financiero",
                MenuVistasModel::LINK => "estado_financiero",
                MenuVistasModel::CONTROLADOR => 'catalogos',
                MenuVistasModel::SUBMENU_ID  => 22,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Captura Poliza",
                MenuVistasModel::LINK => "index",
                MenuVistasModel::CONTROLADOR => 'movimientos',
                MenuVistasModel::SUBMENU_ID  => 23,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Facturación",
                MenuVistasModel::LINK => "facturas",
                MenuVistasModel::CONTROLADOR => 'movimientos',
                MenuVistasModel::SUBMENU_ID  => 23,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Polizas",
                MenuVistasModel::LINK => "polizas",
                MenuVistasModel::CONTROLADOR => 'consultas',
                MenuVistasModel::SUBMENU_ID  => 24,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Saldos",
                MenuVistasModel::LINK => "saldos",
                MenuVistasModel::CONTROLADOR => 'consultas',
                MenuVistasModel::SUBMENU_ID  => 24,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Diario de movimientos",
                MenuVistasModel::LINK => "diario",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Mayor mensual",
                MenuVistasModel::LINK => "mayor",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Balanza de Comprobación",
                MenuVistasModel::LINK => "balanza",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Catalogo cuentas",
                MenuVistasModel::LINK => "cuentas",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Historico Mayor",
                MenuVistasModel::LINK => "historico",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Aux. VS Conta",
                MenuVistasModel::LINK => "aux",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Estado financiero",
                MenuVistasModel::LINK => "estado_financiero",
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 25,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Cierre de Mes",
                MenuVistasModel::LINK => "cierre_mes",
                MenuVistasModel::CONTROLADOR => 'utileria',
                MenuVistasModel::SUBMENU_ID  => 26,
            ],
            [
                MenuVistasModel::MODULO => 'contabilidad',
                MenuVistasModel::NOMBRE => "Todos los ejercicios",
                MenuVistasModel::LINK => "ejercicios",
                MenuVistasModel::CONTROLADOR => 'utileria',
                MenuVistasModel::SUBMENU_ID  => 26,
            ],
            [
                MenuVistasModel::MODULO => 'caja',
                MenuVistasModel::NOMBRE => "Pagos",
                MenuVistasModel::LINK => "pagos",
                MenuVistasModel::CONTROLADOR => 'entradas',
                MenuVistasModel::SUBMENU_ID  => 27,
            ],
            [
                MenuVistasModel::MODULO => 'caja',
                MenuVistasModel::NOMBRE => "Pagos",
                MenuVistasModel::LINK => "pagos",
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID  => 28,
            ],
            [
                MenuVistasModel::MODULO => 'caja',
                MenuVistasModel::NOMBRE => "Corte de Caja",
                MenuVistasModel::LINK => "corteCaja",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 29,
            ],
            [
                MenuVistasModel::MODULO => 'caja',
                MenuVistasModel::NOMBRE => "Poliza de Caja",
                MenuVistasModel::LINK => "polizas",
                MenuVistasModel::CONTROLADOR => 'Reportes',
                MenuVistasModel::SUBMENU_ID  => 29,
            ],
            [
                MenuVistasModel::MODULO => 'cxc',
                MenuVistasModel::NOMBRE => "Cuentas",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID  => 30,
            ],
            [
                MenuVistasModel::MODULO => 'cxc',
                MenuVistasModel::NOMBRE => "Relación Cobranza",
                MenuVistasModel::LINK => "relacion_cobranza",
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID  => 30,
            ],
            [
                MenuVistasModel::MODULO => 'cxc',
                MenuVistasModel::NOMBRE => "Gestión Cobranza",
                MenuVistasModel::LINK => "gestion_cobranza",
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID  => 30,
            ],
            [
                MenuVistasModel::MODULO => 'cxp',
                MenuVistasModel::NOMBRE => "Cuentas",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID  => 31,
            ],
            [
                MenuVistasModel::MODULO => 'cxp',
                MenuVistasModel::NOMBRE => "Relación pagos",
                MenuVistasModel::LINK => "relacion_pagos",
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID  => 31,
            ],
            [
                MenuVistasModel::MODULO => 'metricas',
                MenuVistasModel::NOMBRE => "Recursos",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'metricas',
                MenuVistasModel::SUBMENU_ID  => 32
            ],
            [
                MenuVistasModel::MODULO => 'metricas',
                MenuVistasModel::NOMBRE => "Uso",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'uso',
                MenuVistasModel::SUBMENU_ID  => 32
            ],
            [
                MenuVistasModel::NOMBRE => "Actividad",
                MenuVistasModel::LINK => "#",
                MenuVistasModel::CONTROLADOR => 'actividades',
                MenuVistasModel::MODULO => 'metricas',
                MenuVistasModel::SUBMENU_ID  => 32
            ],
            [
                MenuVistasModel::NOMBRE => "Modulos",
                MenuVistasModel::LINK => "modulos",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8
            ],
            [
                MenuVistasModel::NOMBRE => "Permisos vistas",
                MenuVistasModel::LINK => "permisosVista",
                MenuVistasModel::CONTROLADOR => 'usuarios',
                MenuVistasModel::MODULO => 'usuarios',
                MenuVistasModel::SUBMENU_ID  => 8
            ],
            [
                MenuVistasModel::NOMBRE => 'Trabajadores',
                MenuVistasModel::LINK => 'trabajador',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 33,
            ],
            [
                MenuVistasModel::NOMBRE => 'Nómina del trabajador',
                MenuVistasModel::LINK => 'nominaTrabajador',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 33,
            ],
            [
                MenuVistasModel::NOMBRE => 'Consulta de nómina',
                MenuVistasModel::LINK => 'consultanomina',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 33,
            ],
            [
                MenuVistasModel::NOMBRE => 'Reporte de la nómina',
                MenuVistasModel::LINK => 'reportenomina',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 33,
            ],
            [
                MenuVistasModel::NOMBRE => 'Percepciones y deducciones',
                MenuVistasModel::LINK => 'percepcionesdeducciones',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 34,
            ],
            [
                MenuVistasModel::NOMBRE => 'Párametos de nómina',
                MenuVistasModel::LINK => 'parametrosnomina',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 46,
            ],
            [
                MenuVistasModel::NOMBRE => 'Tablas del sistema',
                MenuVistasModel::LINK => 'tablassistema',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::SUBMENU_ID => 36,
            ],
            [
                MenuVistasModel::NOMBRE => 'Departamentos',
                MenuVistasModel::LINK => 'departamentos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::SUBMENU_ID => 37,
            ],
            [
                MenuVistasModel::NOMBRE => 'Puestos',
                MenuVistasModel::LINK => 'puestos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::SUBMENU_ID => 37,
            ],
            [
                MenuVistasModel::NOMBRE => 'Clasificaciones',
                MenuVistasModel::LINK => 'clasificaciones',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::SUBMENU_ID => 37,
            ],
            [
                MenuVistasModel::NOMBRE => 'Listado',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'factura',
                MenuVistasModel::SUBMENU_ID => 4,
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Principal",
                MenuVistasModel::LINK => '#',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => 'combustible',
                MenuVistasModel::SUBMENU_ID => 44,
            ],
            [
                MenuVistasModel::NOMBRE => "Alta de combustible",
                MenuVistasModel::LINK => 'alta_combustible',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => 'combustible',
                MenuVistasModel::SUBMENU_ID => 44
            ],
            [
                MenuVistasModel::NOMBRE => "Panel de servicios",
                MenuVistasModel::LINK => '#',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Asesor",
                MenuVistasModel::LINK => 'https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/Principal_Asesor/4',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Técnico",
                MenuVistasModel::LINK => 'https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/Principal_Tec/4',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Jefe de Taller",
                MenuVistasModel::LINK => 'https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/Principal_JefeTaller/4',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Ventanilla",
                MenuVistasModel::LINK => 'https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/Panel/4',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "Panel Garantías",
                MenuVistasModel::LINK => 'https://sohexdms.net/cs/companycars/servicios_ford/dms_taller/Garantias_Panel/4',
                MenuVistasModel::MODULO => 'servicios',
                MenuVistasModel::CONTROLADOR => '#',
                MenuVistasModel::SUBMENU_ID => 45
            ],
            [
                MenuVistasModel::NOMBRE => "P. Multipunto",
                MenuVistasModel::LINK => 'presupuestoMultipunto',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID => 2
            ],
            [
                MenuVistasModel::NOMBRE => "PSNI Clientes",
                MenuVistasModel::LINK => 'psniClientes',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID => 2
            ],
            [
                MenuVistasModel::NOMBRE => "Poliza Proveedor Pedidos",
                MenuVistasModel::LINK => 'polizapedidoproducto',
                MenuVistasModel::MODULO => 'cxp',
                MenuVistasModel::CONTROLADOR => 'index',
                MenuVistasModel::SUBMENU_ID => 2
            ],
            [
                MenuVistasModel::NOMBRE => "PSNI Garantías",
                MenuVistasModel::LINK => 'psniGarantias',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'salidas',
                MenuVistasModel::SUBMENU_ID => 2
            ],
            [
                MenuVistasModel::NOMBRE => 'Calendarios',
                MenuVistasModel::LINK => 'calendarios',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::SUBMENU_ID => 36,
            ],
            [
                MenuVistasModel::NOMBRE => 'Movimientos a la nómina',
                MenuVistasModel::LINK => 'movientos_nomina',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 35,
            ],
            [
                MenuVistasModel::NOMBRE => 'Horas extras',
                MenuVistasModel::LINK => 'horas_extras',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 35,
            ],
            [
                MenuVistasModel::NOMBRE => 'Vacaciones',
                MenuVistasModel::LINK => 'vacaciones',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 35,
            ],
            [
                MenuVistasModel::NOMBRE => 'Faltas',
                MenuVistasModel::LINK => 'faltas',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::SUBMENU_ID => 35,
            ],
            [
                MenuVistasModel::NOMBRE => 'Agrega Equipo adicional',
                MenuVistasModel::LINK => 'unidadesParaEquipoOpcional',
                MenuVistasModel::MODULO => 'autos',
                MenuVistasModel::CONTROLADOR => 'EquipoAdicional',
                MenuVistasModel::SUBMENU_ID  => 13
            ],
            [
                MenuVistasModel::NOMBRE => 'Consulta recibos electrónicos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'recibos_electronicos',
                MenuVistasModel::SUBMENU_ID  => 47
            ],
            [
                MenuVistasModel::NOMBRE => 'Emisión de recivos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'recibos_electronicos_emicion',
                MenuVistasModel::SUBMENU_ID  => 47
            ],
            [
                MenuVistasModel::NOMBRE => 'Histórico de Recibos Electrónicos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'recibos_electronicos_historico',
                MenuVistasModel::SUBMENU_ID  => 47
            ],
            [
                MenuVistasModel::NOMBRE => 'Tipos de faltas',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::LINK => 'tipos_faltas',
                MenuVistasModel::SUBMENU_ID  => 36
            ],
            [
                MenuVistasModel::NOMBRE => 'Percepciones por base fiscal',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::LINK => 'percepcciones_base_fiscal',
                MenuVistasModel::SUBMENU_ID  => 48
            ],
            [
                MenuVistasModel::NOMBRE => 'Historicos de salarios',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'catalogosconsultas',
                MenuVistasModel::LINK => 'historico_salarios',
                MenuVistasModel::SUBMENU_ID  => 49
            ],
            // [
            //     MenuVistasModel::NOMBRE => 'Tablero',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 53
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Tablero',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 53
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Depósito',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 50
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Enviar',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 51
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Documentos asociados',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 52
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Acumulados de bases fiscales',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 54
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Percepciones por base fiscal',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 55
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Acumulados por Per/Ded',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 55
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Desgloce de Per/Ded',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 55
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Acumulados del trabajador',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 56
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Acumulados de nóminas',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 57
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Conciliación de acumulados',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 58
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Inscripción al RFC',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 59
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Desgloce de ISR en Excel',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 59
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Cálculo anual',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 59
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Acumulados fiscales',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 59
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Impuesto mensual por trabajador',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 59
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Desgloce de SDI',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 60
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'IDSE',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 61
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Disco',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 61
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Enlace SUA',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 62
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Liquidación del IMSS',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 63
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'SAR e INFONAVIT',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 63
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Prima de riesgo',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 63
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Desgloce de IMSS en EXCEL',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 63
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Bases fiscales por percepción',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 64
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Analizar Recibos Electrónicos',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 65
            // ],
            // [
            //     MenuVistasModel::NOMBRE => 'Historicos de Recibos Electrónicos',
            //     MenuVistasModel::MODULO => 'nomina',
            //     MenuVistasModel::CONTROLADOR => 'nomina',
            //     MenuVistasModel::LINK => 'construccion',
            //     MenuVistasModel::SUBMENU_ID  => 65
            // ],
            [
                MenuVistasModel::NOMBRE => 'Datos de la empresa',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'configuracion',
                MenuVistasModel::LINK => 'datos_empresa',
                MenuVistasModel::SUBMENU_ID  => 91
            ],
            [
                MenuVistasModel::NOMBRE => 'Parametros del sistema',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'configuracion',
                MenuVistasModel::LINK => 'datos_empresa_pd',
                MenuVistasModel::SUBMENU_ID  => 91
            ],
            [
                MenuVistasModel::NOMBRE => 'Pedido Sugerido',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'productos',
                MenuVistasModel::LINK => 'pedidoSugerido',
                MenuVistasModel::SUBMENU_ID  => 1
            ],
            [
                MenuVistasModel::NOMBRE => 'Prospectos',
                MenuVistasModel::MODULO => 'ventas_web',
                MenuVistasModel::CONTROLADOR => 'ventas_web',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::SUBMENU_ID  => 96
            ],
            [
                MenuVistasModel::NOMBRE => 'Asesores telemarketing',
                MenuVistasModel::MODULO => 'ventas_web',
                MenuVistasModel::CONTROLADOR => 'ventas_web',
                MenuVistasModel::LINK => 'telemarketing',
                MenuVistasModel::SUBMENU_ID  => 96
            ],
            [
                MenuVistasModel::NOMBRE => 'Asesores ventas',
                MenuVistasModel::MODULO => 'ventas_web',
                MenuVistasModel::CONTROLADOR => 'ventas_web',
                MenuVistasModel::LINK => 'asesores_venta',
                MenuVistasModel::SUBMENU_ID  => 96
            ],
            [
                MenuVistasModel::NOMBRE => 'Tablero asesores',
                MenuVistasModel::MODULO => 'ventas_web',
                MenuVistasModel::CONTROLADOR => 'ventas_web',
                MenuVistasModel::LINK => 'tablero_asesores',
                MenuVistasModel::SUBMENU_ID  => 96
            ],
            [
                MenuVistasModel::NOMBRE => 'Reportes',
                MenuVistasModel::MODULO => 'ventas_web',
                MenuVistasModel::CONTROLADOR => 'ventas_web',
                MenuVistasModel::LINK => 'reportes',
                MenuVistasModel::SUBMENU_ID  => 96
            ],
            [
                MenuVistasModel::NOMBRE => 'Listado Bitácora inicial',
                MenuVistasModel::MODULO => 'financiamientos',
                MenuVistasModel::CONTROLADOR => 'financiamientos',
                MenuVistasModel::LINK => 'listado_bitacora_inicial',
                MenuVistasModel::SUBMENU_ID  => 97
            ],
            [
                MenuVistasModel::NOMBRE => 'Listado Bitácora compras',
                MenuVistasModel::MODULO => 'financiamientos',
                MenuVistasModel::CONTROLADOR => 'financiamientos',
                MenuVistasModel::LINK => 'listado_bitacora_compras',
                MenuVistasModel::SUBMENU_ID  => 97
            ],
            [
                MenuVistasModel::NOMBRE => 'Inventario',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'productos',
                MenuVistasModel::LINK => 'seleccionaInventario',
                MenuVistasModel::SUBMENU_ID  => 1
            ],
            [
                MenuVistasModel::NOMBRE => 'Pedido piezas',
                MenuVistasModel::MODULO => 'refacciones',
                MenuVistasModel::CONTROLADOR => 'productos',
                MenuVistasModel::LINK => 'crearPedido',
                MenuVistasModel::SUBMENU_ID  => 1
            ],
            [
                MenuVistasModel::NOMBRE => 'Periodos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'herramientas',
                MenuVistasModel::LINK => 'periodos',
                MenuVistasModel::SUBMENU_ID  => 83
            ],
            [
                MenuVistasModel::NOMBRE => 'Mis notificaciones',
                MenuVistasModel::MODULO => 'sistemas',
                MenuVistasModel::CONTROLADOR => 'notificaciones',
                MenuVistasModel::LINK => 'mis_notificaciones',
                MenuVistasModel::SUBMENU_ID  => 100
            ],
            [
                MenuVistasModel::NOMBRE => 'Mis contactos',
                MenuVistasModel::MODULO => 'sistemas',
                MenuVistasModel::CONTROLADOR => 'notificaciones',
                MenuVistasModel::LINK => 'mis_contactos',
                MenuVistasModel::SUBMENU_ID  => 100
            ],
            [
                MenuVistasModel::NOMBRE => 'Daños Bodyshop',
                MenuVistasModel::MODULO => 'logistica',
                MenuVistasModel::CONTROLADOR => 'danios',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::SUBMENU_ID  => 98
            ],
            [
                MenuVistasModel::NOMBRE => 'Faltantes',
                MenuVistasModel::MODULO => 'logistica',
                MenuVistasModel::CONTROLADOR => 'faltantes',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::SUBMENU_ID  => 98
            ],
            [
                MenuVistasModel::NOMBRE => 'Sas seguros',
                MenuVistasModel::MODULO => 'logistica',
                MenuVistasModel::CONTROLADOR => 'sasseguros',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::SUBMENU_ID  => 98
            ],
            [
                MenuVistasModel::NOMBRE => 'Logística',
                MenuVistasModel::MODULO => 'logistica',
                MenuVistasModel::CONTROLADOR => 'logistica',
                MenuVistasModel::LINK => 'listado',
                MenuVistasModel::SUBMENU_ID  => 99
            ],
            [
                MenuVistasModel::NOMBRE => 'Tablero',
                MenuVistasModel::MODULO => 'logistica',
                MenuVistasModel::CONTROLADOR => 'logistica',
                MenuVistasModel::LINK => 'tablero',
                MenuVistasModel::SUBMENU_ID  => 99
            ],
            [
                MenuVistasModel::NOMBRE => 'Horas extras',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::LINK => 'horas_extras',
                MenuVistasModel::SUBMENU_ID  => 68
            ],
            [
                MenuVistasModel::NOMBRE => 'Movimientos por trabajador',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::LINK => 'movimientos_trabajador',
                MenuVistasModel::SUBMENU_ID  => 68
            ],
            [
                MenuVistasModel::NOMBRE => 'Vacaciones',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'reportes',
                MenuVistasModel::LINK => 'Vacaciones',
                MenuVistasModel::SUBMENU_ID  => 68
            ],
            [
                MenuVistasModel::NOMBRE => 'Trabajadores Activos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'tablero_nomina_activos',
                MenuVistasModel::SUBMENU_ID  => 102
            ],
            [
                MenuVistasModel::NOMBRE => 'Percepciones',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'tablero_total_percepciones',
                MenuVistasModel::SUBMENU_ID  => 102
            ],
            [
                MenuVistasModel::NOMBRE => 'Recibos Electrónicos',
                MenuVistasModel::MODULO => 'nomina',
                MenuVistasModel::CONTROLADOR => 'inicio',
                MenuVistasModel::LINK => 'tablero_total_recibos',
                MenuVistasModel::SUBMENU_ID  => 102
            ],
            
        );

        foreach ($data as $key => $items) {
            DB::table(MenuVistasModel::getTableName())
                ->insert($items);
        }
    }
}
