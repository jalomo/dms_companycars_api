<?php

use App\Models\Logistica\CatEstatusSasSegurosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatEstatusSasSegurosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatEstatusSasSegurosModel::ID => 1,
                CatEstatusSasSegurosModel::ESTATUS => 'Estatus 1'
            ],
            [
                CatEstatusSasSegurosModel::ID => 2,
                CatEstatusSasSegurosModel::ESTATUS => 'Estatus 2'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(CatEstatusSasSegurosModel::getTableName())->insert($items);
        }
    }
}
