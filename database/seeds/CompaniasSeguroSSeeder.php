<?php

use App\Models\Financiamientos\CatCompaniaSeguroModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompaniasSeguroSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatCompaniaSeguroModel::ID => 1,
                CatCompaniaSeguroModel::COMPANIA => 'Zurich',
                CatCompaniaSeguroModel::VALOR => '0'
            ],
            [
                CatCompaniaSeguroModel::ID => 2,
                CatCompaniaSeguroModel::COMPANIA => 'Qualitas',
                CatCompaniaSeguroModel::VALOR => '0'
            ],
            [
                CatCompaniaSeguroModel::ID => 3,
                CatCompaniaSeguroModel::COMPANIA => 'ABA',
                CatCompaniaSeguroModel::VALOR => '0'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatCompaniaSeguroModel::getTableName())->insert($value);
        }
    }
}
