<?php

use App\Models\Logistica\CatEstatusAdmvoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstatusAdmvosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatEstatusAdmvoModel::ID => 1,
                CatEstatusAdmvoModel::ESTATUS => 'Facturado'
            ],
            [
                CatEstatusAdmvoModel::ID => 2,
                CatEstatusAdmvoModel::ESTATUS => 'En existencia'
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(CatEstatusAdmvoModel::getTableName())->insert($items);
        }
    }
}
