<?php

use App\Models\Logistica\CatAgenciasModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatAgenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                CatAgenciasModel::ID => 1,
                CatAgenciasModel::AGENCIA => 'Matriz'
            ],
            [
                CatAgenciasModel::ID => 2,
                CatAgenciasModel::AGENCIA => 'Sucursal'
            ],
        );

        foreach ($data as $key => $items) {
            DB::table(CatAgenciasModel::getTableName())->insert($items);
        }
    }
}
