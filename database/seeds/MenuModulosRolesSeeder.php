<?php

use App\Models\Usuarios\RolesModulosModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuModulosRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 1
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 2
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 3
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 4
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 5
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 6
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 7
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 8
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 9
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 10
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 11
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 12
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 13
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 14
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 15
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 16
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 17
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 18
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 19
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 20
            ],
            [
                RolesModulosModel::ROL_ID => 1,
                RolesModulosModel::MODULO_ID => 21
            ],
            [
                RolesModulosModel::ROL_ID => 10,
                RolesModulosModel::MODULO_ID => 1
            ]
        );

        foreach ($data as $key => $items) {
            DB::table(RolesModulosModel::getTableName())->insert($items);
        }
    }
}
