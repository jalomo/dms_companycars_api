<?php

use App\Models\Refacciones\CatCfdiModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CfdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $almacenes = [
            [
                CatCfdiModel::ID => 1,
                CatCfdiModel::CLAVE => 'G01',
                CatCfdiModel::DESCRIPCION => 'Adquisición de mercancias',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,

            ],
            [
                CatCfdiModel::ID => 2,
                CatCfdiModel::CLAVE => 'G02',
                CatCfdiModel::DESCRIPCION => 'Devoluciones, descuentos o bonificaciones',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 3,
                CatCfdiModel::CLAVE => 'G03',
                CatCfdiModel::DESCRIPCION => 'Gastos en general',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 4,
                CatCfdiModel::CLAVE => 'I01',
                CatCfdiModel::DESCRIPCION => 'Construcciones',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 5,
                CatCfdiModel::CLAVE => 'I02',
                CatCfdiModel::DESCRIPCION => 'Mobilario y equipo de oficina por inversiones',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 6,
                CatCfdiModel::CLAVE => 'I03',
                CatCfdiModel::DESCRIPCION => 'Equipo de transporte',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 7,
                CatCfdiModel::CLAVE => 'I04',
                CatCfdiModel::DESCRIPCION => 'Equipo de computo y accesorios',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 8,
                CatCfdiModel::CLAVE => 'I05',
                CatCfdiModel::DESCRIPCION => 'Dados, troqueles, moldes, matrices y herramental',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 9,
                CatCfdiModel::CLAVE => 'I06',
                CatCfdiModel::DESCRIPCION => 'Comunicaciones telefónicas',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 10,
                CatCfdiModel::CLAVE => 'I07',
                CatCfdiModel::DESCRIPCION => 'Comunicaciones satelitales',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 11,
                CatCfdiModel::CLAVE => 'I08',
                CatCfdiModel::DESCRIPCION => 'Otra maquinaria y equipo',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 12,
                CatCfdiModel::CLAVE => 'D01',
                CatCfdiModel::DESCRIPCION => 'Honorarios médicos, dentales y gastos hospitalarios.',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 13,
                CatCfdiModel::CLAVE => 'I08',
                CatCfdiModel::DESCRIPCION => 'Gastos médicos por incapacidad o discapacidad',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 14,
                CatCfdiModel::CLAVE => 'D03',
                CatCfdiModel::DESCRIPCION => 'Gastos funerales.',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 15,
                CatCfdiModel::CLAVE => 'D04',
                CatCfdiModel::DESCRIPCION => 'Donativos',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 16,
                CatCfdiModel::CLAVE => 'D05',
                CatCfdiModel::DESCRIPCION => 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 17,
                CatCfdiModel::CLAVE => 'D06',
                CatCfdiModel::DESCRIPCION => 'Aportaciones voluntarias al SAR',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 18,
                CatCfdiModel::CLAVE => 'D07',
                CatCfdiModel::DESCRIPCION => 'Primas por seguros de gastos médicos.',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 19,
                CatCfdiModel::CLAVE => 'D08',
                CatCfdiModel::DESCRIPCION => 'Gastos de transportación escolar obligatoria',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 20,
                CatCfdiModel::CLAVE => 'D09',
                CatCfdiModel::DESCRIPCION => 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 21,
                CatCfdiModel::CLAVE => 'D10',
                CatCfdiModel::DESCRIPCION => 'Pagos por servicios educativos (colegiaturas)',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ],
            [
                CatCfdiModel::ID => 22,
                CatCfdiModel::CLAVE => 'P01',
                CatCfdiModel::DESCRIPCION => 'Por definir',
                CatCfdiModel::PERSONA_MORAL => 1,
                CatCfdiModel::PERSONA_FISICA => 1,
            ]
        ];

        foreach ($almacenes as $key => $value) {
            DB::table(CatCfdiModel::getTableName())->insert([
                CatCfdiModel::ID => $value[CatCfdiModel::ID],
                CatCfdiModel::DESCRIPCION => $value[CatCfdiModel::DESCRIPCION],
                CatCfdiModel::CLAVE => $value[CatCfdiModel::CLAVE],
                CatCfdiModel::PERSONA_FISICA => $value[CatCfdiModel::PERSONA_FISICA],
                CatCfdiModel::PERSONA_MORAL => $value[CatCfdiModel::PERSONA_MORAL],
            ]);
        }
    }
}
