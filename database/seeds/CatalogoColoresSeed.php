<?php

use App\Models\Refacciones\CatalogoColoresModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CatalogoColoresSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data =  [
            [CatalogoColoresModel::NOMBRE => 'AZUL BRILLANTE', CatalogoColoresModel::CLAVE => '".3'],
            [CatalogoColoresModel::NOMBRE => 'AZUL BRILLANTE TINT', CatalogoColoresModel::CLAVE => 'LC'],
            [CatalogoColoresModel::NOMBRE => 'AZUL IMPACTO', CatalogoColoresModel::CLAVE => '*5'],
            [CatalogoColoresModel::NOMBRE => 'AZUL DINAMICO', CatalogoColoresModel::CLAVE => '/?'],
            [CatalogoColoresModel::NOMBRE => 'AZUL ESTELAR METALIC', CatalogoColoresModel::CLAVE => 'ÑB'],
            [CatalogoColoresModel::NOMBRE => 'AZUL NITRO', CatalogoColoresModel::CLAVE => '/2'],
            [CatalogoColoresModel::NOMBRE => 'AZUL MARINO BRILLANT', CatalogoColoresModel::CLAVE => 'AÑ'],
            [CatalogoColoresModel::NOMBRE => 'AZUL METALICO', CatalogoColoresModel::CLAVE => '5'],
            [CatalogoColoresModel::NOMBRE => 'AZUL METROPOLIS', CatalogoColoresModel::CLAVE => 'ZE'],
            [CatalogoColoresModel::NOMBRE => 'AZUL METROPOLIS METAL', CatalogoColoresModel::CLAVE => 'B0'],
            [CatalogoColoresModel::NOMBRE => 'AZUL OCEANO', CatalogoColoresModel::CLAVE => 'KC'],
            [CatalogoColoresModel::NOMBRE => 'AZUL PRUSIA', CatalogoColoresModel::CLAVE => '*/'],
            [CatalogoColoresModel::NOMBRE => 'AZUL PERFORMANCE', CatalogoColoresModel::CLAVE => 'AV'],
            [CatalogoColoresModel::NOMBRE => 'AZUL COBALTO', CatalogoColoresModel::CLAVE => 'A8'],
            [CatalogoColoresModel::NOMBRE => 'AZUL RELAMPAGO', CatalogoColoresModel::CLAVE => './3'],
            [CatalogoColoresModel::NOMBRE => 'AZUL RELAMPAGO METAL', CatalogoColoresModel::CLAVE => '7'],
            [CatalogoColoresModel::NOMBRE => 'AZUL ELECTRICO', CatalogoColoresModel::CLAVE => 'B6'],
            [CatalogoColoresModel::NOMBRE => 'ARENA INVERNAL', CatalogoColoresModel::CLAVE => 'ÑG'],
            [CatalogoColoresModel::NOMBRE => 'ARENA ESTELAR', CatalogoColoresModel::CLAVE => 'ÑW'],
            [CatalogoColoresModel::NOMBRE => 'AMARILLO AMANECER', CatalogoColoresModel::CLAVE => 'ÑC'],
            [CatalogoColoresModel::NOMBRE => 'AMARILLO DEPORTIVO', CatalogoColoresModel::CLAVE => 'ÑQ'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO BRILLANTE', CatalogoColoresModel::CLAVE => 'BS'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO DIAMANTE', CatalogoColoresModel::CLAVE => 'AD'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO NIEVE', CatalogoColoresModel::CLAVE => 'BU'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO NIEVE SOLIDO', CatalogoColoresModel::CLAVE => 'A7'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO METALICO', CatalogoColoresModel::CLAVE => 'Wa'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO OXFORD', CatalogoColoresModel::CLAVE => '0I'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO OXFORD BC', CatalogoColoresModel::CLAVE => '4'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO PLATINADO', CatalogoColoresModel::CLAVE => 'JI'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO PLATINADO MET', CatalogoColoresModel::CLAVE => 'HE'],
            [CatalogoColoresModel::NOMBRE => 'BLANCO PLATINADO TRI', CatalogoColoresModel::CLAVE => '*9'],
            [CatalogoColoresModel::NOMBRE => 'BRONCE METALICO', CatalogoColoresModel::CLAVE => '.+2'],
            [CatalogoColoresModel::NOMBRE => 'BRONCE METALICO BRIL', CatalogoColoresModel::CLAVE => '2'],
            [CatalogoColoresModel::NOMBRE => 'GRIS CUARZO', CatalogoColoresModel::CLAVE => '3'],
            [CatalogoColoresModel::NOMBRE => 'GRIS IMAN', CatalogoColoresModel::CLAVE => 'ÑÑ'],
            [CatalogoColoresModel::NOMBRE => 'GRIS HIERRO', CatalogoColoresModel::CLAVE => '22'],
            [CatalogoColoresModel::NOMBRE => 'GRIS MINERAL', CatalogoColoresModel::CLAVE => '0.9'],
            [CatalogoColoresModel::NOMBRE => 'GRIS MILITAR', CatalogoColoresModel::CLAVE => '9R'],
            [CatalogoColoresModel::NOMBRE => 'GRIS MERCURIO', CatalogoColoresModel::CLAVE => 'AK'],
            [CatalogoColoresModel::NOMBRE => 'GRIS MERCURIO METALI', CatalogoColoresModel::CLAVE => '8'],
            [CatalogoColoresModel::NOMBRE => 'GRIS NOCTURNO MET', CatalogoColoresModel::CLAVE => '*-'],
            [CatalogoColoresModel::NOMBRE => 'GRIS NOCTURNO METALI', CatalogoColoresModel::CLAVE => 'TF'],
            [CatalogoColoresModel::NOMBRE => 'GRIS PIEDRA', CatalogoColoresModel::CLAVE => '9'],
            [CatalogoColoresModel::NOMBRE => 'MENTA', CatalogoColoresModel::CLAVE => 'ÑN'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA DINAMICO', CatalogoColoresModel::CLAVE => '1Z'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA ENCENDIDO', CatalogoColoresModel::CLAVE => 'ÑD'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA FLAMA', CatalogoColoresModel::CLAVE => 'Ñ0'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA FURIA', CatalogoColoresModel::CLAVE => '21'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA METALICO', CatalogoColoresModel::CLAVE => '0'],
            [CatalogoColoresModel::NOMBRE => 'NARANJA TORNADO', CatalogoColoresModel::CLAVE => '.+3'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO', CatalogoColoresModel::CLAVE => '1D'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO APERLADO', CatalogoColoresModel::CLAVE => '0A'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO APERLADO BC', CatalogoColoresModel::CLAVE => 'N%'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO BRILLANTE', CatalogoColoresModel::CLAVE => '9A'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO OCASO', CatalogoColoresModel::CLAVE => '55'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO OBSIDIANA', CatalogoColoresModel::CLAVE => 'ÑT'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO PROFUNDO', CatalogoColoresModel::CLAVE => 'EA'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO PROFUNDO MET', CatalogoColoresModel::CLAVE => 'D1'],
            [CatalogoColoresModel::NOMBRE => 'NEGRO PROFUNDO METAL', CatalogoColoresModel::CLAVE => 'D2'],
            [CatalogoColoresModel::NOMBRE => 'ORO', CatalogoColoresModel::CLAVE => 'OZ'],
            [CatalogoColoresModel::NOMBRE => 'ORO BLANCO', CatalogoColoresModel::CLAVE => '35'],
            [CatalogoColoresModel::NOMBRE => 'ORO ELECTRICO', CatalogoColoresModel::CLAVE => 'O4'],
            [CatalogoColoresModel::NOMBRE => 'ROJO BRILLANTE', CatalogoColoresModel::CLAVE => 'E5'],
            [CatalogoColoresModel::NOMBRE => 'ROJO CEREZA', CatalogoColoresModel::CLAVE => 'ÑZ'],
            [CatalogoColoresModel::NOMBRE => 'ROJO GRANATE', CatalogoColoresModel::CLAVE => 'R8'],
            [CatalogoColoresModel::NOMBRE => 'ROJO MARTE', CatalogoColoresModel::CLAVE => 'Ñ1'],
            [CatalogoColoresModel::NOMBRE => 'ROJO MANZANA', CatalogoColoresModel::CLAVE => 'MZ'],
            [CatalogoColoresModel::NOMBRE => 'ROJO MEXICANO', CatalogoColoresModel::CLAVE => 'CÑ'],
            [CatalogoColoresModel::NOMBRE => 'ROJO PIMIENTA', CatalogoColoresModel::CLAVE => '78'],
            [CatalogoColoresModel::NOMBRE => 'ROJO RACING', CatalogoColoresModel::CLAVE => 'R5'],
            [CatalogoColoresModel::NOMBRE => 'ROJO RUBI', CatalogoColoresModel::CLAVE => 'E7'],
            [CatalogoColoresModel::NOMBRE => 'ROJO RUBI BC', CatalogoColoresModel::CLAVE => 'Ñ+'],
            [CatalogoColoresModel::NOMBRE => 'ROJO RUBI METALICO', CatalogoColoresModel::CLAVE => 'B'],
            [CatalogoColoresModel::NOMBRE => 'ROJO SOLIDO', CatalogoColoresModel::CLAVE => 'E0'],
            [CatalogoColoresModel::NOMBRE => 'PLATA', CatalogoColoresModel::CLAVE => 'BB'],
            [CatalogoColoresModel::NOMBRE => 'PLATA ASTRAL', CatalogoColoresModel::CLAVE => 'T6'],
            [CatalogoColoresModel::NOMBRE => 'PLATA ESTELAR', CatalogoColoresModel::CLAVE => 'b'],
            [CatalogoColoresModel::NOMBRE => 'PLATA ESTELAR METALI', CatalogoColoresModel::CLAVE => 'P.'],
            [CatalogoColoresModel::NOMBRE => 'PLATA ESTELAR MET', CatalogoColoresModel::CLAVE => '1A'],
            [CatalogoColoresModel::NOMBRE => 'PLATA ESTELAR MET BC', CatalogoColoresModel::CLAVE => 'L1'],
            [CatalogoColoresModel::NOMBRE => 'PLATA METALICO', CatalogoColoresModel::CLAVE => '.I'],
            [CatalogoColoresModel::NOMBRE => 'PLATA METALICO MET', CatalogoColoresModel::CLAVE => 'FR'],
            [CatalogoColoresModel::NOMBRE => 'PLATA METALICO BC', CatalogoColoresModel::CLAVE => 'JY'],
            [CatalogoColoresModel::NOMBRE => 'PLATA TEC METALICO', CatalogoColoresModel::CLAVE => 'NR'],
            [CatalogoColoresModel::NOMBRE => 'PLATA OSTION BC', CatalogoColoresModel::CLAVE => 'Ñ7'],
            [CatalogoColoresModel::NOMBRE => 'PLATA OSTION', CatalogoColoresModel::CLAVE => 'OP'],
            [CatalogoColoresModel::NOMBRE => 'VINO VULCANO', CatalogoColoresModel::CLAVE => '6'],
            [CatalogoColoresModel::NOMBRE => 'VINO TINTO', CatalogoColoresModel::CLAVE => 'D6'],
            [CatalogoColoresModel::NOMBRE => 'VERDE BULLITT', CatalogoColoresModel::CLAVE => '0C']
        ];

        foreach ($data as $key => $items) {
            DB::table(CatalogoColoresModel::getTableName())->insert($items);
        }
    }
}
