<?php

use Illuminate\Database\Seeder;
use App\Models\Nomina\CaTipoEmpleadoModel as Model;
use Illuminate\Support\Facades\DB;

class CaTipoEmpleadoSeeder extends Seeder
{
    private function seedData(){
        return [
            [Model::ID=>1, Model::Descripcion => 'Sindicalizado'],
            [Model::ID=>2, Model::Descripcion => 'Confianza']
        ];
    }

    public function run()
    {
        foreach ($this->seedData() as $key => $items) {
            $exists = DB::connection(Model::connectionName())->table(Model::getTableName())->whereNotNull(MODEL::ID)->where(MODEL::ID, $items[MODEL::ID])->first();
            if($exists == false){
                DB::connection(Model::connectionName())->table(Model::getTableName())->insert($items);
            }
        }
    }
}
