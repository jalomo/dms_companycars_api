<?php

use App\Models\Financiamientos\CatComisionModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComisionFinanciamientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                CatComisionModel::ID => 1,
                CatComisionModel::COMISION => '0%',
                CatComisionModel::VALOR => '0'
            ],
            [
                CatComisionModel::ID => 2,
                CatComisionModel::COMISION => '0.5%',
                CatComisionModel::VALOR => '0.5'
            ],
            [
                CatComisionModel::ID => 3,
                CatComisionModel::COMISION => '1%',
                CatComisionModel::VALOR => '1'
            ],
            [
                CatComisionModel::ID => 4,
                CatComisionModel::COMISION => '1.5%',
                CatComisionModel::VALOR => '1.5'
            ],
            [
                CatComisionModel::ID => 5,
                CatComisionModel::COMISION => '2%',
                CatComisionModel::VALOR => '2'
            ],
            [
                CatComisionModel::ID => 6,
                CatComisionModel::COMISION => '3%',
                CatComisionModel::VALOR => '3'
            ],
            [
                CatComisionModel::ID => 7,
                CatComisionModel::COMISION => '4%',
                CatComisionModel::VALOR => '4'
            ],
            [
                CatComisionModel::ID => 8,
                CatComisionModel::COMISION => '5%',
                CatComisionModel::VALOR => '5'
            ],
           
        ];

        foreach ($data as $value) {
            DB::table(CatComisionModel::getTableName())->insert($value);
        }
    }
}
