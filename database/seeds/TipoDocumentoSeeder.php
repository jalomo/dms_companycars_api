<?php

use App\Models\CuentasPorCobrar\TipoDocumentoCreditoModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoDocumentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipo_documentos = [
            [TipoDocumentoCreditoModel::ID => 1, TipoDocumentoCreditoModel::DESCRIPCION => 'Identificación vigente'],
            [TipoDocumentoCreditoModel::ID => 2, TipoDocumentoCreditoModel::DESCRIPCION => 'Recibos de nómina'],
            [TipoDocumentoCreditoModel::ID => 3, TipoDocumentoCreditoModel::DESCRIPCION => 'Comprobante de domicilio'],
            [TipoDocumentoCreditoModel::ID => 4, TipoDocumentoCreditoModel::DESCRIPCION => 'Solicitud de credito firmada']
        ];

        foreach ($tipo_documentos as $key => $value) {
            DB::table(TipoDocumentoCreditoModel::getTableName())->insert([
                TipoDocumentoCreditoModel::DESCRIPCION => $value[TipoDocumentoCreditoModel::DESCRIPCION]
            ]);
        }
    }
}
