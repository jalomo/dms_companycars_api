<?php

namespace App\Servicios\Telemarketing;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Refacciones\ServicioClientes;
use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\UnidadesModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Telemarketing\CatalogoOrigenesModel;
use App\Models\Telemarketing\CitasVentasModel;
use App\Models\Telemarketing\VentasWebModel;
use App\Models\Usuarios\User;
use App\Servicios\Usuarios\ServicioUsuarios;

class ServicioVentasWeb extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas web';
        $this->modelo = new VentasWebModel();
        $this->modelo_user = new User();
        $this->modelo_ventas = new VentasWebModel();
        $this->servicio_usuarios = new ServicioUsuarios();
        $this->servicio_clientes = new ServicioClientes();
    }

    public function getReglasGuardar()
    {
        return [
            VentasWebModel::ID_CLIENTE => 'required',
            VentasWebModel::ID_UNIDAD => 'required',
            VentasWebModel::ID_USUARIO_CREO => 'required',
            VentasWebModel::ID_ASESOR_WEB_ASIGNADO => 'nullable',
            VentasWebModel::ID_USUARIO_TELEMARKETING => 'nullable',
            VentasWebModel::FECHA_CITA_PROGRAMADA => 'nullable',
            VentasWebModel::INTENTOS => 'nullable',
            VentasWebModel::NO_CONTACTAR => 'nullable',
            VentasWebModel::ID_ORIGEN => 'required',
            VentasWebModel::VENDIDO => 'nullable',
            VentasWebModel::COMENTARIO_VENTA => 'nullable',
            VentasWebModel::FECHA_COMENTARIO => 'nullable',
            VentasWebModel::INFORMACION_ERRONEA => 'nullable',
        ];
    }
    public function getReglasGuardarClienteNoExiste()
    {
        return [
            ClientesModel::NOMBRE => 'required',
            ClientesModel::APELLIDO_PATERNO => 'required',
            ClientesModel::APELLIDO_MATERNO => 'required',
            ClientesModel::TELEFONO => 'required',
            ClientesModel::TELEFONO_2 => 'nullable',
            ClientesModel::CORREO_ELECTRONICO => 'required',
            VentasWebModel::ID_UNIDAD => 'required',
            VentasWebModel::ID_USUARIO_CREO => 'required',
            VentasWebModel::ID_ASESOR_WEB_ASIGNADO => 'nullable',
            VentasWebModel::ID_USUARIO_TELEMARKETING => 'nullable',
            VentasWebModel::FECHA_CITA_PROGRAMADA => 'nullable',
            VentasWebModel::INTENTOS => 'nullable',
            VentasWebModel::NO_CONTACTAR => 'nullable',
            VentasWebModel::ID_ORIGEN => 'required',
            VentasWebModel::VENDIDO => 'nullable',
            VentasWebModel::COMENTARIO_VENTA => 'nullable',
            VentasWebModel::FECHA_COMENTARIO => 'nullable',
            VentasWebModel::INFORMACION_ERRONEA => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentasWebModel::ID_CLIENTE => 'required',
            VentasWebModel::ID_UNIDAD => 'required',
            VentasWebModel::ID_USUARIO_CREO => 'required',
            VentasWebModel::ID_ASESOR_WEB_ASIGNADO => 'nullable',
            VentasWebModel::ID_USUARIO_TELEMARKETING => 'nullable',
            VentasWebModel::FECHA_CITA_PROGRAMADA => 'nullable',
            VentasWebModel::INTENTOS => 'nullable',
            VentasWebModel::NO_CONTACTAR => 'nullable',
            VentasWebModel::ID_ORIGEN => 'required',
            VentasWebModel::VENDIDO => 'nullable',
            VentasWebModel::COMENTARIO_VENTA => 'nullable',
            VentasWebModel::FECHA_COMENTARIO => 'nullable',
            VentasWebModel::INFORMACION_ERRONEA => 'nullable',
        ];
    }
    public function getReglasCustomUpdate()
    {
        return [
            VentasWebModel::ID_CLIENTE => 'nullable',
            VentasWebModel::ID_UNIDAD => 'nullable',
            VentasWebModel::ID_USUARIO_CREO => 'nullable',
            VentasWebModel::ID_ASESOR_WEB_ASIGNADO => 'nullable',
            VentasWebModel::ID_USUARIO_TELEMARKETING => 'nullable',
            VentasWebModel::FECHA_CITA_PROGRAMADA => 'nullable',
            VentasWebModel::INTENTOS => 'nullable',
            VentasWebModel::NO_CONTACTAR => 'nullable',
            VentasWebModel::ID_ORIGEN => 'nullable',
            VentasWebModel::VENDIDO => 'nullable',
            VentasWebModel::COMENTARIO_VENTA => 'nullable',
            VentasWebModel::FECHA_COMENTARIO => 'nullable',
            VentasWebModel::INFORMACION_ERRONEA => 'nullable',
        ];
    }
    public function store($parametros)
    {
        if (isset($parametros[VentasWebModel::ID_CLIENTE])) {
            return $this->crear($parametros);
        } else {
            //Guardar al cliente
            $cliente = $this->servicio_clientes->crear([
                ClientesModel::NOMBRE => $parametros[ClientesModel::NOMBRE],
                ClientesModel::APELLIDO_PATERNO => $parametros[ClientesModel::APELLIDO_PATERNO],
                ClientesModel::APELLIDO_MATERNO => $parametros[ClientesModel::APELLIDO_MATERNO],
                ClientesModel::TELEFONO => $parametros[ClientesModel::TELEFONO],
                ClientesModel::TELEFONO_2 => isset($parametros[ClientesModel::TELEFONO_2]) ? $parametros[ClientesModel::TELEFONO_2] : '',
                ClientesModel::CORREO_ELECTRONICO => $parametros[ClientesModel::CORREO_ELECTRONICO],
                ClientesModel::ES_CLIENTE => 0,
            ]);
            //Insertar 
            return $this->crear([
                VentasWebModel::ID_CLIENTE => $cliente->id,
                VentasWebModel::ID_UNIDAD => $parametros[VentasWebModel::ID_UNIDAD],
                VentasWebModel::ID_USUARIO_CREO => $parametros[VentasWebModel::ID_USUARIO_CREO],
                VentasWebModel::ID_ASESOR_WEB_ASIGNADO => $parametros[VentasWebModel::ID_ASESOR_WEB_ASIGNADO],
                VentasWebModel::ID_USUARIO_TELEMARKETING => $parametros[VentasWebModel::ID_USUARIO_TELEMARKETING],
                VentasWebModel::ID_ORIGEN => $parametros[VentasWebModel::ID_ORIGEN]
            ]);
        }
    }
    public function updateClienteVenta($parametros, $id)
    {
        if (!isset($parametros[VentasWebModel::ID_CLIENTE])) {
            //Guardar al cliente
            $cliente = $this->servicio_clientes->crear([
                ClientesModel::NOMBRE => $parametros[ClientesModel::NOMBRE],
                ClientesModel::APELLIDO_PATERNO => $parametros[ClientesModel::APELLIDO_PATERNO],
                ClientesModel::APELLIDO_MATERNO => $parametros[ClientesModel::APELLIDO_MATERNO],
                ClientesModel::TELEFONO => $parametros[ClientesModel::TELEFONO],
                ClientesModel::TELEFONO_2 => isset($parametros[ClientesModel::TELEFONO_2]) ? $parametros[ClientesModel::TELEFONO_2] : '',
                ClientesModel::CORREO_ELECTRONICO => $parametros[ClientesModel::CORREO_ELECTRONICO],
                ClientesModel::ES_CLIENTE => 0,
            ]);
            $parametros[VentasWebModel::ID_CLIENTE] = $cliente->id;
        }
        return $this->massUpdateWhereId(VentasWebModel::ID, $id, [
            VentasWebModel::ID_CLIENTE => $parametros[VentasWebModel::ID_CLIENTE],
            VentasWebModel::ID_UNIDAD => $parametros[VentasWebModel::ID_UNIDAD],
            VentasWebModel::ID_USUARIO_CREO => $parametros[VentasWebModel::ID_USUARIO_CREO],
            VentasWebModel::ID_ASESOR_WEB_ASIGNADO => $parametros[VentasWebModel::ID_ASESOR_WEB_ASIGNADO],
            VentasWebModel::ID_USUARIO_TELEMARKETING => $parametros[VentasWebModel::ID_USUARIO_TELEMARKETING],
            VentasWebModel::ID_ORIGEN => $parametros[VentasWebModel::ID_ORIGEN]
        ]);
    }
    public function getAllVentaCita($idventa)
    {
        return  $this->modelo->select(
            VentasWebModel::getTableName() . '.*',
            CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::NOMBRE . ' as unidad',
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID . ' as id_cita',
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID_STATUS,
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::FECHA_CITA,
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::HORARIO,
            ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO,
            ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO,
            ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO_2,
            ClientesModel::getTableName() . '.' . ClientesModel::CORREO_ELECTRONICO,
            'usuario_asesor.nombre as nombre_usuario_asesor',
            'usuario_asesor.apellido_paterno as apellido_paterno_asesor',
            'usuario_asesor.apellido_materno as apellido_materno_asesor'
        )
            ->from(VentasWebModel::getTableName())
            ->join(
                ClientesModel::getTableName(),
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_CLIENTE,
                '=',
                ClientesModel::getTableName() . '.' . ClientesModel::ID
            )
            ->join(
                CatalogoAutosModel::getTableName(),
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_UNIDAD,
                '=',
                CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::ID
            )
            ->join(
                CitasVentasModel::getTableName(),
                CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID_VENTA,
                '=',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID
            )
            ->join(
                User::getTableName() . ' as usuario_asesor',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ASESOR_WEB_ASIGNADO,
                '=',
                'usuario_asesor.' . User::ID
            )
            ->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID, $idventa)
            ->get();
    }
    public function buscarVentasPorParametros($parametros)
    {

        $query = $this->modelo->select(
            VentasWebModel::getTableName() . '.*',
            CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::NOMBRE . ' as unidad',
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID . ' as id_cita',
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID_STATUS,
            CitasVentasModel::getTableName() . '.' . CitasVentasModel::FECHA_CITA,
            CatalogoOrigenesModel::getTableName() . '.' . CatalogoOrigenesModel::ORIGEN,
            ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO,
            ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO,
            ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO,
            ClientesModel::getTableName() . '.' . ClientesModel::TELEFONO_2,
            ClientesModel::getTableName() . '.' . ClientesModel::CORREO_ELECTRONICO
        )
            ->from(VentasWebModel::getTableName())
            ->join(
                ClientesModel::getTableName(),
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_CLIENTE,
                '=',
                ClientesModel::getTableName() . '.' . ClientesModel::ID
            )
            ->join(
                CatalogoAutosModel::getTableName(),
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_UNIDAD,
                '=',
                CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::ID
            )
            ->leftJoin(
                CitasVentasModel::getTableName(),
                CitasVentasModel::getTableName() . '.' . CitasVentasModel::ID_VENTA,
                '=',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID
            )
            ->join(
                CatalogoOrigenesModel::getTableName(),
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ORIGEN,
                '=',
                CatalogoOrigenesModel::getTableName() . '.' . CatalogoOrigenesModel::ID
            );

        $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::NO_CONTACTAR, $parametros['no_contactar']);
        if (!isset($parametros['fecha'])) {
            $parametros['fecha'] = date('Y-m-d');
        }
        if ($parametros['vista'] === 'prospectos') {
            if ($parametros['id_perfil'] == 11) {
                $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_USUARIO_CREO, $parametros['id_usuario']);
            }
            $query->addSelect(
                'usuario_telemarketing.nombre as nombre_usuario_telemarketing',
                'usuario_telemarketing.apellido_paterno as apellido_paterno_telemarketing',
                'usuario_telemarketing.apellido_materno as apellido_materno_telemarketing'
            );
            $query->join(
                User::getTableName() . ' as usuario_telemarketing',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_USUARIO_TELEMARKETING,
                '=',
                'usuario_telemarketing.' . User::ID
            );
        }
        if ($parametros['vista'] === 'telemarketing' || $parametros['vista'] === 'reportes') {
            if ($parametros['id_perfil'] == 12) {
                $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_USUARIO_TELEMARKETING, $parametros['id_usuario']);
            }
            $query->addSelect(
                'usuario_telemarketing.nombre as nombre_usuario_telemarketing',
                'usuario_telemarketing.apellido_paterno as apellido_paterno_telemarketing',
                'usuario_telemarketing.apellido_materno as apellido_materno_telemarketing',
                'usuario_asesor.nombre as nombre_usuario_asesor',
                'usuario_asesor.apellido_paterno as apellido_paterno_asesor',
                'usuario_asesor.apellido_materno as apellido_materno_asesor'
            );
            $query->leftJoin(
                User::getTableName() . ' as usuario_asesor',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ASESOR_WEB_ASIGNADO,
                '=',
                'usuario_asesor.' . User::ID
            );
            $query->join(
                User::getTableName() . ' as usuario_telemarketing',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_USUARIO_TELEMARKETING,
                '=',
                'usuario_telemarketing.' . User::ID
            );

            $query->Where(function ($query) use ($parametros) {
                $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::FECHA_CITA_PROGRAMADA, $parametros['fecha'])
                    ->orWhere(VentasWebModel::getTableName() . '.' . VentasWebModel::FECHA_CITA_PROGRAMADA);
            });
        }
        if ($parametros['vista'] === 'asesores_web') {
            if ($parametros['id_perfil'] == 13) {
                $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ASESOR_WEB_ASIGNADO, $parametros['id_usuario']);
            }
            $query->addSelect(
                'usuario_asesor.nombre as nombre_usuario_asesor',
                'usuario_asesor.apellido_paterno as apellido_paterno_asesor',
                'usuario_asesor.apellido_materno as apellido_materno_asesor'
            );
            $query->join(
                User::getTableName() . ' as usuario_asesor',
                VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ASESOR_WEB_ASIGNADO,
                '=',
                'usuario_asesor.' . User::ID
            );
            $query->orWhere(function ($query) use ($parametros) {
                $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::FECHA_CITA_PROGRAMADA, $parametros['fecha'])
                    ->orWhere(VentasWebModel::getTableName() . '.' . VentasWebModel::FECHA_CITA_PROGRAMADA);
            });
        }
        if (isset($parametros['informacion_erronea']) && $parametros['informacion_erronea'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::INFORMACION_ERRONEA, $parametros['informacion_erronea']);
        }
        //Reportes
        if (isset($parametros['id_origen']) && $parametros['id_origen'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ORIGEN, $parametros['id_origen']);
        }
        if (isset($parametros['id_usuario_telemarketing']) && $parametros['id_usuario_telemarketing'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_USUARIO_TELEMARKETING, $parametros['id_usuario_telemarketing']);
        }

        if (isset($parametros['id_asesor']) && $parametros['id_asesor'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::ID_ASESOR_WEB_ASIGNADO, $parametros['id_asesor']);
        }
        if (isset($parametros['fecha_inicio']) && $parametros['fecha_inicio'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::CREATED_AT, '>=', $parametros['fecha_inicio']);
        }
        if (isset($parametros['fecha_fin']) && $parametros['fecha_fin'] != '') {
            $query->where(VentasWebModel::getTableName() . '.' . VentasWebModel::CREATED_AT, '<=', $parametros['fecha_fin']);
        }

        // if($parametros['id_perfil']==4 || $parametros['id_perfil']==5){
        //     $query->join(
        //         User::getTableName().' as usuario_asesor',VentasWebModel::getTableName().'.'.VentasWebModel::ID_ASESOR_WEB_ASIGNADO,'=', 'usuario_asesor.'.User::ID
        //     );
        // }
        return $query->get();
    }
    public function getSiguienteAsesor($parametros)
    {
        $asesores = $this->modelo_user->where(User::ROL_ID, '=', $parametros['rol_id'])->where(User::ACTIVO, '=', 1)->orderBy(User::ID, 'asc')->get();
        if ($parametros['rol_id'] == 12) {
            $tipo_usuario = VentasWebModel::ID_USUARIO_TELEMARKETING;
        } else {
            $tipo_usuario = VentasWebModel::ID_ASESOR_WEB_ASIGNADO;
        }
        $usuario_venta = $this->modelo_ventas->select($tipo_usuario)
            ->orderBy(User::ID, 'desc')
            ->where($tipo_usuario, '!=', null)
            ->limit(1)
            ->get();

        if (count($asesores) == 1) {
            return $asesores[0][User::ID];
        } else {
            if (count($usuario_venta) == 1) {
                $id_usuario = $usuario_venta[0][$tipo_usuario];
                //Validar si es el último asesor, regresar al primero
                if ($id_usuario == null) {
                    return $asesores[0][User::ID];
                }

                //saber si es el último asesor para que vuelva el primero
                if ($asesores[count($asesores) - 1][User::ID] == $id_usuario) {
                    return $asesores[0][User::ID];
                }
                $asesor_seleccionado = $this->modelo_user->select(User::ID)
                    ->where(User::ROL_ID, '=', $parametros['rol_id'])
                    ->where(User::ACTIVO, '=', 1)
                    ->where(USER::ID, '>', $id_usuario)
                    ->limit(1)
                    ->orderBy(User::ID, 'asc')
                    ->get();
                if (count($asesor_seleccionado) == 1) {
                    return $asesor_seleccionado[0][User::ID];
                } else {
                    return $asesores[0][User::ID];
                }
            } else {
                //No se han registrado ventas
                return $asesores[0][User::ID];
            }
        }
    }
}
