<?php

namespace App\Servicios\Telemarketing;

use App\Models\Autos\CatalogoAutosModel;
use App\Servicios\Core\ServicioDB;
use App\Models\Autos\UnidadesModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Telemarketing\CitasVentasModel;
use App\Models\Telemarketing\VentasWebModel;
use App\Models\Usuarios\User;

class ServicioCitasVentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'citas ventas';
        $this->modelo = new CitasVentasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CitasVentasModel:: ID_VENTA => 'required',
            CitasVentasModel:: ID_ASESOR => 'required',
            CitasVentasModel:: ID_STATUS => 'required',
            CitasVentasModel:: ID_USUARIO_CREO => 'required',
            CitasVentasModel:: FECHA_CITA => 'required',
            CitasVentasModel:: HORARIO => 'required|date_format:H:i:s'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CitasVentasModel:: ID_VENTA => 'required',
            CitasVentasModel:: ID_ASESOR => 'required',
            CitasVentasModel:: ID_STATUS => 'required',
            CitasVentasModel:: ID_USUARIO_CREO => 'required',
            CitasVentasModel:: FECHA_CITA => 'required|date',
            CitasVentasModel:: HORARIO => 'required|date_format:H:i:s'
        ];
    }
    public function getByIdVenta($id){
        return $this->modelo->where(CitasVentasModel:: ID_VENTA,$id)->get();
    }
    //Obtener los horarios que tiene el asesor ocupados
    public function getHorariosOcupadosAsesor($parametros){
        $horarios = $this->modelo->where(CitasVentasModel::ID_ASESOR,$parametros['id_asesor'])
                                ->where(CitasVentasModel::FECHA_CITA,$parametros['fecha'])
                                ->select(CitasVentasModel::HORARIO)
                                ->get();
        
    	$array_horarios = array();
    	foreach ($horarios as $key => $value) {
    		$array_horarios[] = $value['horario'];
    	}
    	return $array_horarios;
    }
    //Función para saber si un horario de un asesor en un día específico está ocupado
    public function validarCita($parametros){
        return $this->modelo->where(CitasVentasModel:: FECHA_CITA,$parametros['fecha_cita'])
                    ->where(CitasVentasModel:: HORARIO,$parametros['horario'])
                    ->where(CitasVentasModel::ID,'!=',$parametros['id_cita'])
                    ->get();
    }
    //guardar cita
    public function storeCita($parametros){
        //actualizar el asesor en la tabla de ventas
        $this->massUpdateWhereId(VentasWebModel::ID, $parametros['id_venta'], [VentasWebModel::ID_ASESOR_WEB_ASIGNADO,$parametros['id_asesor']]);
        return $this->store($parametros);
    }
    //Obtener todos los datos de una cita filtrados por fecha y asesor
    public function getDataCitas($parametros){
        return $this->modelo->select(
                                CitasVentasModel::getTableName().'.'.CitasVentasModel::ID,
                                CitasVentasModel::getTableName().'.'.CitasVentasModel::FECHA_CITA,
                                CitasVentasModel::getTableName().'.'.CitasVentasModel::HORARIO,
                                CitasVentasModel::getTableName().'.'.CitasVentasModel::ID_STATUS,
                                ClientesModel::getTableName().'.'.ClientesModel::NOMBRE.' as nombre_cliente',
                                ClientesModel::getTableName().'.'.ClientesModel::APELLIDO_PATERNO.' as ap_cliente',
                                ClientesModel::getTableName().'.'.ClientesModel::APELLIDO_MATERNO.' as am_cliente',
                                ClientesModel::getTableName().'.'.ClientesModel::TELEFONO,
                                ClientesModel::getTableName().'.'.ClientesModel::TELEFONO_2,
                                ClientesModel::getTableName().'.'.ClientesModel::CORREO_ELECTRONICO,
                                CatalogoAutosModel::getTableName().'.'.CatalogoAutosModel::NOMBRE.' as unidad',
                                User::getTableName().'.'.User::NOMBRE.' as nombre_asesor',
                                User::getTableName().'.'.User::APELLIDO_PATERNO.' as ap_asesor',
                                User::getTableName().'.'.User::APELLIDO_MATERNO.' as am_asesor',
                                User::getTableName().'.'.User::ACTIVO
                            )
                            ->where(CitasVentasModel::FECHA_CITA,$parametros['fecha'])
                            ->where(VentasWebModel::ID_ASESOR_WEB_ASIGNADO,$parametros['id_asesor'])
                            ->join(
                                VentasWebModel::getTableName(),VentasWebModel::getTableName().'.'.VentasWebModel::ID,'=',CitasVentasModel::getTableName().'.'.CitasVentasModel::ID_VENTA
                            )
                            ->join(
                                CatalogoAutosModel::getTableName(),CatalogoAutosModel::getTableName().'.'.CatalogoAutosModel::ID,'=',VentasWebModel::getTableName().'.'.VentasWebModel::ID_UNIDAD
                            )
                            ->join(
                                User::getTableName(),User::getTableName().'.'.User::ID,'=',VentasWebModel::getTableName().'.'.VentasWebModel::ID_ASESOR_WEB_ASIGNADO
                            )
                            ->join(
                                ClientesModel::getTableName(),ClientesModel::getTableName().'.'.ClientesModel::ID,'=',VentasWebModel::getTableName().'.'.VentasWebModel::ID_CLIENTE
                            )
                            ->get();
    }
    public function updateStatusCita($parametros){
       return $this->massUpdateWhereId(CitasVentasModel::ID, $parametros['id_cita'], [CitasVentasModel::ID_STATUS=>$parametros['id_status']]);
    }
}

