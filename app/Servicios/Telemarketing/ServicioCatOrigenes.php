<?php

namespace App\Servicios\Telemarketing;

use App\Servicios\Core\ServicioDB;
use App\Models\Telemarketing\CatalogoOrigenesModel;

class ServicioCatOrigenes extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas web';
        $this->modelo = new CatalogoOrigenesModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoOrigenesModel:: ORIGEN => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoOrigenesModel:: ORIGEN => 'required'
        ];
    }
}
