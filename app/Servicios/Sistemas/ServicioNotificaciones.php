<?php

namespace App\Servicios\Sistemas;

use App\Servicios\Core\ServicioDB;

class ServicioNotificaciones extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'notificaciones';
    }

}
