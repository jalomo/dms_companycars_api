<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\DevolucionAlmacenModel;


class ServicioDevolucionAlmacen extends ServicioDB
{

    public function __construct()
    {
        $this->recurso = 'devolucion almacen';
        $this->modelo = new DevolucionAlmacenModel();
    }

    public function getReglasGuardar()
    {
        return [
            DevolucionAlmacenModel::ORDEN_COMPRA_ID => 'required|exists:orden_compra,id',
            DevolucionAlmacenModel::PRODUCTO_ID => 'required|exists:producto,id',
            DevolucionAlmacenModel::ALMACEN_ID => 'required|exists:almacen,id',
            DevolucionAlmacenModel::CANTIDAD => 'required|numeric',
        ];
    }

}
