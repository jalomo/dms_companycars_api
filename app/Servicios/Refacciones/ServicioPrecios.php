<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\Precios;

class ServicioPrecios extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'precio';
        $this->modelo = new Precios();
    }

    public function getReglasGuardar()
    {
        return [
            Precios::PRECIO_MAYOREO => 'required|numeric',
            Precios::PRECIO_INTERNO => 'required|numeric',
            Precios::PRECIO_TALLER => 'required|numeric',
            Precios::PRECIO_OTRAS_DISTRIBUIDORAS => 'required|numeric',
            Precios::IMPUESTO => 'required|numeric',
            Precios::PRECIO_PUBLICO => 'required|numeric'


        ];
    }
    public function getReglasUpdate()
    {
        return [
            Precios::PRECIO_MAYOREO => 'numeric|numeric',
            Precios::PRECIO_INTERNO => 'numeric|numeric',
            Precios::PRECIO_TALLER => 'numeric|numeric',
            Precios::PRECIO_OTRAS_DISTRIBUIDORAS => 'numeric|numeric',
            Precios::IMPUESTO => 'numeric|numeric',
            Precios::PRECIO_PUBLICO => 'numeric|numeric'
        ];
    }
}
