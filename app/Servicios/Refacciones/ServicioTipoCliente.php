<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\TipoClienteModel;

class ServicioTipoCliente extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo cliente';
        $this->modelo = new TipoClienteModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoClienteModel::NOMBRE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoClienteModel::NOMBRE => 'required'
        ];
    }
}
