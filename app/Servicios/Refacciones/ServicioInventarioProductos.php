<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatalogoUbicacionProductoModel;
use App\Models\Refacciones\InventarioModel;
use App\Models\Refacciones\InventarioProductosModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\StockProductosModel;
use App\Models\Refacciones\EstatusInventarioModel;
use App\Models\Usuarios\User;
use Illuminate\Support\Facades\DB;

class ServicioInventarioProductos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'inventario productos';
        $this->modelo = new InventarioProductosModel();
        $this->productosModelo = new ProductosModel();
    }

    public function getReglasGuardar()
    {
        return [

            InventarioProductosModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',id',
            InventarioProductosModel::CANTIDAD_INVENTARIO => 'required|numeric',
            InventarioProductosModel::CANTIDAD_STOCK => 'required|numeric',
            InventarioProductosModel::VALOR_UNITARIO => 'required|numeric',
            InventarioProductosModel::DIFERENCIA => 'nullable|numeric',
            InventarioProductosModel::SOBRANTES => 'nullable|numeric',
            InventarioProductosModel::FALTANTES => 'nullable|numeric',
            InventarioProductosModel::VALOR_RESIDUO => 'nullable',
            InventarioProductosModel::USUARIO_ID => 'required|exists:' . User::getTableName() . ',id',
            InventarioProductosModel::INVENTARIO_ID => 'required|exists:' . InventarioModel::getTableName() . ',id'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            InventarioProductosModel::PRODUCTO_ID => 'required|exists:' . ProductosModel::getTableName() . ',id',
            InventarioProductosModel::CANTIDAD_INVENTARIO => 'required|numeric',
            InventarioProductosModel::CANTIDAD_STOCK => 'required|numeric',
            InventarioProductosModel::VALOR_UNITARIO => 'required|numeric',
            InventarioProductosModel::DIFERENCIA => 'nullable|numeric',
            InventarioProductosModel::SOBRANTES => 'nullable|numeric',
            InventarioProductosModel::FALTANTES => 'nullable|numeric',
            InventarioProductosModel::VALOR_RESIDUO => 'nullable',
            InventarioProductosModel::USUARIO_ID => 'required|exists:' . User::getTableName() . ',id',
            InventarioProductosModel::INVENTARIO_ID => 'required|exists:' . InventarioModel::getTableName() . ',id'
        ];
    }

    public function inventarioProductos($parametros)
    {
        $query = $this->productosModelo
            ->select(
                ProductosModel::getTableName() . '.' . ProductosModel::ID . ' as producto_id',
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                ProductosModel::getTableName() . '.' . ProductosModel::UNIDAD,
                ProductosModel::getTableName() . '.' . ProductosModel::VALOR_UNITARIO,
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
                StockProductosModel::getTableName() . '.' . StockProductosModel::CANTIDAD_ACTUAL . ' as cantidad_stock',
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::CANTIDAD_STOCK . ' as cantidad_stock_inventario',
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::CANTIDAD_INVENTARIO . ' as cantidad_inventario',
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::DIFERENCIA,
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::VALOR_RESIDUO,
                CatalogoUbicacionProductoModel::getTableName() . '.' . CatalogoUbicacionProductoModel::NOMBRE . ' as ubicacionProducto'
            )
            ->join(
                StockProductosModel::getTableName(),
                StockProductosModel::getTableName() . '.' . StockProductosModel::PRODUCTO_ID,
                '=',
                ProductosModel::getTableName() . '.' . ProductosModel::ID
            )
            ->join(
                CatalogoUbicacionProductoModel::getTableName(),
                CatalogoUbicacionProductoModel::getTableName() . '.' . CatalogoUbicacionProductoModel::ID,
                '=',
                ProductosModel::UBICACION_PRODUCTO_ID
            )
            ->leftJoin(InventarioProductosModel::getTableName(), function ($data) use ($parametros) {
                $data->on(
                    InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::PRODUCTO_ID,
                    '=',
                    ProductosModel::getTableName() . '.' . ProductosModel::ID
                )->where(
                    InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::INVENTARIO_ID, '=',
                    $parametros[InventarioProductosModel::INVENTARIO_ID]
                );
            })

            ->where(
                StockProductosModel::getTableName() . '.' . StockProductosModel::CANTIDAD_ACTUAL,
                '>=',
                1
            );

        if (isset($parametros['inventario_finalizado']) && isset($parametros[InventarioProductosModel::INVENTARIO_ID]) && $parametros['inventario_finalizado']) {
            
            $query->whereNull(InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::CANTIDAD_INVENTARIO);
        }

        if (isset($parametros[InventarioProductosModel::INVENTARIO_ID]) && $parametros[InventarioProductosModel::INVENTARIO_ID]) {

            $query->where(function ($data) use ($parametros) {
                $data->orwhere(
                    InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::INVENTARIO_ID,
                    $parametros[InventarioProductosModel::INVENTARIO_ID]
                );

                $data->orWhereNull(InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::INVENTARIO_ID);
            });
        }

        if (isset($parametros[ProductosModel::DESCRIPCION])) {
            $query->Where(ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,  'iLIKE', '%' . $parametros[ProductosModel::DESCRIPCION] . '%');
            $query->orWhere(ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,  'iLIKE', '%' . $parametros[ProductosModel::DESCRIPCION] . '%');
        }

        if (isset($parametros[InventarioProductosModel::PRODUCTO_ID]) && isset($parametros[InventarioProductosModel::INVENTARIO_ID])) {
            $query->where(InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::PRODUCTO_ID, $parametros[InventarioProductosModel::PRODUCTO_ID]);
            $query->where(
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::INVENTARIO_ID,
                $parametros[InventarioProductosModel::INVENTARIO_ID]
            );
        }

        
        return $query->get();
    }


    public function validarInventarioCompleto($id_inventario)
    {
        return $this->inventarioProductos([
            'inventario_finalizado' => true,
            InventarioProductosModel::INVENTARIO_ID => $id_inventario
        ]);
    }


    public function validarExisteProductoInventariado($parametros)
    {
        return $this->modelo
            ->where(
                InventarioProductosModel::PRODUCTO_ID,
                $parametros[InventarioProductosModel::PRODUCTO_ID]
            )
            ->where(
                InventarioProductosModel::INVENTARIO_ID,
                $parametros[InventarioProductosModel::INVENTARIO_ID]
            )
            ->get();
    }

    public function getByProductoId($producto_id) {
        return $this->modelo
            ->select(
                 DB::raw('sum(inventario_productos.faltantes) as totalFaltantes'),
                 DB::raw('sum(inventario_productos.sobrantes) as totalSobrantes')
            )
            ->join(
                InventarioModel::getTableName(),
                InventarioModel::getTableName() . '.' . InventarioModel::ID,
                '=',
                InventarioProductosModel::INVENTARIO_ID
            )
            ->where(InventarioProductosModel::PRODUCTO_ID, $producto_id )
            ->where(InventarioModel::ESTATUS_INVENTARIO_ID, EstatusInventarioModel::FINALIZADO)
            ->groupBy(
                InventarioProductosModel::getTableName() . '.' . InventarioProductosModel::PRODUCTO_ID
            )
            ->first();
    }
}
