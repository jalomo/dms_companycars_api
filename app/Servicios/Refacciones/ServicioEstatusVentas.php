<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusVentaModel;

class ServicioEstatusVentas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus ventas';
        $this->modelo = new EstatusVentaModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusVentaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusVentaModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
        ];
    }
}
