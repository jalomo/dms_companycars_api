<?php

namespace App\Servicios\Refacciones;

use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\Almacenes;
use App\Models\Refacciones\EstatusTraspasoModel;
use App\Models\Refacciones\ProductoAlmacenModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReEstatusTraspasoModel;
use App\Models\Refacciones\TraspasoProductoAlmacenModel;
use App\Models\Refacciones\Traspasos;
use App\Models\Refacciones\StockProductosModel;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class ServicioTraspasoProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'producto';
        $this->modelo = new TraspasoProductoAlmacenModel();
        $this->servicioProducto = new ServicioProductos();
        $this->servicioTraspaso = new ServicioTraspasos();
        $this->servicioAlmacen = new ServicioAlmacenes();
        $this->servicioProductoAlmacen = new ServicioProductoAlmacen();
        $this->servicioReEstatusTraspaso = new ServicioReEstatusTraspaso();
        $this->servicioConsultaStock = new ServicioConsultaStock();
        $this->servicioDesgloseProductos = new ServicioDesgloseProductos();

    }

    public function getReglasGuardar()
    {
        return [
            TraspasoProductoAlmacenModel::PRODUCTO_ID => 'required|numeric',
            TraspasoProductoAlmacenModel::VALOR_UNITARIO => 'required|numeric',
            TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID => 'required|numeric',
            TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID => 'required|numeric',
            TraspasoProductoAlmacenModel::TRASPASO_ID => 'required|numeric',
            TraspasoProductoAlmacenModel::CANTIDAD => 'required|numeric',
            TraspasoProductoAlmacenModel::ACTIVO => 'required|numeric',
        ];
    }

    public function getReglasUpdate()
    {
        return [
            TraspasoProductoAlmacenModel::PRODUCTO_ID => 'numeric',
            TraspasoProductoAlmacenModel::VALOR_UNITARIO => 'required|numeric',
            TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID => 'numeric',
            TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID => 'numeric',
            TraspasoProductoAlmacenModel::TOTAL => 'required',
            TraspasoProductoAlmacenModel::TRASPASO_ID => 'required|numeric',
            TraspasoProductoAlmacenModel::CANTIDAD => 'required|numeric',
            TraspasoProductoAlmacenModel::ACTIVO => 'numeric'
        ];
    }

    public function actualizarProductoAlmacen(Request $request, $id)
    {
        return $this->update($request, $id);
    }

    public function getAll()
    {
        $data = $this->buscarTodos();
        foreach ($data as $key => $item) {
            $data[$key]['producto'] = $this->servicioProducto->getById($item[TraspasoProductoAlmacenModel::PRODUCTO_ID]);
            $data[$key]['almacen_origen'] = $this->servicioAlmacen->getById($item[TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID]);
            $data[$key]['almacen_destino'] = $this->servicioAlmacen->getById($item[TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID]);
        }

        return $data;
    }


    public function getbyidproducto($id)
    {
        return $this->modelo->where(TraspasoProductoAlmacenModel::PRODUCTO_ID, $id)->get();
    }

    public function getByIdTraspaso($traspaso_id)
    {
        $data =  $this->modelo
            ->where(TraspasoProductoAlmacenModel::TRASPASO_ID, $traspaso_id)
            ->limit(100)->get();
        foreach ($data as $key => $item) {
            $data[$key]['producto'] = $this->servicioProducto->getById($item[TraspasoProductoAlmacenModel::PRODUCTO_ID]);
            $data[$key]['almacen_origen'] = $this->servicioAlmacen->getById($item[TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID]);
            $data[$key]['almacen_destino'] = $this->servicioAlmacen->getById($item[TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID]);
            $totale_destino = $this->servicioProductoAlmacen->getCantidadAlmByProducto([
                ProductoAlmacenModel::ALMACEN_ID => $item[TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID],
                ProductoAlmacenModel::PRODUCTO_ID => $item[TraspasoProductoAlmacenModel::PRODUCTO_ID]
            ]);
            $data[$key]['total_prod_almacen_dest'] = isset($totale_destino) ? $totale_destino->cantidad : 0;
            $totales_origen = $this->servicioProductoAlmacen->getCantidadAlmByProducto([
                ProductoAlmacenModel::ALMACEN_ID => $item[TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID],
                ProductoAlmacenModel::PRODUCTO_ID => $item[TraspasoProductoAlmacenModel::PRODUCTO_ID]
            ]);
            $data[$key]['total_prod_almacen_orig'] = isset($totales_origen) ? $totales_origen->cantidad : 0;
        }

        return $data;
    }

    public function getDetalleTraspasoAlmacen($traspaso_id)
    {
        $tabla_detalle_traspaso = TraspasoProductoAlmacenModel::getTableName();
        $tabla_producto = ProductosModel::getTableName();
        $tabla_stock_productos = StockProductosModel::getTableName();
        $tabla_re_traspasos = ReEstatusTraspasoModel::getTableName();
        $consulta =  $this->modelo
        ->select(
            'detalle_traspaso_producto_almacen.id',
            'producto.id as producto_id',
            'producto.no_identificacion',
            'producto.descripcion',
            "detalle_traspaso_producto_almacen.cantidad",
            "detalle_traspaso_producto_almacen.almacen_destino_id",
            "detalle_traspaso_producto_almacen.almacen_origen_id",
            'stock_productos.cantidad_almacen_primario',
            'stock_productos.cantidad_almacen_secundario',
            'producto.valor_unitario',
            're_estatus_traspaso.estatus_id'
        )
        ->join($tabla_producto, $tabla_producto . '.id', '=', $tabla_detalle_traspaso . '.producto_id')
        ->join($tabla_stock_productos, $tabla_producto . '.id', '=', $tabla_stock_productos . '.producto_id')
        ->leftjoin($tabla_re_traspasos, $tabla_re_traspasos . '.traspaso_id', '=', $tabla_detalle_traspaso . '.traspaso_id')
        ->where($tabla_detalle_traspaso.'.'.TraspasoProductoAlmacenModel::TRASPASO_ID, $traspaso_id)
        ->get();

        return $this->getEstructuraDetalleTraspaso($consulta);
        
    }

    private function getEstructuraDetalleTraspaso($consulta) {
        $listado = [];
        foreach ($consulta as $val) {
            if(!$val['estatus_id']) {
                if ($val['almacen_origen_id'] == 1) {
                    $val['cantidad_almacen_primario'] = $val['cantidad_almacen_primario'] - $val['cantidad'];
                    $val['cantidad_almacen_secundario'] = $val['cantidad_almacen_secundario'] + $val['cantidad'];
                } else {
                    $val['cantidad_almacen_primario'] = $val['cantidad_almacen_primario'] + $val['cantidad'];
                    $val['cantidad_almacen_secundario'] = $val['cantidad_almacen_secundario'] - $val['cantidad'];
                }
            }
            $val['total_valor'] = $val['cantidad'] * $val['valor_unitario'];

            array_push($listado, $val);
        }
        return $listado;
    }
    //TODO: revisar funcionalidad
    public function getByIdTraspaso2($traspaso_id)
    {
        $tabla_almacen = Almacenes::getTableName();
        $tabla_producto = ProductosModel::getTableName();
        $tabla_producto_almacen = TraspasoProductoAlmacenModel::getTableName();
        $data =  $this->modelo
            ->select(
                $tabla_producto_almacen . '.id',
                $tabla_producto_almacen . '.producto_id',
                $tabla_producto . '.descripcion as producto',
                $tabla_producto . '.no_identificacion as no_identificacion',
                $tabla_producto_almacen . '.almacen_origen_id',
                $tabla_almacen . '.nombre as almacen_origen',
                $tabla_producto_almacen . '.almacen_destino_id',
                'almc_dest.nombre as almacen_destino',
                $tabla_producto_almacen . '.traspaso_id',
                $tabla_producto_almacen . '.activo',
                $tabla_producto_almacen . '.total',
                $tabla_producto_almacen . '.valor_unitario',
                $tabla_producto_almacen . '.cantidad as cantidad_traspasar'
            )
            ->join($tabla_almacen, $tabla_producto_almacen . '.almacen_origen_id', '=', $tabla_almacen . '.id')
            ->join($tabla_almacen . ' as almc_dest', $tabla_producto_almacen . '.almacen_destino_id', '=', 'almc_dest.id')
            ->join($tabla_producto, $tabla_producto_almacen . '.producto_id', '=', $tabla_producto . '.id')
            ->where(TraspasoProductoAlmacenModel::TRASPASO_ID, $traspaso_id)
            ->limit(100)
            ->get();

        foreach ($data as $key => $item) {
            $stock_actual = $this->servicioProducto->stockProductos([
                'producto_id' => $item->producto_id,
                'compra_realizada' => true
            ]);

            $stock_producto = count($stock_actual) > 0 ? $stock_actual[0]->stockActual : 0;
            $data[$key]['stock_actual'] = $stock_producto; 
        }

        return $data;
    }

    public function handleTraspasos($data)
    {
        $traspaso_model = $this->modelo
            ->where(TraspasoProductoAlmacenModel::TRASPASO_ID, $data[TraspasoProductoAlmacenModel::TRASPASO_ID])
            ->get();
        foreach ($traspaso_model as $key => $item) {
            $this->updateProductoCantidad($item, $data[Traspasos::FOLIO_ID]);
        }

        $this->servicioReEstatusTraspaso->crear([
            TraspasoProductoAlmacenModel::TRASPASO_ID => $data[TraspasoProductoAlmacenModel::TRASPASO_ID],
            ReEstatusTraspasoModel::ESTATUS_ID => EstatusTraspasoModel::ESTATUS_REALIZADO,
            ReEstatusTraspasoModel::USER_ID => $data[ReEstatusTraspasoModel::USER_ID],
        ]);

        return $this->servicioTraspaso
            ->massUpdateWhereId(
                Traspasos::ID,
                $data[TraspasoProductoAlmacenModel::TRASPASO_ID],
                [Traspasos::ESTATUS => Traspasos::ESTATUS_FINALIZADO]
            );
    }

    // descontar productos
    public function updateProductoCantidad($traspaso_item, $folio)
    {
        $data_producto = $this->servicioProducto->getById($traspaso_item[TraspasoProductoAlmacenModel::PRODUCTO_ID]);
        if ($traspaso_item[TraspasoProductoAlmacenModel::CANTIDAD] > $data_producto->cantidad) {
            throw new ParametroHttpInvalidoException([
                'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $data_producto->no_identificacion])
            ]);
        }

        $almacen_origen_cantidad = $this->getTotalOrcreateRecord(
            $traspaso_item[TraspasoProductoAlmacenModel::PRODUCTO_ID],
            $traspaso_item[TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID],
            $data_producto
        );

        $almacen_destino_cantidad = $this->getTotalOrcreateRecord(
            $traspaso_item[TraspasoProductoAlmacenModel::PRODUCTO_ID],
            $traspaso_item[TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID],
            $data_producto
        );


        $total_sumar = $almacen_destino_cantidad->cantidad + $traspaso_item[TraspasoProductoAlmacenModel::CANTIDAD];
        $this->servicioProductoAlmacen->massUpdateWhereId(ProductoAlmacenModel::ID, $almacen_destino_cantidad->id, [
            ProductoAlmacenModel::CANTIDAD => $total_sumar
        ]);

        $resta_producto_envia = $almacen_origen_cantidad->cantidad - $traspaso_item[TraspasoProductoAlmacenModel::CANTIDAD];
        $this->servicioProductoAlmacen->massUpdateWhereId(ProductoAlmacenModel::ID, $almacen_origen_cantidad->id, [
            ProductoAlmacenModel::CANTIDAD => $resta_producto_envia
        ]);

        $this->massUpdateWhereId(TraspasoProductoAlmacenModel::ID, $traspaso_item[TraspasoProductoAlmacenModel::ID], [
            TraspasoProductoAlmacenModel::DESCONTADO_A_PRODUCTO => 1
        ]);
    }

    public function getTotalOrcreateRecord($pructo_id, $traspaso_id, $data_producto)
    {
        $data =  $this->servicioProductoAlmacen->getCantidadAlmByProducto([
            ProductoAlmacenModel::PRODUCTO_ID => $pructo_id,
            ProductoAlmacenModel::ALMACEN_ID => $traspaso_id,
        ]);

        if (!isset($data)) {
            $new_record = $this->servicioProductoAlmacen->crear([
                ProductoAlmacenModel::PRODUCTO_ID => $pructo_id,
                ProductoAlmacenModel::ALMACEN_ID => $traspaso_id,
                ProductoAlmacenModel::NO_IDENTIFICACION => $data_producto->no_identificacion,
                ProductoAlmacenModel::CANTIDAD => 0,
            ]);

            return $new_record;
        }

        return $data;
    }

    public function getReglasConfirmarTraspaso()
    {
        return [
            TraspasoProductoAlmacenModel::TRASPASO_ID => 'required|numeric',
            ReEstatusTraspasoModel::USER_ID => 'required',
            Traspasos::FOLIO_ID => 'required|numeric',
        ];
    }

    public function store($request)
    {
        $traspaso_producto = $this->existeProducto($request->producto_id, $request->traspaso_id);
        if (count($traspaso_producto) > 0) {
            $data[TraspasoProductoAlmacenModel::PRODUCTO_ID] = $request->producto_id;
            $data[TraspasoProductoAlmacenModel::ALMACEN_ORIGEN_ID] = $request->almacen_origen_id;
            $data[TraspasoProductoAlmacenModel::ALMACEN_DESTINO_ID] = $request->almacen_destino_id;
            $data[TraspasoProductoAlmacenModel::TRASPASO_ID] = $request->traspaso_id;
            $data[TraspasoProductoAlmacenModel::ACTIVO] = $request->activo;
            $cantidad_total = $traspaso_producto[0][TraspasoProductoAlmacenModel::CANTIDAD] + $request->cantidad;
            $data[TraspasoProductoAlmacenModel::CANTIDAD] = $cantidad_total;
            $data[TraspasoProductoAlmacenModel::VALOR_UNITARIO] = $request->valor_unitario;
            $data[TraspasoProductoAlmacenModel::TOTAL] = $request->valor_unitario * $cantidad_total;

            return DB::table($this->modelo->getTableName())
                ->where(TraspasoProductoAlmacenModel::ID, $traspaso_producto[0][TraspasoProductoAlmacenModel::ID])
                ->update($data);
        } else {
            $total = $request->valor_unitario * $request->cantidad;
            $request->merge([TraspasoProductoAlmacenModel::TOTAL => $total]);
            return $this->modelo->create($request->all());
        }
    }

    public function existeProducto($pructo_id, $traspaso_id)
    {
        return $this->modelo
            ->where(TraspasoProductoAlmacenModel::PRODUCTO_ID, $pructo_id)
            ->where(TraspasoProductoAlmacenModel::TRASPASO_ID, $traspaso_id)
            ->get()->toArray();
    }

}
