<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusVentaModel;
use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ReVentasEstatusModel;
use App\Models\Refacciones\VentaProductoModel;
use Illuminate\Support\Facades\DB;

class ServicioPedidoSugerido extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'ventas';
        $this->modelo = new VentaProductoModel();
        $this->modeloProductos = new ProductosModel();
        $this->servicioProductos = new ServicioProductos();
    }

    public function queryVentasPedidoSugerido($parametros)
    {
        $fecha_actual = date('Y-m-d');

        $data = $this->modelo->select(
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::TOTAL_VENTA,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ' as fecha_venta',
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION . ' as nombre_producto',
            DB::raw('sum(' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CANTIDAD . ') as cantidad'),
            DB::raw('EXTRACT(YEAR FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ') as venta_anio'),
            DB::raw('EXTRACT(MONTH FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ') as venta_mes')
        )->join(
            ProductosModel::getTableName(),
            ProductosModel::getTableName() . '.' . ProductosModel::ID,
            '=',
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
        )
            ->join(
                ReVentasEstatusModel::getTableName(),
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::VENTA_ID,
                '=',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::ID
            )
            ->where(
                ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ESTATUS_VENTA_ID,
                EstatusVentaModel::ESTATUS_FINALIZADA
            )
            ->where(ReVentasEstatusModel::getTableName() . '.' . ReVentasEstatusModel::ACTIVO, TRUE);

        if (isset($parametros['sugerido_rapido']) && $parametros['sugerido_rapido']) {
            $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 5 month"));
            $data->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                $fecha_inicial . ' 00:00:00',
                $fecha_actual . ' 23:59:59'
            ]);
        }

        if (isset($parametros['sugerido_lento']) && $parametros['sugerido_lento']) {
            $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 5 month"));
            $data->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                $fecha_inicial . ' 00:00:00',
                $fecha_actual . ' 23:59:59'
            ]);
        }

        if (isset($parametros[ProductosModel::NO_IDENTIFICACION_FACTURA])) {
            $data->where(ProductosModel::NO_IDENTIFICACION_FACTURA, $parametros[ProductosModel::NO_IDENTIFICACION_FACTURA]);
        }

        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $data->where(VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }

        if (isset($parametros['numero_mes'])) {
            $data->where(
                DB::raw("MONTH(" . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ")"),
                $parametros['numero_mes']
            );
        }

        $data->groupBy(
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::TOTAL_VENTA,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT,
            VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID,
            ProductosModel::getTableName() . '.' . ProductosModel::ID,
            DB::raw('EXTRACT(YEAR FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ')'),
            DB::raw('EXTRACT(MONTH FROM ' . VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT . ')')
        );

        $data->orderBy('cantidad', 'asc');

        return $data->get();
    }

    public function getAllTipoProductosVenta()
    {
        $query = $this->modelo
            ->select(
                DB::raw("DISTINCT(" . VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID . ")"),
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION . ' as nombre_producto',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            )->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                '=',
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            );

        return $query->get();
    }

    public function pedidosugeridorapido($item_id = '')
    {
        if ($item_id != '') {
            $final_array = [];
            $producto_data = $this->servicioProductos->getById($item_id);

            $producto_venta = $this->queryVentasPedidoSugerido([
                'sugerido_rapido' => true,
                VentaProductoModel::PRODUCTO_ID => $producto_data->id
            ])->toArray();

            $promedio_producto = $this->promedioProducto($producto_venta);

            if ($promedio_producto != null && $promedio_producto > 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto_data->no_identificacion,
                    'nombre_producto' => $producto_data->descripcion,
                    'producto_id' => $producto_data->id,
                    'promedio' => $promedio_producto,
                ]);
            }
            return $final_array;
        }

        $tipos_producto = $this->getAllTipoProductosVenta()->toArray();
        $final_array = [];
        foreach ($tipos_producto as $key => $producto) {
            $producto_id = $producto['producto_id'];
            $producto_venta = $this->queryVentasPedidoSugerido([
                'sugerido_rapido' => true,
                VentaProductoModel::PRODUCTO_ID => $producto_id
            ])->toArray();

            $promedio_producto = $this->promedioProducto($producto_venta);
            if ($promedio_producto != null && $promedio_producto > 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto['no_identificacion'],
                    'nombre_producto' => $producto['nombre_producto'],
                    'producto_id' => $producto['producto_id'],
                    'promedio' => $promedio_producto,
                ]);
            }
        }

        return $final_array;
    }

    public function promedioProducto($data_array)
    {

        $array_length = count($data_array);

        if ($array_length >= 4) {

            $total = 0;
            $venta_array = [];
            foreach ($data_array as $key => $item) {

                $total = $item['cantidad'] + $total;
                $venta_array[$item['venta_mes']]['cantidad'] = $item['cantidad'];
            }


            return $total / 5;
        }

        return 0;
    }

    public function promedioLento($data_array)
    {
        $array_length = count($data_array);
        if ($array_length > 0 && $array_length <= 3) {
            $total = 0;
            $venta_array = [];

            foreach ($data_array as $key => $item) {
                $total = $item['cantidad'] + $total;
                $venta_array[$item['venta_mes']]['cantidad'] = $item['cantidad'];
            }

            return round($total / 5);
        } else {
            return 0;
        }
    }


    public function calcularSugeridoLento($item_id = '')
    {
        if ($item_id != '') {
            $final_array = [];
            $producto_data = $this->servicioProductos->getById($item_id);
            $producto_venta = $this->queryVentasPedidoSugerido([
                VentaProductoModel::PRODUCTO_ID => $producto_data->id,
                'sugerido_lento' => true
            ])->toArray();

            $promedio_producto_lento = $this->promedioLento($producto_venta);
            if ($promedio_producto_lento != 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto_data->no_identificacion,
                    'nombre_producto' => $producto_data->descripcion,
                    'producto_id' => $producto_data->id,
                    'promedio' => $promedio_producto_lento
                ]);
            }

            return $final_array;
        }

        $tipos_producto = $this->getAllTipoProductosVenta()->toArray();
        $final_array = [];
        foreach ($tipos_producto as $key => $producto) {
            $producto_id = $producto['producto_id'];
            $producto_venta = $this->queryVentasPedidoSugerido([
                VentaProductoModel::PRODUCTO_ID => $producto_id,
                'sugerido_lento' => true
            ])->toArray();

            $promedio_producto_lento = $this->promedioLento($producto_venta);
            if ($promedio_producto_lento != 0) {
                array_push($final_array, [
                    'no_identificacion' => $producto['no_identificacion'],
                    'nombre_producto' => $producto['nombre_producto'],
                    'producto_id' => $producto['producto_id'],
                    'promedio' => $promedio_producto_lento
                ]);
            }
        }

        return $final_array;
    }


    public function calcularSugeridoObsoleto($item_id = '')
    {
        $parametros = [];
        $parametros['obsoleto'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function buildQueryCategoriaC($parametros)
    {
        $query = $this->modeloProductos
            ->select(
                ProductosModel::getTableName() . '.' . ProductosModel::ID .' as producto_id',
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION .' as nombre_producto',
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA
            );
        $query->whereNotIn(ProductosModel::getTableName() . '.' . ProductosModel::ID, function ($query) use ($parametros) {
            $query->select(
                VentaProductoModel::getTableName() . '.' . VentaProductoModel::PRODUCTO_ID
            )
                ->from(VentaProductoModel::getTableName());

            $fecha_actual = date('Y-m-d');
            if (isset($parametros['obsoleto']) && $parametros['obsoleto']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 1 year"));
                $query->whereBetween(
                    VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT,
                    [
                        $fecha_inicial . ' 00:00:00',
                        $fecha_actual . ' 23:59:59'
                    ]
                );
            }

            if (isset($parametros['estancado']) && $parametros['estancado']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 1 year"));
                $periodo = date("Y-m-d", strtotime($fecha_inicial . '+ 5 month'));
                $periodo_undia = date("Y-m-d", strtotime($periodo . '+ 1 day'));

                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $periodo_undia . ' 23:59:59'
                ]);
            }

            if (isset($parametros['potencial_obsoleto']) && $parametros['potencial_obsoleto']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 91 day"));

                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $fecha_actual . ' 23:59:59'
                ]);
            }

            if (isset($parametros['inactivo']) && $parametros['inactivo']) {
                $fecha_inicial = date("Y-m-d", strtotime($fecha_actual . "- 3 month"));
                $query->whereBetween(VentaProductoModel::getTableName() . '.' . VentaProductoModel::CREATED_AT, [
                    $fecha_inicial . ' 00:00:00',
                    $fecha_actual . ' 23:59:59'
                ]);
            }
        });


        if (isset($parametros[VentaProductoModel::PRODUCTO_ID])) {
            $query->where(ProductosModel::getTableName() . '.' . ProductosModel::ID, $parametros[VentaProductoModel::PRODUCTO_ID]);
        }
        
        return $query->get();
    }

    public function calcularSugeridoInactivo($item_id = '')
    {
        $parametros = [];
        $parametros['inactivo'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }

        return $this->buildQueryCategoriaC($parametros);
    }

    public function calcularSugeridoPotencialObsoleto($item_id = '')
    {
        $parametros = [];
        $parametros['potencial_obsoleto'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function calcularSugeridoEstancado($item_id = '')
    {
        // if ($item_id != '') {
        //     $producto_data = $this->servicioProductos->getById($item_id);
        //     $producto_venta = $this->queryVentasPedidoSugerido([
        //         VentaProductoModel::PRODUCTO_ID => $producto_data->id,
        //         'sugerido_estancado' => true
        //     ])->toArray();

        //     if (count($producto_venta) == 1) {
        //         $valor_promedio = $this->validarEstancado($producto_venta);
        //         return [
        //             'no_identificacion' => $producto_data->no_identificacion,
        //             'nombre_producto' => $producto_data->descripcion,
        //             'producto_id' => $producto_data->id,
        //             'promedio' => $valor_promedio
        //         ];
        //     }
        // }

        // $tipos_producto = $this->getAllTipoProductosVenta()->toArray();
        // $final_array = [];
        // foreach ($tipos_producto as $key => $producto) {
        //     $producto_id = $producto['producto_id'];
        //     $producto_venta = $this->queryVentasPedidoSugerido([
        //         VentaProductoModel::PRODUCTO_ID => $producto_id,
        //         'sugerido_estancado' => true
        //     ])->toArray();

        //     if (count($producto_venta) == 1) {
        //         $valor_promedio = $this->validarEstancado($producto_venta);
        //         array_push($final_array, [
        //             'no_identificacion' => $producto['no_identificacion'],
        //             'nombre_producto' => $producto['nombre_producto'],
        //             'producto_id' => $producto['producto_id'],
        //             'promedio' => $valor_promedio
        //         ]);
        //     }
        // }

        // return $final_array;
        $parametros = [];
        $parametros['estancado'] = true;
        if ($item_id != '') {
            $parametros[VentaProductoModel::PRODUCTO_ID] = $item_id;
        }
        return $this->buildQueryCategoriaC($parametros);
    }

    public function validarEstancado($data)
    {

        $new_data = $data[0];
        $cantidad = $new_data['cantidad'];
        return round($cantidad / 11);
    }

    public function categorizaporproducto($producto_id)
    {
        return [
            'rapido' => $this->pedidosugeridorapido($producto_id),
            'lento' => $this->calcularSugeridoLento($producto_id),
            'inactivo' => $this->calcularSugeridoInactivo($producto_id),
            'potencial_obsoleto' => $this->calcularSugeridoPotencialObsoleto($producto_id),
            // 'estancado' => $this->calcularSugeridoEstancado($producto_id),
            'obsoleto' => $this->calcularSugeridoObsoleto($producto_id)
        ];
    }

    public function pedidoSugerido_bk()
    {
        // if ($array_length > 2) {
        //     //quitar ultimo
        //     unset($data_array[$array_length - 1]);
        //     //quitar primero 
        //     unset($data_array[0]);
        // }

        // $total = 0;
        // if ($array_length != 0) {
        //     foreach ($data_array as $key => $valor) {
        //         $total = $valor['cantidad'] + $total;
        //     }
        // }
        // if ($total == 0 && count($data_array) == 0) {
        //     return 0;
        // }

        // $promedio = $total / count($data_array);
        // return round($promedio);
    }
}
