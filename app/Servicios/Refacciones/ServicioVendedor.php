<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\VendedorModel;

class ServicioVendedor extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'vendedor';
        $this->modelo = new VendedorModel();
    }

    public function getReglasGuardar()
    {
        return [
            VendedorModel::NOMBRE => 'required',
            VendedorModel::APELLIDO_MATERNO => 'required',
            VendedorModel::APELLIDO_PATERNO => 'nullable',
            VendedorModel::NUMERO_VENDEDOR => 'nullable',
            VendedorModel::RFC => 'required',
            VendedorModel::DIRECCION => 'required',
            VendedorModel::ESTADO => 'required',
            VendedorModel::MUNICIPIO => 'required',
            VendedorModel::COLONIA => 'required',
            VendedorModel::CODIGO_POSTAL => 'required',
            VendedorModel::TELEFONO => 'required',
            VendedorModel::TELEFONO_2 => 'nullable',
            VendedorModel::CORREO_ELECTRONICO => 'required|email',
            VendedorModel::CORREO_ELECTRONICO_2 => 'nullable|email',
            VendedorModel::NOTAS => 'nullable',
            VendedorModel::CLAVE_VENDEDOR => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VendedorModel::NOMBRE => 'required',
            VendedorModel::APELLIDO_MATERNO => 'required',
            VendedorModel::APELLIDO_PATERNO => 'nullable',
            VendedorModel::NUMERO_VENDEDOR => 'nullable',
            VendedorModel::RFC => 'required',
            VendedorModel::DIRECCION => 'required',
            VendedorModel::ESTADO => 'required',
            VendedorModel::MUNICIPIO => 'required',
            VendedorModel::COLONIA => 'required',
            VendedorModel::CODIGO_POSTAL => 'required',
            VendedorModel::TELEFONO => 'required',
            VendedorModel::TELEFONO_2 => 'nullable',
            VendedorModel::CORREO_ELECTRONICO => 'required|email',
            VendedorModel::CORREO_ELECTRONICO_2 => 'nullable|email',
            VendedorModel::NOTAS => 'nullable',
            VendedorModel::CLAVE_VENDEDOR => 'nullable'
        ];
    }
}
