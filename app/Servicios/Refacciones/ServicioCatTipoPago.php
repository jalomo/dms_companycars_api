<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\CatTipoPagoModel;

class ServicioCatTipoPago extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'Tipo de pago';
        $this->modelo = new CatTipoPagoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatTipoPagoModel::NOMBRE => 'required',
            CatTipoPagoModel::CLAVE => 'required',
            CatTipoPagoModel::ACTIVO => 'nullable|boolean',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatTipoPagoModel::NOMBRE => 'nullable',
            CatTipoPagoModel::CLAVE => 'nullable',
            CatTipoPagoModel::ACTIVO => 'nullable|boolean',
        ];
    }
}
