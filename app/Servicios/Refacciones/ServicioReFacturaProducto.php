<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\ReFacturaProductoModel;

class ServicioReFacturaProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 're factura producto';
        $this->modelo = new ReFacturaProductoModel();
    }

    public function getReglasGuardar()
    {
        return [
            ReFacturaProductoModel::FACTURA_ID => 'required|numeric|exists:factura,id',
            ReFacturaProductoModel::PRODUCTO_ID => 'required|numeric|exists:producto,id',
            ReFacturaProductoModel::ESTATUS_FACTURA_ID => 'required|numeric|exists:estatus_factura,id',
            ReFacturaProductoModel::ACTIVO => 'required|boolean',
            ReFacturaProductoModel::USER_ID => 'nullable',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            ReFacturaProductoModel::FACTURA_ID => 'required|numeric|exists:factura,id',
            ReFacturaProductoModel::PRODUCTO_ID => 'required|numeric|exists:producto,id',
            ReFacturaProductoModel::ESTATUS_FACTURA_ID => 'required|numeric|exists:estatus_factura,id',
            ReFacturaProductoModel::ACTIVO => 'required|boolean',
            ReFacturaProductoModel::USER_ID => 'nullable',
        ];
    }
    
}
