<?php

namespace App\Servicios\Refacciones;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Servicios\Core\ServicioDB;

class ServicioValidaPedidoProducto extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'validar';
        $this->modelo = new ProductosPedidosModel();
    }

    public function validarProductoPedido($parametros)
    {
        $query = $this->modelo->select(
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_SOLICITADA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_BACKORDER,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_CARGADA,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID,
            ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PROVEEDOR_ID,
            ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION,
            ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA
        )
            ->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID
            );

        $query->whereRaw('(producto_pedidos.cantidad_solicitada > producto_pedidos.cantidad_cargada or producto_pedidos.cantidad_cargada is null)');
        $query->where(ProductosPedidosModel::PRODUCTO_ID, '=', $parametros[ProductosPedidosModel::PRODUCTO_ID]);
        $query->where(ProductosPedidosModel::PROVEEDOR_ID, '=', $parametros[ProductosPedidosModel::PROVEEDOR_ID]);
        return $query->get()->first();
    }

    public function updatepedido($parametros)
    {
        $cantidad_factura = intval($parametros['cantidad_factura']);
        $cantidad_cargada = is_null($parametros['cantidad_cargada']) ? $cantidad_factura : $parametros['cantidad_cargada'] + $cantidad_factura;
        $cantidad_solicitada = $parametros['cantidad_solicitada'];
        
        $back_order = $cantidad_solicitada - $cantidad_cargada;
        $estatus = $back_order > 0 ? ProductosPedidosModel::ESTATUS_BACKORDER : ProductosPedidosModel::ESTATUS_FINALIZADO;

        return $this->massUpdateWhereId(
            ProductosPedidosModel::ID,
            $parametros['producto_pedido_id'],
            [
                'cantidad_cargada' => $cantidad_cargada,
                'cantidad_backorder' => $back_order,
                'estatus_id' => $estatus
            ]
        );

    }
}
