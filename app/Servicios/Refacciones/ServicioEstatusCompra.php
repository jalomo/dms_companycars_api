<?php

namespace App\Servicios\Refacciones;

use App\Servicios\Core\ServicioDB;
use App\Models\Refacciones\EstatusCompra as EstatusCompraModel;

class ServicioEstatusCompra extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus compra';
        $this->modelo = new EstatusCompraModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusCompraModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusCompraModel::NOMBRE => 'required|string|regex:/^[\pL\s]+$/u|max:50|unique:' . $this->modelo->getTable() . ',nombre',
        ];
    }
}
