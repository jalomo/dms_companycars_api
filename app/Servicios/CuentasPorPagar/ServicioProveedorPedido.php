<?php

namespace App\Servicios\CuentasPorPagar;

use App\Models\Refacciones\ProductosModel;
use App\Models\Refacciones\ProductosPedidosModel;
use App\Models\Refacciones\ProveedorPedidoProductoModel;
use App\Models\Refacciones\ProveedorRefacciones;
use App\Servicios\Core\ServicioDB;

class ServicioProveedorPedido extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'proveedor';
        $this->modelo = new ProductosPedidosModel();
    }

    public function polizaspedidoproveedor($parametros)
    {
        $query = $this->modelo
            ->select(
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ESTATUS_ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::CANTIDAD_SOLICITADA,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::MA_PEDIDO_ID,
                ProductosModel::getTableName() . '.' . ProductosModel::NO_IDENTIFICACION_FACTURA,
                ProductosModel::getTableName() . '.' . ProductosModel::DESCRIPCION
            )
            ->join(
                ProductosModel::getTableName(),
                ProductosModel::getTableName() . '.' . ProductosModel::ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::PRODUCTO_ID
            )
            ->join(
                ProveedorPedidoProductoModel::getTableName(),
                ProveedorPedidoProductoModel::getTableName() . '.' . ProveedorPedidoProductoModel::PRODUCTO_PEDIDO_ID,
                ProductosPedidosModel::getTableName() . '.' . ProductosPedidosModel::ID
            )
            ->join(
                ProveedorRefacciones::getTableName(),
                ProveedorRefacciones::getTableName() . '.' . ProveedorRefacciones::ID,
                ProveedorPedidoProductoModel::getTableName() . '.' . ProveedorPedidoProductoModel::PROVEEDOR_ID
            );

            if(isset($parametros[ProveedorPedidoProductoModel::PROVEEDOR_ID])){
                $query->where(
                    ProveedorPedidoProductoModel::getTableName() . '.'.ProveedorPedidoProductoModel::PROVEEDOR_ID,
                    '=',
                    $parametros[ProveedorPedidoProductoModel::PROVEEDOR_ID]
                );
            }

            return $query->get();
    }

}
