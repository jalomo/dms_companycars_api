<?php

namespace App\Servicios\CuentasPorPagar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorPagar\CatTipoAbonoModel;

class ServicioCatTipoAbono extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo abono';
        $this->modelo = new CatTipoAbonoModel();
    }

  
}
