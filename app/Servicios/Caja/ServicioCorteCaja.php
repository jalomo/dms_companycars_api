<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\CorteCajaModel;
use App\Models\Caja\MovimientoCajaModel;
use App\Models\Caja\CatalogoCajaModel;
use App\Models\Usuarios\User;

class ServicioCorteCaja extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'corte de caja';
        $this->modelo = new CorteCajaModel();
    }

    public function getReglasGuardar()
    {
        return [
            CorteCajaModel::CAJA_ID => 'required|numeric',
            CorteCajaModel::MOVIMIENTO_CAJA_ID => 'required|numeric',
            CorteCajaModel::FECHA_TRANSACCION => 'date|required',
            CorteCajaModel::USUARIO_REGISTRO => 'required|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CorteCajaModel::CAJA_ID => 'required|numeric',
            CorteCajaModel::MOVIMIENTO_CAJA_ID => 'required|numeric',
            CorteCajaModel::FECHA_TRANSACCION => 'date|required',
            CorteCajaModel::USUARIO_REGISTRO => 'required|numeric'
        ];
    }

    public function getByUsuarioFecha($params)
    {
        return $this->modelo
            ->select(
                CorteCajaModel::getTableName(). '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO
            )
            ->join(User::getTableName(), User::getTableName().'.'.User::ID, CorteCajaModel::USUARIO_REGISTRO)
            ->where(CorteCajaModel::USUARIO_REGISTRO, $params[CorteCajaModel::USUARIO_REGISTRO])
            ->where(CorteCajaModel::FECHA_TRANSACCION, $params[CorteCajaModel::FECHA_TRANSACCION])
            ->first();
    }

    public function getByUsuario($params)
    {
        return $this->modelo
            ->select(
                CorteCajaModel::getTableName(). '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO,
                MovimientoCajaModel::getTableName() . '.' . MovimientoCajaModel::NOMBRE . ' as MovimientoNombre',
                CatalogoCajaModel::getTableName() . '.' . CatalogoCajaModel::NOMBRE . ' as CajaNombre'
            )
            ->join(User::getTableName(), User::getTableName().'.'.User::ID, CorteCajaModel::USUARIO_REGISTRO)
            ->join(MovimientoCajaModel::getTableName(), MovimientoCajaModel::getTableName().'.'.MovimientoCajaModel::ID, CorteCajaModel::MOVIMIENTO_CAJA_ID)
            ->join(CatalogoCajaModel::getTableName(), CatalogoCajaModel::getTableName().'.'. CatalogoCajaModel::ID, CorteCajaModel::CAJA_ID)
            ->where(CorteCajaModel::USUARIO_REGISTRO, $params[CorteCajaModel::USUARIO_REGISTRO])
            ->get();
    }
}
