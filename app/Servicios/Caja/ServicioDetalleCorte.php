<?php

namespace App\Servicios\Caja;

use App\Servicios\Core\ServicioDB;
use App\Models\Caja\DetalleCorteModel;
use App\Models\Refacciones\CatTipoPagoModel;

class ServicioDetalleCorte extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'cajas';
        $this->modelo = new DetalleCorteModel();
    }

    public function getReglasGuardar()
    {

        return [
            DetalleCorteModel::CORTE_CAJA_ID => 'required|numeric|exists:corte_caja,id',
            DetalleCorteModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            DetalleCorteModel::TOTAL_CANTIDAD_CAJA => 'required|numeric',
            DetalleCorteModel::TOTAL_CANTIDAD_REPORTADA => 'required|numeric',        
            DetalleCorteModel::FALTANTES => 'required|numeric',        
            DetalleCorteModel::SOBRANTES => 'required|numeric'        
        ];
    }
    public function getReglasUpdate()
    {
        return [ 
            DetalleCorteModel::CORTE_CAJA_ID => 'required|numeric|exists:corte_caja,id',
            DetalleCorteModel::TIPO_PAGO_ID => 'required|numeric|exists:cat_tipo_pago,id',
            DetalleCorteModel::TOTAL_CANTIDAD_CAJA => 'required|numeric',
            DetalleCorteModel::TOTAL_CANTIDAD_REPORTADA => 'required|numeric',
            DetalleCorteModel::FALTANTES => 'required|numeric',        
            DetalleCorteModel::SOBRANTES => 'required|numeric'                   
        ];
    }

    
    public function getByCajaId($params) {
        return $this->modelo
            ->select(
                DetalleCorteModel::getTableName(). '.*',
                CatTipoPagoModel::getTableName() . '.' . CatTipoPagoModel::NOMBRE . ' as TipoPagoNombre')
            ->join(CatTipoPagoModel::getTableName(), CatTipoPagoModel::getTableName(). '.' .CatTipoPagoModel::ID , DetalleCorteModel::TIPO_PAGO_ID)
            ->where(DetalleCorteModel::CORTE_CAJA_ID, $params[DetalleCorteModel::CORTE_CAJA_ID])
            ->get();
    }

}
