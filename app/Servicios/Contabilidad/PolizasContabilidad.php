<?php

namespace App\Servicios\Contabilidad;

use App\Servicios\Core\ServicioDB;
use App\Models\Contabilidad\PolizasContabilidadModel;

class PolizasContabilidad extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'polizas contabilidad';
        $this->modelo = new PolizasContabilidadModel();
    }

    public function getReglasGuardar()
    {
        return [
            PolizasContabilidadModel::POLIZA => 'required',
            PolizasContabilidadModel::CONCEPTO => 'required',
            PolizasContabilidadModel::FORMULO => 'required',
            PolizasContabilidadModel::CUENTA => 'required',
            PolizasContabilidadModel::SUBCUENTA => 'required',
            
        ];
    }
    public function getReglasUpdate()
    {
        return [
            PolizasContabilidadModel::POLIZA => 'nullable',
            PolizasContabilidadModel::CONCEPTO => 'nullable',
            PolizasContabilidadModel::FORMULO => 'nullable',
            PolizasContabilidadModel::CUENTA => 'nullable',
            PolizasContabilidadModel::SUBCUENTA => 'nullable',
        ];
    }
}
