<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\RolesModulosModel;
use App\Models\Usuarios\RolModel;

class ServicioRoles extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new RolModel();
        $this->modeloRolesModulos = new RolesModulosModel();
        $this->servicioRolesModulos = new ServicioRolesModulos;
        $this->recurso = 'roles';
    }

    public function getReglasGuardar()
    {
        return [
            RolModel::ROL => 'required|unique:roles,rol'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            RolModel::ROL => 'required|unique:roles,rol'
        ];
    }

    public function getReglasStoreModulos()
    {
        return [
            RolesModulosModel::MODULO_ID => 'required|array',
            RolesModulosModel::ROL_ID => 'required|numeric',
        ];
    }

    public function getReglasByRol()
    {
        return [
            RolesModulosModel::ROL_ID => 'required|numeric'
        ];
    }

    public function storeRolesModulos($parametros)
    {
        $modulos = $parametros[RolesModulosModel::MODULO_ID];
        $this->modeloRolesModulos
            ->where(RolesModulosModel::ROL_ID, '=',  $parametros[RolesModulosModel::ROL_ID])
            ->delete();

        foreach ($modulos as $key => $item) {
            $this->servicioRolesModulos->crear([
                RolesModulosModel::ROL_ID => $parametros[RolesModulosModel::ROL_ID],
                RolesModulosModel::MODULO_ID => $item,
            ]);
        }
    }

    public function rolesAndModulos($parametros)
    {
        $query = $this->modelo->select(
            'modulo.id as modulo_id',
            'modulo.nombre as modulo',
            'modulo.icono',
            'modulo.orden',
            'roles.id as rol_id',
            'roles.rol'
        );
        $query->join('det_roles_modulos', 'det_roles_modulos.rol_id', '=','roles.id');
        $query->join('modulo', 'det_roles_modulos.modulo_id', '=','modulo.id');
        
        if (isset($parametros[RolesModulosModel::ROL_ID])) {
            $query->where(RolesModulosModel::ROL_ID, '=', $parametros[RolesModulosModel::ROL_ID]);
        }

        return  $query->orderBy('modulo.orden','ASC')->get();
    }
}
