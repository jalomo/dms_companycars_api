<?php

namespace App\Servicios\Usuarios\Menu;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\MenuVistasModel;

class ServiciomenuVistas extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new MenuVistasModel();
        $this->recurso = 'submenus';
    }

    public function getReglasGuardar()
    {
        return [
            MenuVistasModel::CONTROLADOR => 'required',
            MenuVistasModel::NOMBRE => 'required',
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            MenuVistasModel::LINK => 'nullable',
            MenuVistasModel::MODULO => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            MenuVistasModel::CONTROLADOR => 'required',
            MenuVistasModel::NOMBRE => 'required',
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id',
            MenuVistasModel::LINK => 'nullable',
            MenuVistasModel::MODULO => 'required'
        ];
    }

    public function getReglasMenuVista()
    {
        return [
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id'
        ];
    }

    public function vistasBySubmenuId($parametros)
    {
        $query = $this->modelo->select(
            'menu_vistas.id as vista_id',
            'menu_vistas.nombre as nombre_vista',
            'menu_vistas.link',
            'menu_vistas.controlador',
            'menu_vistas.modulo',
            'menu_submenu.id as submenu_id',
            'menu_submenu.nombre_submenu'
        );

        $query->join('menu_submenu', 'menu_vistas.submenu_id', '=', 'menu_submenu.id');

        if (isset($parametros[MenuVistasModel::SUBMENU_ID])) {
            $query->where(MenuVistasModel::SUBMENU_ID, '=', $parametros[MenuVistasModel::SUBMENU_ID]);
        }

        return  $query->get();
    }

    public function getReglasVistas()
    {
        return [
            MenuVistasModel::SUBMENU_ID => 'required|exists:menu_submenu,id'
        ];
    }

    public function getVistas($parametros)
    {
        return $this->modelo
        ->where(MenuVistasModel::SUBMENU_ID, '=', $parametros[MenuVistasModel::SUBMENU_ID])
        ->get();
    }
}
