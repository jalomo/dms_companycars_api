<?php

namespace App\Servicios\Usuarios\Log;

use App\Servicios\Core\ServicioDB;
use App\Models\Usuarios\Log\LogSessionesModel;

class ServicioLogSessiones extends ServicioDB
{
    public function __construct()
    {
        $this->modelo = new LogSessionesModel();
        $this->recurso = 'log sessiones';
    }

    public function getReglasGuardar()
    {
        return [
            LogSessionesModel::DIRECCION_IP => 'required',
            LogSessionesModel::USUARIO_ID => 'required|exists:usuarios,id',
            LogSessionesModel::DESCRIPCION => 'required'
        ];
    }

    public function getReglasUpdate()
    {
        return [
            LogSessionesModel::DIRECCION_IP => 'required',
            LogSessionesModel::USUARIO_ID => 'required|exists:usuarios,id',
            LogSessionesModel::DESCRIPCION => 'required'
        ];
    }
}
