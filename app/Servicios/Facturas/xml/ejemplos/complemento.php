<?php
error_reporting(1);
ini_set('display_errors', 1);

require dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'autoload.php';

$cfdi = new Comprobante();

// Preparar valores
$moneda = 'XXX';
$subtotal  = 0;
$iva       =  11.40;
//$descuento =   1.40;
$total     = 0;
$fecha     = time();

// Establecer valores generales
$cfdi->LugarExpedicion   = '12345';
//$cfdi->FormaPago         = '27';
//$cfdi->MetodoPago        = 'PUE';
$cfdi->Folio             = '21';
$cfdi->Serie             = 'A';
$cfdi->TipoDeComprobante = 'P';
//$cfdi->TipoCambio        = 1;
$cfdi->Moneda            = $moneda;
$cfdi->setSubTotal($subtotal);
$cfdi->setTotal($total);
$cfdi->setDescuento($descuento);
$cfdi->setFecha($fecha);

// Agregar emisor
$cfdi->Emisor = Emisor::init(
    'LAN7008173R5',                    // RFC
    '622',                             // Régimen Fiscal
    'Emisor Ejemplo'                   // Nombre (opcional)
);

// Agregar receptor
$cfdi->Receptor = Receptor::init(
    'XAXX010101000',                   // RFC
    'I08',                             // Uso del CFDI
    'Receptor Ejemplo'                 // Nombre (opcional)
);

// Preparar datos del concepto 1
$concepto = Concepto::init(
    '52141807',                        // clave producto SAT
    '2',                               // cantidad
    'P83',                             // clave unidad SAT
    'Nombre del producto de ejemplo',
    95.00,                             // precio
    190.00                             // importe
);
$concepto->NoIdentificacion = 'PR01'; // clave de producto interna
$concepto->Unidad = 'Servicio';       // unidad de medida interna
$concepto->Descuento = 0.0;

// Agregar impuesto (traslado) al concepto 1
$traslado = new ConceptoTraslado;
$traslado->Impuesto = '002';          // IVA
$traslado->TipoFactor = 'Cuota';
$traslado->TasaOCuota = 0.16;
$traslado->Base = $subtotal;
$traslado->Importe = $iva;
$concepto->agregarImpuesto($traslado);

// Agregar impuesto (retencion) al concepto 1
$traslado = new ConceptoRetencion;
$traslado->Impuesto = '001';          // ISR
$traslado->TipoFactor = 'Tasa';
$traslado->TasaOCuota = 0.10;
$traslado->Base = $subtotal;
$traslado->Importe = $traslado->Base * $traslado->TasaOCuota;
$concepto->agregarImpuesto($traslado);

// Agregar concepto 1 a CFDI
$cfdi->agregarConcepto($concepto);

// Agregar más conceptos al CFDI
// $concepto = Concepto::init(...);
// ...
// $cfdi->agregarConcepto($concepto);





$tipoRelacion = '04';
$cfdi->CfdiRelacionados = CfdiRelacionados::init($tipoRelacion);
$cfdi->CfdiRelacionados->agregarUUID('670B9562-B30D-52D5-B827-655787665500');










$complementoPago = new  ComplementoPago();



$complementoPago->addDoctoRelacionado(
  'DoctoRelacionado',//$item,
  '970e4f32-0fe0-11e7-93ae-92361f002671',//$IdDocumento,
  'ssd',//$Serie,
  'sdsd',//$Folio,
  'MXN',//$MonedaDR,
  '100',//$TipoCambioDR,
  'PPD',//$MetodoDePagoDR,
  '2',//$NumParcialidad,
  '10000.00',//$ImpSaldoAnt,
  '20000.00',//$ImpPagado,
  '4000.00'//$ImpSaldoInsoluto
);

/*
$complementoPago->addPago(
  '2017-03-22T09:00:00',//$FechaPago,
  '06',//$FormaDePagoP,
  'MXN',//$MonedaP,
  null,//$TipoCambioP,
  '10000',//$Monto,
  '0000051',//$NumOperacion=null,
  '',//$RfcEmisorCtaOrd=null,
  '',//$NomBancoOrdExt=null,
  '',//$CtaOrdenante=null,
  '',//$RfcEmisorCtaBen=null,
  '',//$CtaBeneficiario=null,
  '',//$TipoCadPago=null,
  '',//$CertPago=null,
  '',//$CadPago=null,
  ''//$SelloPago=null
);

*/
// Agregar complemento al CFDI
$cfdi->Complemento[] = $complementoPago;














// Ejemplo de objeto DOMDocument que contiene una Addenda

$addendaDoc = new DOMDocument();
$child = $addendaDoc->createElement('EjemploAddenda');
$child->setAttribute('atributo', 'valor');
$addendaDoc->appendChild($child);

// Agregar addenda al CFDI
$cfdi->Addenda[] = $addendaDoc;















// Mostrar XML del CFDI generado hasta el momento
// header('Content-type: application/xml; charset=UTF-8');
// echo $cfdi->obtenerXml();
// die;

// Cargar certificado que se utilizará para generar el sello del CFDI
$cert = new UtilCertificado();

// Si no se especifica la ruta manualmente, se intentará obtener automatícamente
// UtilCertificado::establecerRutaOpenSSL();

$ok = $cert->loadFiles(
    dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.cer',
    dirname(__FILE__).DIRECTORY_SEPARATOR.'LAN7008173R5.key',
    '12345678a'
);
if(!$ok) {
    die('Ha ocurrido un error al cargar el certificado.');
}

$ok = $cfdi->sellar($cert);
if(!$ok) {
    die('Ha ocurrido un error al sellar el CFDI.');
}

// Mostrar XML del CFDI con el sello
header('Content-type: application/xml; charset=UTF-8');
//header('Content-Disposition: attachment; filename="xml/tu_archivo.xml');
echo $cfdi->obtenerXml();
die;

// Mostrar objeto que contiene los datos del CFDI
print_r($cfdi);


die;



die('OK');
