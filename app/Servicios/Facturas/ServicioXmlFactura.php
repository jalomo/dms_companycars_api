<?php

namespace App\Servicios\Facturas;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Facturas\xml\lib\autoload;
use App\Servicios\Facturas\xml\lib\Comprobante;
use App\Servicios\Facturas\xml\lib\Concepto;
use App\Servicios\Facturas\xml\lib\ConceptoRetencion;
use App\Servicios\Facturas\xml\lib\ConceptoTraslado;
use App\Servicios\Facturas\xml\lib\Emisor;
use App\Servicios\Facturas\xml\lib\Receptor;
use App\Servicios\Facturas\xml\lib\UtilCertificado;

class ServicioXmlFactura extends ServicioDB
{
    private $cfdi;
    private $subtotal;
    private $total;
    private $descuento;
    private $fecha;
    private $iva;
    public function __construct()
    {
        $this->cfdi = new Comprobante();
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setIva($iva)
    {
        $this->iva = $iva;
    }

    public function getIva()
    {
        return $this->iva;
    }

    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    }

    public function getDescuento()
    {
        return $this->descuento;
    }

    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    public function getSubtotal()
    {
        return $this->subtotal;
    }

    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function loadXmlData($path_xml)
    {
        // $xml = simplexml_load_file($path_xml);
        // $ns = $xml->getNamespaces(true);
        // $xml->registerXPathNamespace('c', $ns['cfdi']);
        // $xml->registerXPathNamespace('t', $ns['tfd']);
        // cargar de db
        // return $xml;
    }

    public function getDataComprobante(array $data)
    {
        if (count($data) > 0) {
            $comprobante = $data[0];
            $tipo_cambio = $this->validateAndCastItem($comprobante['TipoCambio']);
            $fecha = $this->validateAndCastItem($comprobante['Fecha']);
            $total = $this->validateAndCastItem($comprobante['Total']);
            $subtotal = $this->validateAndCastItem($comprobante['SubTotal']);
            $moneda = $this->validateAndCastItem($comprobante['Moneda']);
            $forma_pago = $this->validateAndCastItem($comprobante['FormaPago']);
            $tipo_comprobante = $this->validateAndCastItem($comprobante['TipoDeComprobante']);
            $lugar_expedicion = $this->validateAndCastItem($comprobante['LugarExpedicion']);
            $metodo_pago = $this->validateAndCastItem($comprobante['MetodoPago']);
            $folio = $this->validateAndCastItem($comprobante['Folio']);
            $serie = $this->validateAndCastItem($comprobante['Serie']);

            $this->setSubtotal($subtotal);
            $this->setTotal($total);
            $this->setFecha($fecha);

            $this->cfdi->LugarExpedicion   = $lugar_expedicion;
            $this->cfdi->FormaPago         = $forma_pago;
            $this->cfdi->MetodoPago        = $metodo_pago;
            $this->cfdi->Folio             = $folio;
            $this->cfdi->Serie             = $serie;
            $this->cfdi->TipoDeComprobante = $tipo_comprobante;
            $this->cfdi->TipoCambio        = $tipo_cambio;
            $this->cfdi->Moneda            = $moneda;
            $this->cfdi->setSubTotal($subtotal);
            $this->cfdi->setTotal($total);
            // $this->cfdi->setDescuento($descuento);
            $this->cfdi->setFecha($fecha);
        }
    }

    public function getDataEmisor(array $data)
    {
        foreach ($data as $emisor) {
            $this->cfdi->Emisor = Emisor::init(
                $emisor['Rfc'],
                $emisor['RegimenFiscal'],
                $emisor['Nombre']
            );
        }
    }

    public function getDataReceptor(array $data)
    {
        foreach ($data as $key => $item) {
            $this->cfdi->Receptor = Receptor::init(
                $item['Rfc'],
                $item['UsoCFDI'],
                $item['Nombre']
            );
        }
    }

    public function getDataConcepto(array $data)
    {
        foreach ($data as $key => $item) {
            $concepto = Concepto::init(
                $item['ClaveProdServ'], // clave producto SAT
                $item['Cantidad'], // cantidad
                $item['NoIdentificacion'], // clave unidad SAT
                $item['Descripcion'], //Nombre del producto
                $item['ValorUnitario'], // precio
                $item['Importe'] // importe
            );
            $concepto->NoIdentificacion = $item['NoIdentificacion']; // clave de producto interna
            $concepto->Unidad = $item['Unidad']; // unidad de medida interna
            $concepto->Descuento = isset($item['Descuento']) ? $item['Descuento'] : 0;
        }
    }

    public function getDataConceptoTraslado(array $data, $concepto)
    {
        foreach ($data as $key => $item) {
            $traslado = new ConceptoTraslado();
            $traslado->Impuesto = $item['Impuesto'];          // IVA
            $traslado->TipoFactor = self::TIPO_FACTOR_CUOTA; //'Cuota';
            $traslado->TasaOCuota = $item['TasaOCuota'];
            $traslado->Base = $item['Base'];
            $traslado->Importe = $item['Importe'];
            $concepto->agregarImpuesto($traslado);
        }
    }

    public function getDataconceptoRetencion(array $data, $concepto)
    {
        foreach ($data as $key => $item) {
            $traslado = new ConceptoRetencion();
            // Agregar impuesto (retencion) al concepto 1
            $traslado->Impuesto = $item['Impuesto']; // ISR
            $traslado->TipoFactor = self::TIPO_FACTOR_TASA; //'Tasa';
            $traslado->TasaOCuota = $item['TasaOCuota'];
            $traslado->Base = $item['Base'];
            $traslado->Importe = $traslado->Base * $traslado->TasaOCuota;
            $concepto->agregarImpuesto($traslado);
        }
    }

    public function validateAndCastItem($element)
    {
        return isset($element) ? (string) trim($element) : '';
    }

    // $this->getDataComprobante($xml->xpath('//cfdi:Comprobante'));
    // $this->getDataEmisor($xml->xpath('//cfdi:Comprobante//cfdi:Emisor'));
    // $this->getDataReceptor($xml->xpath('//cfdi:Comprobante//cfdi:Receptor'));
    // $this->getDataConcepto($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto'));
    // $this->generarXml();
    // $this->getDataConceptoTraslado($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado'));
    // $this->getDataconceptoRetencion($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado'));

    public function generarXml()
    {
        header('Content-type: application/xml; charset=UTF-8');
        echo $this->cfdi->obtenerXml();
    }

    public const TIPO_FACTOR_TASA = 'Tasa';
    public const TIPO_FACTOR_CUOTA = 'Cuota';
    public const MONEDA = 'MXN';
}
