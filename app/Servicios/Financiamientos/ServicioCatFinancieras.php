<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatFinancierasModel;

class ServicioCatFinancieras extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo financieras';
        $this->modelo = new CatFinancierasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatFinancierasModel::FINANCIERA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatFinancierasModel::FINANCIERA => 'required'
        ];
    }
}


