<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\EstatusFinanciamientosModel;

class ServicioEstatusFinanciamientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'estatus financiamientos';
        $this->modelo = new EstatusFinanciamientosModel();
    }

    public function getReglasGuardar()
    {
        return [
            EstatusFinanciamientosModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            EstatusFinanciamientosModel::ESTATUS => 'required'
        ];
    }
}


