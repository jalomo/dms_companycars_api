<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\CatalogoAutosModel;
use App\Models\Financiamientos\FinanciamientosModel;
use App\Models\Refacciones\ClientesModel;
use App\Models\Usuarios\User;

class ServicioFinanciamientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'financiamientos';
        $this->modelo = new FinanciamientosModel();
    }

    public function getReglasGuardar()
    {
        return [
            FinanciamientosModel::ID_ASESOR_VENTAS => 'required',
            FinanciamientosModel::ID_CLIENTE => 'required',
            FinanciamientosModel::FECHA_BURO => 'required',
            FinanciamientosModel::SE_INGRESA => 'required',
            FinanciamientosModel::ID_PROPUESTA => 'required',
            FinanciamientosModel::FECHA_INGRESO => 'required',
            FinanciamientosModel::HAY_UNIDAD => 'required',
            FinanciamientosModel::ID_FINANCIERA1 => 'nullable',
            FinanciamientosModel::ID_FINANCIERA2 => 'nullable',
            FinanciamientosModel::ID_UNIDAD => 'required',
            FinanciamientosModel::ID_CATALOGO => 'required',
            FinanciamientosModel::PRECIO_UNIDAD => 'required',
            FinanciamientosModel::ENGANCHE => 'required',
            FinanciamientosModel::MF => 'required',
            FinanciamientosModel::PLAN => 'required',
            FinanciamientosModel::COMISION_FINANCIERA => 'required',
            FinanciamientosModel::COMISION_ASESOR => 'required',
            FinanciamientosModel::ID_ESTATUS => 'nullable',
            FinanciamientosModel::ID_PERFIL => 'required',
            FinanciamientosModel::SCORE => 'required',
            FinanciamientosModel::CUENTA_MAS_ALTA => 'required',
            FinanciamientosModel::TIEMPO_CUENTA => 'required',
            FinanciamientosModel::MENSUALIDAD => 'required',
            FinanciamientosModel::EGRESO_BRUTO => 'required',
            FinanciamientosModel::TOTAL => 'required',
            FinanciamientosModel::TOTAL2 => 'required',
            FinanciamientosModel::INGRESO_INDEP => 'required',
            FinanciamientosModel::INGRESO_NOMINA => 'required',
            FinanciamientosModel::NIVEL_ENDEUDAMIENTO => 'required',
            FinanciamientosModel::ARRAIGO_CASA => 'required',
            FinanciamientosModel::ARRAIGO_EMPLEO => 'required',
            FinanciamientosModel::COMENTARIO => 'nullable',
            FinanciamientosModel::MONTO_FINANCIAR => 'required',
            FinanciamientosModel::ID_TIPO_SEGURO => 'required',
            FinanciamientosModel::ID_COMPANIA => 'required',
            FinanciamientosModel::ID_UDI => 'required',
            FinanciamientosModel::COMISION_AGENCIA => 'required',
            FinanciamientosModel::NO_POLIZA => 'required',
            FinanciamientosModel::INICIO_VIGENCIA => 'required',
            FinanciamientosModel::VENCIMIENTO_ANUAL => 'required',
            FinanciamientosModel::PRIMA_TOTAL => 'required',
            FinanciamientosModel::IVA => 'required',
            FinanciamientosModel::EMISION_POLIZA => 'required',
            FinanciamientosModel::PRIMA_NETA => 'required',
            FinanciamientosModel::UDI_AGENCIA => 'required',
            FinanciamientosModel::COMISION_ASESOR_PRIMA => 'required',
            FinanciamientosModel::ID_COMISION => 'required',
            FinanciamientosModel::ID_TIPO_SEGURO_FC => 'required',
            FinanciamientosModel::ID_COMPANIA_FC => 'required',
            FinanciamientosModel::NO_POLIZA_FC => 'required',
            FinanciamientosModel::INICIO_VIGENCIA_FC => 'required',
            FinanciamientosModel::VENCIMIENTO_ANUAL_FC => 'required',
            FinanciamientosModel::PRIMA_TOTAL_FC => 'required',
            FinanciamientosModel::IVA_FC => 'required',
            FinanciamientosModel::EMISION_POLIZA_FC => 'required',
            FinanciamientosModel::PRIMA_NETA_FC => 'required',
            FinanciamientosModel::UDI_AGENCIA_FC => 'required',
            FinanciamientosModel::COMISION_ASESOR_PRIMA_FC => 'required',
            FinanciamientosModel::ID_UDI_FC => 'required',
            FinanciamientosModel::BONO => 'required',
            FinanciamientosModel::PLAZO => 'required',
            FinanciamientosModel::FECHA_CONDICIONAMIENTO => 'required',
            FinanciamientosModel::HORA_CONDICIONAMIENTO => 'required',
            FinanciamientosModel::FECHA_CUMPLIMIENTO => 'required',
            FinanciamientosModel::HORA_CUMPLIMIENTO => 'required',
            FinanciamientosModel::FECHA_APROBACION => 'required',
            FinanciamientosModel::HORA_APROBACION => 'required',
            FinanciamientosModel::FECHA_CONTRATO => 'required',
            FinanciamientosModel::HORA_CONTRATO => 'required',
            FinanciamientosModel::ID_ESTATUS_PISO => 'nullable',
            FinanciamientosModel::FECHA_COMPRA => 'required',
            FinanciamientosModel::HORA_COMPRA => 'required',
            FinanciamientosModel::COMENTARIOS_COMPRA => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            FinanciamientosModel::ID_ASESOR_VENTAS => 'required',
            FinanciamientosModel::ID_CLIENTE => 'required',
            FinanciamientosModel::FECHA_BURO => 'required',
            FinanciamientosModel::SE_INGRESA => 'required',
            FinanciamientosModel::ID_PROPUESTA => 'required',
            FinanciamientosModel::FECHA_INGRESO => 'required',
            FinanciamientosModel::HAY_UNIDAD => 'required',
            FinanciamientosModel::ID_FINANCIERA1 => 'nullable',
            FinanciamientosModel::ID_FINANCIERA2 => 'nullable',
            FinanciamientosModel::ID_UNIDAD => 'required',
            FinanciamientosModel::ID_CATALOGO => 'required',
            FinanciamientosModel::PRECIO_UNIDAD => 'required',
            FinanciamientosModel::ENGANCHE => 'required',
            FinanciamientosModel::MF => 'required',
            FinanciamientosModel::PLAN => 'required',
            FinanciamientosModel::COMISION_FINANCIERA => 'required',
            FinanciamientosModel::COMISION_ASESOR => 'required',
            FinanciamientosModel::ID_ESTATUS => 'nullable',
            FinanciamientosModel::ID_PERFIL => 'required',
            FinanciamientosModel::SCORE => 'required',
            FinanciamientosModel::CUENTA_MAS_ALTA => 'required',
            FinanciamientosModel::TIEMPO_CUENTA => 'required',
            FinanciamientosModel::MENSUALIDAD => 'required',
            FinanciamientosModel::EGRESO_BRUTO => 'required',
            FinanciamientosModel::TOTAL => 'required',
            FinanciamientosModel::TOTAL2 => 'required',
            FinanciamientosModel::INGRESO_INDEP => 'required',
            FinanciamientosModel::INGRESO_NOMINA => 'required',
            FinanciamientosModel::NIVEL_ENDEUDAMIENTO => 'required',
            FinanciamientosModel::ARRAIGO_CASA => 'required',
            FinanciamientosModel::ARRAIGO_EMPLEO => 'required',
            FinanciamientosModel::COMENTARIO => 'nullable',
            FinanciamientosModel::MONTO_FINANCIAR => 'required',
            FinanciamientosModel::ID_TIPO_SEGURO => 'required',
            FinanciamientosModel::ID_COMPANIA => 'required',
            FinanciamientosModel::ID_UDI => 'required',
            FinanciamientosModel::COMISION_AGENCIA => 'required',
            FinanciamientosModel::NO_POLIZA => 'required',
            FinanciamientosModel::INICIO_VIGENCIA => 'required',
            FinanciamientosModel::VENCIMIENTO_ANUAL => 'required',
            FinanciamientosModel::PRIMA_TOTAL => 'required',
            FinanciamientosModel::IVA => 'required',
            FinanciamientosModel::EMISION_POLIZA => 'required',
            FinanciamientosModel::PRIMA_NETA => 'required',
            FinanciamientosModel::UDI_AGENCIA => 'required',
            FinanciamientosModel::COMISION_ASESOR_PRIMA => 'required',
            FinanciamientosModel::ID_COMISION => 'required',
            FinanciamientosModel::ID_TIPO_SEGURO_FC => 'required',
            FinanciamientosModel::ID_COMPANIA_FC => 'required',
            FinanciamientosModel::NO_POLIZA_FC => 'required',
            FinanciamientosModel::INICIO_VIGENCIA_FC => 'required',
            FinanciamientosModel::VENCIMIENTO_ANUAL_FC => 'required',
            FinanciamientosModel::PRIMA_TOTAL_FC => 'required',
            FinanciamientosModel::IVA_FC => 'required',
            FinanciamientosModel::EMISION_POLIZA_FC => 'required',
            FinanciamientosModel::PRIMA_NETA_FC => 'required',
            FinanciamientosModel::UDI_AGENCIA_FC => 'required',
            FinanciamientosModel::COMISION_ASESOR_PRIMA_FC => 'required',
            FinanciamientosModel::ID_UDI_FC => 'required',
            FinanciamientosModel::BONO => 'required',
            FinanciamientosModel::PLAZO => 'required',
            FinanciamientosModel::FECHA_CONDICIONAMIENTO => 'required',
            FinanciamientosModel::HORA_CONDICIONAMIENTO => 'required',
            FinanciamientosModel::FECHA_CUMPLIMIENTO => 'required',
            FinanciamientosModel::HORA_CUMPLIMIENTO => 'required',
            FinanciamientosModel::FECHA_APROBACION => 'required',
            FinanciamientosModel::HORA_APROBACION => 'required',
            FinanciamientosModel::FECHA_CONTRATO => 'required',
            FinanciamientosModel::HORA_CONTRATO => 'required',
            FinanciamientosModel::ID_ESTATUS_PISO => 'nullable',
            FinanciamientosModel::FECHA_COMPRA => 'required',
            FinanciamientosModel::HORA_COMPRA => 'required',
            FinanciamientosModel::COMENTARIOS_COMPRA => 'nullable'
        ];
    }
    public function reglasUpdateBitacoraCompras(){
        return [
            FinanciamientosModel::FECHA_ENVIO => 'required|date',
            FinanciamientosModel::ANIO => 'required|numeric|digits:4',
            FinanciamientosModel::SERIE => 'required',
            FinanciamientosModel::ID_USO_SEGURO => 'required',
            FinanciamientosModel::ACCESORIOS => 'required',
            FinanciamientosModel::EXTENSION_GARANTIA => 'required',
            FinanciamientosModel::OBSERVACIONES => 'nullable',
        ];
    }
    public function buscarFinanciamientosPorParametros($parametros)
    {
        return $this->modelo
            ->select(
                FinanciamientosModel::getTableName() . '.*',
                User::getTableName() . '.' . User::NOMBRE,
                User::getTableName() . '.' . User::APELLIDO_PATERNO,
                User::getTableName() . '.' . User::APELLIDO_MATERNO,
                ClientesModel::getTableName() . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_PATERNO . ' as ap_cliente',
                ClientesModel::getTableName() . '.' . ClientesModel::APELLIDO_MATERNO . ' as am_cliente',
                CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::NOMBRE . ' as unidad'
            )
            ->from(FinanciamientosModel::getTableName())
            ->join(
                User::getTableName(),
                User::getTableName() . '.' . User::ID,
                '=',
                FinanciamientosModel::ID_ASESOR_VENTAS
            )
            ->join(
                ClientesModel::getTableName(),
                ClientesModel::getTableName() . '.' . ClientesModel::ID,
                '=',
                FinanciamientosModel::ID_CLIENTE
            )
            ->join(
                CatalogoAutosModel::getTableName(),
                CatalogoAutosModel::getTableName() . '.' . CatalogoAutosModel::ID,
                '=',
                FinanciamientosModel::ID_UNIDAD
            )
            ->get();
    }
    public function updateBitacoraCompras($parametros,$id){
        return  $this->massUpdateWhereId(FinanciamientosModel::ID,$id, $parametros);
    }
}
