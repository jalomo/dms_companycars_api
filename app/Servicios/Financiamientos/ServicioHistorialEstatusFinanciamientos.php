<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\catEstatusPlanPisoModel;
use App\Models\Financiamientos\EstatusFinanciamientosModel;
use App\Models\Financiamientos\FinanciamientosModel;
use App\Models\Financiamientos\HistorialEstatusFinanciamientosModel;
use App\Models\Usuarios\User;

class ServicioHistorialEstatusFinanciamientos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'historial estatus financiamientos';
        $this->modelo = new HistorialEstatusFinanciamientosModel();
        $this->servicio_financiamiento = new ServicioFinanciamientos();
    }

    public function getReglasGuardar()
    {
        return [
            HistorialEstatusFinanciamientosModel::ID_ESTATUS => 'required',
            HistorialEstatusFinanciamientosModel::COMENTARIO => 'required',
            HistorialEstatusFinanciamientosModel::ID_USUARIO => 'required',
            HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO => 'required',
            HistorialEstatusFinanciamientosModel::TIPO_COMENTARIO => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            HistorialEstatusFinanciamientosModel::ID_ESTATUS => 'required',
            HistorialEstatusFinanciamientosModel::COMENTARIO => 'required',
            HistorialEstatusFinanciamientosModel::ID_USUARIO => 'required',
            HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO => 'required',
            HistorialEstatusFinanciamientosModel::TIPO_COMENTARIO => 'required'
        ];
    }
    public function store($parametros)
    {
        if($parametros['tipo_comentario']==1){
            $this->servicio_financiamiento->massUpdateWhereId(FinanciamientosModel::ID, $parametros['id_financiamiento'], [FinanciamientosModel::ID_ESTATUS => $parametros['id_estatus'],FinanciamientosModel::COMENTARIO => $parametros['comentario']]);
        }else{
            $this->servicio_financiamiento->massUpdateWhereId(FinanciamientosModel::ID, $parametros['id_financiamiento'], [FinanciamientosModel::ID_ESTATUS_PISO => $parametros['id_estatus'],FinanciamientosModel::COMENTARIOS_COMPRA => $parametros['comentario']]);
        }
        $data_insert = [
            HistorialEstatusFinanciamientosModel::ID_ESTATUS => $parametros['id_estatus'],
            HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO => $parametros['id_financiamiento'],
            HistorialEstatusFinanciamientosModel::COMENTARIO => $parametros['comentario'],
            HistorialEstatusFinanciamientosModel::ID_USUARIO => $parametros['id_usuario'],
            HistorialEstatusFinanciamientosModel::TIPO_COMENTARIO => $parametros['tipo_comentario'],
        ];
        return $this->crear($data_insert);
    }
    public function buscarComentariosPorParametros($parametros){
        return $this->modelo->select(
                            HistorialEstatusFinanciamientosModel::getTableName().'.*',
                            EstatusFinanciamientosModel::getTableName().'.'.EstatusFinanciamientosModel::ESTATUS,
                            catEstatusPlanPisoModel::getTableName().'.'.catEstatusPlanPisoModel::ESTATUS.' as estatus_piso',
                            User::getTableName().'.'.User::NOMBRE,
                            User::getTableName().'.'.User::APELLIDO_PATERNO,
                            User::getTableName().'.'.User::APELLIDO_MATERNO
                            )
                            ->where(HistorialEstatusFinanciamientosModel::ID_FINANCIAMIENTO,$parametros['id_financiamiento'])
                            ->Leftjoin(
                                EstatusFinanciamientosModel::getTableName(),
                                EstatusFinanciamientosModel::getTableName().'.'.EstatusFinanciamientosModel::ID
                                ,'=',
                                HistorialEstatusFinanciamientosModel::getTableName().'.'.HistorialEstatusFinanciamientosModel::ID_ESTATUS
                            )
                            ->Leftjoin(
                                catEstatusPlanPisoModel::getTableName(),
                                catEstatusPlanPisoModel::getTableName().'.'.catEstatusPlanPisoModel::ID
                                ,'=',
                                HistorialEstatusFinanciamientosModel::getTableName().'.'.HistorialEstatusFinanciamientosModel::ID_ESTATUS
                            )
                            ->join(
                                User::getTableName(),
                                User::getTableName().'.'.User::ID
                                ,'=',
                                HistorialEstatusFinanciamientosModel::getTableName().'.'.HistorialEstatusFinanciamientosModel::ID_USUARIO
                            )
                            ->orderBy(HistorialEstatusFinanciamientosModel::CREATED_AT,'desc')
                            ->get();
    }
}
