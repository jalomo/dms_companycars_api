<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatPropuestasModel;

class ServicioCatPropuestas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo propuestas';
        $this->modelo = new CatPropuestasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatPropuestasModel::PROPUESTA => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatPropuestasModel::PROPUESTA => 'required'
        ];
    }
}


