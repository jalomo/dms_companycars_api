<?php

namespace App\Servicios\Financiamientos;

use App\Servicios\Core\ServicioDB;
use App\Models\Financiamientos\CatEstatusPlanPisoModel;

class ServicioCatEstatusPlanPiso extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus plan piso';
        $this->modelo = new CatEstatusPlanPisoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusPlanPisoModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusPlanPisoModel::ESTATUS => 'required'
        ];
    }
}



