<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\CatalogoAutosModel;

class ServicioCatalogoAutos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo autos';
        $this->modelo = new CatalogoAutosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoAutosModel::NOMBRE => 'required',
            CatalogoAutosModel::CLAVE => 'required',
            CatalogoAutosModel::TIEMPO_LAVADO => 'required',
            CatalogoAutosModel::CATEGORIA_ID => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoAutosModel::NOMBRE => 'required',
            CatalogoAutosModel::CLAVE => 'required',
            CatalogoAutosModel::TIEMPO_LAVADO => 'required',
            CatalogoAutosModel::CATEGORIA_ID => 'required'
        ];
    }

    public function getReglasBusquedaAuto()
    {
        return [
            CatalogoAutosModel::NOMBRE => 'required'
        ];
    }

    public function searchIdAuto($request)
    {
        $data = $this->modelo->where(CatalogoAutosModel::NOMBRE,$request->get(CatalogoAutosModel::NOMBRE))
            ->select(CatalogoAutosModel::ID)->limit(1)->get();
        
        return $data;
    }

    public function searchCatalogos($id_catalogo)
    {
        $data = $this->modelo->where(CatalogoAutosModel::CATEGORIA_ID,$id_catalogo);
        
        return $data->get();
    }
}
