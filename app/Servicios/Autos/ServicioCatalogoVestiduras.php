<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\CatalogoVestidurasModel;

class ServicioCatalogoVestiduras extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo vestiduras';
        $this->modelo = new CatalogoVestidurasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatalogoVestidurasModel::NOMBRE => 'required',
            CatalogoVestidurasModel::CLAVE => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatalogoVestidurasModel::NOMBRE => 'required',
            CatalogoVestidurasModel::CLAVE => 'required'
        ];
    }

    public function searchIdVestidura($nombre)
    {
        $data = $this->modelo->where(CatalogoVestidurasModel::NOMBRE,$nombre)
            ->select(CatalogoVestidurasModel::ID)->limit(1)->get();
        
        return $data;
    }
}
