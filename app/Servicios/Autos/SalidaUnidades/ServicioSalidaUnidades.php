<?php

namespace App\Servicios\Autos\SalidaUnidades;

use App\Servicios\Core\ServicioDB;
use App\Models\Autos\SalidaUnidades\AhorroCombustibleModel;
use App\Models\Autos\SalidaUnidades\ComentariosModel;
use App\Models\Autos\SalidaUnidades\ConfortModel;
use App\Models\Autos\SalidaUnidades\DesempenoModel;
use App\Models\Autos\SalidaUnidades\DocumentosModel;
use App\Models\Autos\SalidaUnidades\EspecialesModel;
use App\Models\Autos\SalidaUnidades\ExtrasModel;
use App\Models\Autos\SalidaUnidades\FuncionamientoVehiculoModel;
use App\Models\Autos\SalidaUnidades\IluminacionModel;
use App\Models\Autos\SalidaUnidades\OtrasTecnologiasModel;
use App\Models\Autos\SalidaUnidades\SalidaUnidadesModel;
use App\Models\Autos\SalidaUnidades\SeguridadModel;
use App\Models\Autos\SalidaUnidades\SeguridadPasivaModel;
use App\Models\Autos\SalidaUnidades\VehiculoHibridoModel;

class ServicioSalidaUnidades extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'salida unidades';
        $this->modelo = new SalidaUnidadesModel();
        $this->modeloAhorroCombustible = new AhorroCombustibleModel();
        $this->modeloComentarios = new ComentariosModel();
        $this->modeloConfort = new ConfortModel();
        $this->modeloDesempeno = new DesempenoModel();
        $this->modeloDocumentos = new DocumentosModel();
        $this->modeloEspeciales = new EspecialesModel();
        $this->modeloExtras = new ExtrasModel();
        $this->modeloFuncionamientoVehiculo = new FuncionamientoVehiculoModel();
        $this->modeloIluminacion = new IluminacionModel();
        $this->modeloOtrasTecnologias = new OtrasTecnologiasModel();
        $this->modeloSeguridad = new SeguridadModel();
        $this->modeloSeguridadPasiva = new SeguridadPasivaModel();
        $this->modeloVehiculoHibrido = new VehiculoHibridoModel();
    }

    public function getReglasGuardar()
    {
        return [
            SalidaUnidadesModel::NOMBRE_CLIENTE => 'required',
            SalidaUnidadesModel::ID_CATALOGO => 'required|numeric',
            SalidaUnidadesModel::NO_ECONOMICO => 'nullable',
            SalidaUnidadesModel::NO_SERIE => 'nullable',
            SalidaUnidadesModel::ID_MODELO => 'required|numeric',
            SalidaUnidadesModel::ID_COLOR => 'required|numeric',
            SalidaUnidadesModel::VEHICULO => 'nullable',
            SalidaUnidadesModel::NOMBRE_VENDEDOR => 'nullable',
            SalidaUnidadesModel::TIPO_VENTA => 'nullable',
            SalidaUnidadesModel::NO_ENCUESTA => 'nullable',
            SalidaUnidadesModel::NO_PEDIDO => 'nullable',
            SalidaUnidadesModel::FECHA_REGRESO => 'nullable',
            SalidaUnidadesModel::NOMBRE_ASESOR => 'nullable',
            SalidaUnidadesModel::URL_FIRMA_CLIENTE => 'nullable',
            SalidaUnidadesModel::URL_FIRMA_ASESOR => 'nullable',
            AhorroCombustibleModel::IVCT => 'nullable',
            AhorroCombustibleModel::TIVCT => 'nullable',
            ComentariosModel::EXPLICO_ASISTENCIA_FORD => 'required',
            ComentariosModel::EXPLICO_FORD_PROTECT => 'nullable',
            ComentariosModel::ACCESORIOS_PERSONALIZAR => 'nullable',
            ComentariosModel::PRUEBA_MANEJO => 'nullable',
            ComentariosModel::INFORMACION_MANTENIMIENTO => 'nullable',
            ComentariosModel::ASESOR_SERVICIO => 'nullable',
            ConfortModel::ACCIONAMIENTO_MANUAL => 'required',
            ConfortModel::ACCIONAMIENTO_ELECTRICO => 'nullable',
            ConfortModel::ASIENTO_DELANTERO_MASAJE => 'nullable',
            ConfortModel::ASIENTO_CALEFACCION => 'nullable',
            ConfortModel::EASY_POWER_FOLD => 'nullable',
            ConfortModel::MEMORIA_ASIENTOS => 'nullable',
            DesempenoModel::MOTORES => 'required',
            DesempenoModel::CLUTCH => 'nullable',
            DesempenoModel::CAJA_CAMBIOS => 'nullable',
            DesempenoModel::ARBOL_TRANSMISIONES => 'nullable',
            DesempenoModel::DIFERENCIAL => 'nullable',
            DesempenoModel::DIFERENCIAL_TRACCION => 'nullable',
            DesempenoModel::CAJA_REDUCTORA => 'nullable',
            DesempenoModel::CAJA_TRANSFERENCIA => 'nullable',
            DesempenoModel::SISTEMA_DINAMICO => 'nullable',
            DesempenoModel::DIRECCION => 'nullable',
            DesempenoModel::SUSPENSION => 'nullable',
            DesempenoModel::TOMA_PODER => 'nullable',
            DocumentosModel::CARTA_FACTURA => 'required',
            DocumentosModel::POLIZA_GARANTIA => 'nullable',
            DocumentosModel::POLIZA_SEGURO => 'nullable',
            DocumentosModel::TENENCIA => 'nullable',
            DocumentosModel::HERRAMIENTAS => 'nullable',
            EspecialesModel::PEPS => 'required',
            EspecialesModel::APERTIRA_CAJUELA => 'required',
            EspecialesModel::ENCENDIDO_REMOTO => 'required',
            EspecialesModel::TECHO_PANORAMICO => 'required',
            EspecialesModel::AIRE_ACONDICIONADO => 'required',
            EspecialesModel::EATC => 'required',
            EspecialesModel::CONSOLA_ENFRIAMIENTO => 'required',
            EspecialesModel::VOLANTE_AJUSTE_ALTURA => 'required',
            EspecialesModel::VOLANTE_CALEFACTADO => 'required',
            EspecialesModel::ESTRIBOS_ELECTRICOS => 'required',
            EspecialesModel::APERTURA_CAJA_CARGA => 'required',
            EspecialesModel::LIMPIAPARABRISAS => 'required',
            EspecialesModel::ESPEJO_ELECTRICO => 'required',
            EspecialesModel::ESPEJOS_ABATIBLES => 'required',
            ExtrasModel::CONECTIVIDAD => 'required',
            ExtrasModel::SYNC => 'nullable',
            ExtrasModel::MYFORD_TOUCH => 'nullable',
            ExtrasModel::MYFORD_TOUCH_NAVEGACION => 'nullable',
            ExtrasModel::SYNC_FORDPASS => 'nullable',
            ExtrasModel::APPLE_CARPLAY => 'nullable',
            ExtrasModel::ANDROID_AUTO => 'nullable',
            ExtrasModel::CONSOLA_SMARTPHONE => 'nullable',
            ExtrasModel::AUDIO_SHAKER_PRO => 'nullable',
            ExtrasModel::RADIO_HD => 'nullable',
            ExtrasModel::EQUIPO_SONY => 'nullable',
            ExtrasModel::PUERTOS_USB => 'nullable',
            ExtrasModel::BLUETOOH => 'nullable',
            ExtrasModel::WIFI => 'nullable',
            ExtrasModel::TARJETA_SD => 'nullable',
            ExtrasModel::INVERSOR_CORRIENTE => 'nullable',
            ExtrasModel::ESPEJO_ELECTROCROMATICO => 'nullable',
            FuncionamientoVehiculoModel::ASISTENTE_ARRANQUE => 'required',
            FuncionamientoVehiculoModel::PRESERVACION_CARRIL => 'nullable',
            FuncionamientoVehiculoModel::SENSORES_DEL_LAT => 'nullable',
            FuncionamientoVehiculoModel::BLIS => 'nullable',
            FuncionamientoVehiculoModel::ALERTA_TRAFICO_CRUZADO => 'nullable',
            FuncionamientoVehiculoModel::ASISTENCIA_ACTIVA_ESTACI => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_REVERSA => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_FRONTAL => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_360_GRADOS => 'nullable',
            FuncionamientoVehiculoModel::SITEMA_MON_PLL_TMS => 'nullable',
            IluminacionModel::ILUMINACION_AMBIENTAL => 'required',
            IluminacionModel::SISTEMA_ILUMINACION => 'nullable',
            IluminacionModel::FAROS_BIXENON => 'nullable',
            IluminacionModel::HID => 'nullable',
            IluminacionModel::LED => 'nullable',
            OtrasTecnologiasModel::EASY_FUEL => 'required',
            OtrasTecnologiasModel::TRACK_APPS => 'nullable',
            OtrasTecnologiasModel::AUTO_START_STOP => 'nullable',
            OtrasTecnologiasModel::SISTEMA_CTR_TERRENO => 'nullable',
            SeguridadModel::SISTEMA_FRENADO => 'required',
            SeguridadModel::CONTROL_TRACCION => 'nullable',
            SeguridadModel::ESC => 'nullable',
            SeguridadModel::RSC => 'nullable',
            SeguridadModel::CONTROL_TORQUE_CURVAS => 'nullable',
            SeguridadModel::CONTROL_CURVAS => 'nullable',
            SeguridadModel::CONTROL_BALANCE_REMOLQUE => 'nullable',
            SeguridadModel::FRENOS_ELECTRONICOS => 'nullable',
            SeguridadModel::CONTROL_ELECTRONICO_DES => 'nullable',
            SeguridadPasivaModel::CINTURON_SEGURIDAD => 'required',
            SeguridadPasivaModel::BOLSAS_AIRE => 'nullable',
            SeguridadPasivaModel::CHASIS_CARROCERIA => 'nullable',
            SeguridadPasivaModel::CARROSERIA_HIDROFORMA => 'nullable',
            SeguridadPasivaModel::CRISTALES => 'nullable',
            SeguridadPasivaModel::CABECERAS => 'nullable',
            SeguridadPasivaModel::SISTEMA_ALERTA => 'nullable',
            SeguridadPasivaModel::TECLADO_PUERTA => 'nullable',
            SeguridadPasivaModel::ALARMA_VOLUMETRICA => 'nullable',
            SeguridadPasivaModel::ALARMA_PERIMETRAL => 'nullable',
            SeguridadPasivaModel::MYKEY => 'nullable',
            VehiculoHibridoModel::VEHICULO_HIBRIDO => 'required',
            VehiculoHibridoModel::MOTOR_GASOLINA => 'nullable',
            VehiculoHibridoModel::TIPS_MANEJO => 'nullable',
            VehiculoHibridoModel::TRANSMISION_ECVT => 'nullable'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            SalidaUnidadesModel::NOMBRE_CLIENTE => 'required',
            SalidaUnidadesModel::ID_CATALOGO => 'nullable|numeric',
            SalidaUnidadesModel::NO_ECONOMICO => 'nullable',
            SalidaUnidadesModel::NO_SERIE => 'nullable',
            SalidaUnidadesModel::ID_MODELO => 'nullable|numeric',
            SalidaUnidadesModel::ID_COLOR => 'nullable|numeric',
            SalidaUnidadesModel::VEHICULO => 'nullable',
            SalidaUnidadesModel::NOMBRE_VENDEDOR => 'nullable',
            SalidaUnidadesModel::TIPO_VENTA => 'nullable',
            SalidaUnidadesModel::NO_ENCUESTA => 'nullable',
            SalidaUnidadesModel::NO_PEDIDO => 'nullable',
            SalidaUnidadesModel::FECHA_REGRESO => 'nullable',
            SalidaUnidadesModel::NOMBRE_ASESOR => 'nullable',
            SalidaUnidadesModel::URL_FIRMA_CLIENTE => 'nullable',
            SalidaUnidadesModel::URL_FIRMA_ASESOR => 'nullable',
            AhorroCombustibleModel::IVCT => 'nullable',
            AhorroCombustibleModel::TIVCT => 'nullable',
            AhorroCombustibleModel::SALIDA_UNIDAD_ID => 'required',
            ComentariosModel::EXPLICO_ASISTENCIA_FORD => 'required',
            ComentariosModel::EXPLICO_FORD_PROTECT => 'nullable',
            ComentariosModel::ACCESORIOS_PERSONALIZAR => 'nullable',
            ComentariosModel::PRUEBA_MANEJO => 'nullable',
            ComentariosModel::INFORMACION_MANTENIMIENTO => 'nullable',
            ComentariosModel::ASESOR_SERVICIO => 'nullable',
            ConfortModel::ACCIONAMIENTO_MANUAL => 'required',
            ConfortModel::ACCIONAMIENTO_ELECTRICO => 'nullable',
            ConfortModel::ASIENTO_DELANTERO_MASAJE => 'nullable',
            ConfortModel::ASIENTO_CALEFACCION => 'nullable',
            ConfortModel::EASY_POWER_FOLD => 'nullable',
            ConfortModel::MEMORIA_ASIENTOS => 'nullable',
            DesempenoModel::MOTORES => 'required',
            DesempenoModel::CLUTCH => 'nullable',
            DesempenoModel::CAJA_CAMBIOS => 'nullable',
            DesempenoModel::ARBOL_TRANSMISIONES => 'nullable',
            DesempenoModel::DIFERENCIAL => 'nullable',
            DesempenoModel::DIFERENCIAL_TRACCION => 'nullable',
            DesempenoModel::CAJA_REDUCTORA => 'nullable',
            DesempenoModel::CAJA_TRANSFERENCIA => 'nullable',
            DesempenoModel::SISTEMA_DINAMICO => 'nullable',
            DesempenoModel::DIRECCION => 'nullable',
            DesempenoModel::SUSPENSION => 'nullable',
            DesempenoModel::TOMA_PODER => 'nullable',
            DocumentosModel::CARTA_FACTURA => 'required',
            DocumentosModel::POLIZA_GARANTIA => 'nullable',
            DocumentosModel::POLIZA_SEGURO => 'nullable',
            DocumentosModel::TENENCIA => 'nullable',
            DocumentosModel::HERRAMIENTAS => 'nullable',
            EspecialesModel::PEPS => 'required',
            EspecialesModel::APERTIRA_CAJUELA => 'required',
            EspecialesModel::ENCENDIDO_REMOTO => 'required',
            EspecialesModel::TECHO_PANORAMICO => 'required',
            EspecialesModel::AIRE_ACONDICIONADO => 'required',
            EspecialesModel::EATC => 'required',
            EspecialesModel::CONSOLA_ENFRIAMIENTO => 'required',
            EspecialesModel::VOLANTE_AJUSTE_ALTURA => 'required',
            EspecialesModel::VOLANTE_CALEFACTADO => 'required',
            EspecialesModel::ESTRIBOS_ELECTRICOS => 'required',
            EspecialesModel::APERTURA_CAJA_CARGA => 'required',
            EspecialesModel::LIMPIAPARABRISAS => 'required',
            EspecialesModel::ESPEJO_ELECTRICO => 'required',
            EspecialesModel::ESPEJOS_ABATIBLES => 'required',
            ExtrasModel::CONECTIVIDAD => 'required',
            ExtrasModel::SYNC => 'nullable',
            ExtrasModel::MYFORD_TOUCH => 'nullable',
            ExtrasModel::MYFORD_TOUCH_NAVEGACION => 'nullable',
            ExtrasModel::SYNC_FORDPASS => 'nullable',
            ExtrasModel::APPLE_CARPLAY => 'nullable',
            ExtrasModel::ANDROID_AUTO => 'nullable',
            ExtrasModel::CONSOLA_SMARTPHONE => 'nullable',
            ExtrasModel::AUDIO_SHAKER_PRO => 'nullable',
            ExtrasModel::RADIO_HD => 'nullable',
            ExtrasModel::EQUIPO_SONY => 'nullable',
            ExtrasModel::PUERTOS_USB => 'nullable',
            ExtrasModel::BLUETOOH => 'nullable',
            ExtrasModel::WIFI => 'nullable',
            ExtrasModel::TARJETA_SD => 'nullable',
            ExtrasModel::INVERSOR_CORRIENTE => 'nullable',
            ExtrasModel::ESPEJO_ELECTROCROMATICO => 'nullable',
            FuncionamientoVehiculoModel::ASISTENTE_ARRANQUE => 'required',
            FuncionamientoVehiculoModel::PRESERVACION_CARRIL => 'nullable',
            FuncionamientoVehiculoModel::SENSORES_DEL_LAT => 'nullable',
            FuncionamientoVehiculoModel::BLIS => 'nullable',
            FuncionamientoVehiculoModel::ALERTA_TRAFICO_CRUZADO => 'nullable',
            FuncionamientoVehiculoModel::ASISTENCIA_ACTIVA_ESTACI => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_REVERSA => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_FRONTAL => 'nullable',
            FuncionamientoVehiculoModel::CAMARA_360_GRADOS => 'nullable',
            FuncionamientoVehiculoModel::SITEMA_MON_PLL_TMS => 'nullable',
            IluminacionModel::ILUMINACION_AMBIENTAL => 'required',
            IluminacionModel::SISTEMA_ILUMINACION => 'nullable',
            IluminacionModel::FAROS_BIXENON => 'nullable',
            IluminacionModel::HID => 'nullable',
            IluminacionModel::LED => 'nullable',
            OtrasTecnologiasModel::EASY_FUEL => 'required',
            OtrasTecnologiasModel::TRACK_APPS => 'nullable',
            OtrasTecnologiasModel::AUTO_START_STOP => 'nullable',
            OtrasTecnologiasModel::SISTEMA_CTR_TERRENO => 'nullable',
            SeguridadModel::SISTEMA_FRENADO => 'required',
            SeguridadModel::CONTROL_TRACCION => 'nullable',
            SeguridadModel::ESC => 'nullable',
            SeguridadModel::RSC => 'nullable',
            SeguridadModel::CONTROL_TORQUE_CURVAS => 'nullable',
            SeguridadModel::CONTROL_CURVAS => 'nullable',
            SeguridadModel::CONTROL_BALANCE_REMOLQUE => 'nullable',
            SeguridadModel::FRENOS_ELECTRONICOS => 'nullable',
            SeguridadModel::CONTROL_ELECTRONICO_DES => 'nullable',
            SeguridadPasivaModel::CINTURON_SEGURIDAD => 'required',
            SeguridadPasivaModel::BOLSAS_AIRE => 'nullable',
            SeguridadPasivaModel::CHASIS_CARROCERIA => 'nullable',
            SeguridadPasivaModel::CARROSERIA_HIDROFORMA => 'nullable',
            SeguridadPasivaModel::CRISTALES => 'nullable',
            SeguridadPasivaModel::CABECERAS => 'nullable',
            SeguridadPasivaModel::SISTEMA_ALERTA => 'nullable',
            SeguridadPasivaModel::TECLADO_PUERTA => 'nullable',
            SeguridadPasivaModel::ALARMA_VOLUMETRICA => 'nullable',
            SeguridadPasivaModel::ALARMA_PERIMETRAL => 'nullable',
            SeguridadPasivaModel::MYKEY => 'nullable',
            VehiculoHibridoModel::VEHICULO_HIBRIDO => 'required',
            VehiculoHibridoModel::MOTOR_GASOLINA => 'nullable',
            VehiculoHibridoModel::TIPS_MANEJO => 'nullable',
            VehiculoHibridoModel::TRANSMISION_ECVT => 'nullable'
        ];
    }

    public function guardar($parametros)
    {
        $salida_unidad = $this->modelo->create($parametros);
        $parametros[VehiculoHibridoModel::SALIDA_UNIDAD_ID] = $salida_unidad->id;
        $this->modeloAhorroCombustible->create($parametros);
        $this->modeloComentarios->create($parametros);
        $this->modeloConfort->create($parametros);
        $this->modeloDesempeno->create($parametros);
        $this->modeloDocumentos->create($parametros);
        $this->modeloEspeciales->create($parametros);
        $this->modeloExtras->create($parametros);
        $this->modeloFuncionamientoVehiculo->create($parametros);
        $this->modeloIluminacion->create($parametros);
        $this->modeloOtrasTecnologias->create($parametros);
        $this->modeloSeguridad->create($parametros);
        $this->modeloSeguridadPasiva->create($parametros);
        $this->modeloVehiculoHibrido->create($parametros);
    }

    public function getAll()
    {
        return $this->modelo->with([
            'ahorro_combustible',
            'comentarios',
            'confort',
            'desempeno',
            'documentos',
            'especiales',
            'extras',
            'funcionamiento',
            'iluminacion',
            'otras_tecnologias',
            'seguridad_pasiva',
            'seguridad',
            'vehiculo_hibrido'
        ])->get();
    }

    public function getOne($id)
    {
        return $this->modelo->with([
            'ahorro_combustible',
            'comentarios',
            'confort',
            'desempeno',
            'documentos',
            'especiales',
            'extras',
            'funcionamiento',
            'iluminacion',
            'otras_tecnologias',
            'seguridad_pasiva',
            'seguridad',
            'vehiculo_hibrido'
        ])->where(SalidaUnidadesModel::ID, $id)->get();
    }
}
