<?php

namespace App\Servicios\Autos;

use App\Models\Autos\CatModelosModel;
use App\Models\Autos\CatalogoAutosModel;
use App\Models\Autos\CategoriaCatAutosModel;
use App\Servicios\Core\ServicioDB;

class ServicioCategoriaAutos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catalogo autos';
        $this->modelo = new CategoriaCatAutosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CategoriaCatAutosModel::NOMBRE => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CategoriaCatAutosModel::NOMBRE => 'required',
        ];
    }

}
