<?php

namespace App\Servicios\Autos;

use App\Servicios\Core\ServicioDB;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioFolios;
use App\Models\Autos\EstatusVentaAutosModel;
use App\Models\Autos\VentasAutosModel;
use App\Models\Contabilidad\CatalogoCuentasModel;
use App\Models\CuentasPorCobrar\CuentasPorCobrarModel;
use App\Models\CuentasPorCobrar\EstatusCuentaModel;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;

class ServicioVentaAutos extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'venta auto';
        $this->modelo = new VentasAutosModel();
        $this->servicioFolios = new ServicioFolios();
        $this->servicioCuentasxCobrar = new ServicioCuentaPorCobrar();
    }

    public function getReglasGuardar()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'required|numeric',
            VentasAutosModel::ID_CLIENTE => 'required|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'required|numeric',
            VentasAutosModel::ID_TIPO_AUTO => 'required|numeric',
            VentasAutosModel::VENDEDOR_ID => 'required|numeric',
            CuentasPorCobrarModel::TOTAL => 'required',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'required|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'required|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'required|numeric',
            VentasAutosModel::MONEDA => 'required',
            CuentasPorCobrarModel::TASA_INTERES => 'required|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'required|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            VentasAutosModel::ID_UNIDAD => 'nullable|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric',
            VentasAutosModel::ID_FOLIO => 'nullable|numeric',
            VentasAutosModel::ID_CLIENTE => 'nullable|numeric',
            VentasAutosModel::VENDEDOR_ID => 'nullable|numeric',
            VentasAutosModel::ID_ESTATUS => 'nullable|numeric',
            CuentasPorCobrarModel::TOTAL => 'nullable',
            VentasAutosModel::ID_TIPO_AUTO => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => 'nullable|numeric',
            CuentasPorCobrarModel::TIPO_PAGO_ID => 'nullable|numeric',
            VentasAutosModel::MONEDA => 'nullable',
            CuentasPorCobrarModel::TASA_INTERES => 'nullable|numeric',
            CuentasPorCobrarModel::ENGANCHE => 'nullable|numeric',
            VentasAutosModel::SERVICIO_CITA_ID => 'nullable|numeric'
        ];
    }

    public function getReglasFiltroPrepedido()
    {
        return [
            VentasAutosModel::FECHA_INICIO => 'date',
            VentasAutosModel::FECHA_FIN => 'date',
        ];
    }

    public function store($parametros)
    {
        $folio = $this->servicioFolios->generarFolio(CatalogoCuentasModel::CAT_VENTA_AUTOS_NUEVOS);
        $parametros[VentasAutosModel::ID_FOLIO] = $folio->id;
        $importe = $parametros[CuentasPorCobrarModel::TOTAL];
        if ($parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID] == TipoFormaPagoModel::FORMA_CREDITO) {

            $tasa_interes = $parametros[CuentasPorCobrarModel::TASA_INTERES];
            $intereses = ($tasa_interes * ($importe / 100));
            $total_pagar = $importe + $intereses;
            $parametros[CuentasPorCobrarModel::TOTAL] = $total_pagar;
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = $intereses;
        } else {
            $parametros[CuentasPorCobrarModel::IMPORTE] = $importe;
            $parametros[CuentasPorCobrarModel::TOTAL] = $importe;
            $parametros[CuentasPorCobrarModel::INTERESES] = 0;
        }

        $this->store_cxc($parametros);
        return $this->modelo->create($parametros);
    }

    public function store_cxc($parametros)
    {

        return $this->servicioCuentasxCobrar->crear([
            CuentasPorCobrarModel::FOLIO_ID => $parametros[VentasAutosModel::ID_FOLIO],
            CuentasPorCobrarModel::CLIENTE_ID => $parametros[VentasAutosModel::ID_CLIENTE],
            CuentasPorCobrarModel::ESTATUS_CUENTA_ID => EstatusCuentaModel::ESTATUS_PROCESO,
            CuentasPorCobrarModel::CONCEPTO => "VENTA_AUTO_NUEVO",
            CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_FORMA_PAGO_ID],
            CuentasPorCobrarModel::TIPO_PAGO_ID => $parametros[CuentasPorCobrarModel::TIPO_PAGO_ID],
            CuentasPorCobrarModel::PLAZO_CREDITO_ID => $parametros[CuentasPorCobrarModel::PLAZO_CREDITO_ID],
            CuentasPorCobrarModel::TOTAL => $parametros[CuentasPorCobrarModel::TOTAL],
            CuentasPorCobrarModel::ENGANCHE => $parametros[CuentasPorCobrarModel::ENGANCHE],
            CuentasPorCobrarModel::TASA_INTERES => $parametros[CuentasPorCobrarModel::TASA_INTERES],
            CuentasPorCobrarModel::INTERESES => $parametros[CuentasPorCobrarModel::INTERESES],
            CuentasPorCobrarModel::IMPORTE => $parametros[CuentasPorCobrarModel::IMPORTE],
            CuentasPorCobrarModel::COMENTARIOS => "",
            CuentasPorCobrarModel::FECHA => date('Y-m-d')
        ]);
    }

    public function getPrepedidos($parametros)
    {
        $data = [];
        $data['id_estatus_venta'] = EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO;
        return $this->getVentaAutos(array_merge($data, $parametros));
    }

    public function getVentaAutos($parametros)
    {
        $tabla_venta = $this->modelo->getTable();

        $query = $this->modelo->select(
            'ventas_autos.id',
            'ventas_autos.moneda',
            'ventas_autos.servicio_cita_id',
            'unidades.id as id_unidad',
            'unidades.unidad_descripcion',
            'unidades.motor',
            'unidades.transmision',
            'unidades.combustible',
            'unidades.precio_costo',
            'unidades.precio_venta',
            'unidades.no_serie',
            'unidades.placas',
            'unidades.numero_cilindros',
            'unidades.id_estado',
            'catalogo_marcas.id as marca_id',
            'catalogo_marcas.nombre as nombre_marca',

            'catalogo_modelos.id as modelo_id',
            'catalogo_modelos.nombre as nombre_modelo',

            'catalogo_colores.id as color_id',
            'catalogo_colores.nombre as nombre_color',

            'catalogo_anio.id as id_anio',
            'catalogo_anio.nombre as nombre_anio',

            'clientes.id as cliente_id',
            'clientes.nombre_empresa as nombre_empresa',
            'clientes.nombre as nombre_cliente',
            'clientes.apellido_paterno as cliente_apellido_paterno',
            'clientes.apellido_materno as cliente_apellido_materno',
            'clientes.rfc as cliente_rfc',
            'clientes.telefono',
            'clientes.correo_electronico',
            'clientes.rfc as cliente_rfc',
            'cuentas_por_cobrar.id as cxc_id',
            'cuentas_por_cobrar.concepto',
            'cuentas_por_cobrar.tipo_forma_pago_id',
            'cuentas_por_cobrar.tipo_pago_id',
            'cuentas_por_cobrar.plazo_credito_id',
            'cuentas_por_cobrar.enganche',
            'cuentas_por_cobrar.total',
            'cuentas_por_cobrar.tasa_interes',
            'usuarios.id as id_vendedor',
            'usuarios.nombre as nombre_vendedor',
            'usuarios.apellido_paterno as apellido_paterno_vendedor',
            'usuarios.apellido_materno as apellido_materno_vendedor',
            'estatus_venta_autos.nombre as estatus_venta',
            'ventas_autos.created_at'

        )->from($tabla_venta);
        $query->join('unidades', 'unidades.id', '=', 'ventas_autos.id_unidad');
        $query->join('clientes', 'clientes.id', '=', 'ventas_autos.id_cliente');
        $query->join('cuentas_por_cobrar', 'cuentas_por_cobrar.folio_id', '=', 'ventas_autos.id_folio');
        $query->join('usuarios', 'usuarios.id', '=', 'ventas_autos.vendedor_id');
        $query->join('estatus_venta_autos', 'estatus_venta_autos.id', '=', 'ventas_autos.id_estatus');

        $query->join('catalogo_marcas', 'catalogo_marcas.id', '=', 'unidades.marca_id');
        $query->join('catalogo_anio', 'catalogo_anio.id', '=', 'unidades.anio_id');
        $query->join('catalogo_modelos', 'catalogo_modelos.id', '=', 'unidades.modelo_id');
        $query->join('catalogo_colores', 'catalogo_colores.id', '=', 'unidades.color_id');

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->join('equipo_opcional_unidad', 'equipo_opcional_unidad.venta_auto_id', '=', 'ventas_autos.id');
        }

        if (isset($parametros['servicio_cita_id']) && $parametros['servicio_cita_id']) {
            $query->whereNull('ventas_autos.servicio_cita_id');
        }

        if (isset($parametros['id_venta'])) {
            $query->where('ventas_autos.id', $parametros['id_venta']);
        }

        if (isset($parametros['id_estatus_venta'])) {
            $query->where('ventas_autos.id_estatus', $parametros['id_estatus_venta']);
        }

        if (isset($parametros['fecha_inicio']) && !isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
        }

        if (!isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['fecha_inicio']) && isset($parametros['fecha_fin'])) {
            $query->where('ventas_autos.created_at', '>=', $parametros['fecha_inicio']);
            $query->where('ventas_autos.created_at', '<=', $parametros['fecha_fin']);
        }

        if (isset($parametros['venta_auto']) && $parametros['venta_auto']) {
            $query->groupBy(
                'ventas_autos.id',
                'ventas_autos.moneda',
                'unidades.id',
                'unidades.unidad_descripcion',
                'unidades.motor',
                'unidades.transmision',
                'unidades.combustible',
                'unidades.precio_costo',
                'unidades.precio_venta',
                'unidades.numero_cilindros',
                'unidades.no_serie',
                'unidades.placas',
                'catalogo_marcas.nombre',
                'catalogo_modelos.nombre',
                'catalogo_colores.nombre',
                'catalogo_anio.nombre',
                'clientes.nombre_empresa',
                'clientes.nombre',
                'clientes.apellido_paterno',
                'clientes.apellido_materno',

                'clientes.rfc',
                'clientes.telefono',
                'clientes.correo_electronico',
                'cuentas_por_cobrar.concepto',
                'cuentas_por_cobrar.tipo_forma_pago_id',
                'cuentas_por_cobrar.tipo_pago_id',
                'cuentas_por_cobrar.plazo_credito_id',
                'cuentas_por_cobrar.enganche',
                'cuentas_por_cobrar.total',
                'cuentas_por_cobrar.tasa_interes',
                'usuarios.nombre',
                'usuarios.apellido_paterno',
                'usuarios.apellido_materno',
                'estatus_venta_autos.nombre',

                'ventas_autos.servicio_cita_id',
                'ventas_autos.created_at',

                'unidades.id_estado',
                'catalogo_marcas.id',
                'catalogo_modelos.id',
                'catalogo_colores.id',
                'catalogo_anio.id',
                'clientes.id',
                'cuentas_por_cobrar.id',
                'usuarios.id',
                'estatus_venta_autos.nombre'
            );
        }

        $query->limit(200);
        return $query->get();
    }

    public function getOne($id)
    {
        return $this->getVentaAutos(['id_venta' => $id]);
    }

    public function getPreventaById($id)
    {

        return $this->getVentaAutos([
            'id_venta' => $id,
            'id_estatus_venta' => EstatusVentaAutosModel::ESTATUS_PRE_PEDIDO
        ]);
    }
}
