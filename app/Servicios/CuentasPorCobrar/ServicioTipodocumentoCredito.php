<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Models\CuentasPorCobrar\TipoDocumentoCreditoModel;

class ServicioTipodocumentoCredito extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo documentos';
        $this->modelo = new TipoDocumentoCreditoModel();
    }

    public function getReglasGuardar()
    {
        return [
            TipoDocumentoCreditoModel::DESCRIPCION => 'required|string|max:50|unique:' . $this->modelo->getTable() . ',descripcion',

        ];
    }
    public function getReglasUpdate()
    {
        return [
            TipoDocumentoCreditoModel::DESCRIPCION => 'required|string|max:50|unique:' . $this->modelo->getTable() . ',descripcion',
        ];
    }
}
