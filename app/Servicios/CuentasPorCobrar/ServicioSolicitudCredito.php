<?php

namespace App\Servicios\CuentasPorCobrar;

use App\Servicios\Core\ServicioDB;
use App\Servicios\Core\ServicioManejoArchivos;
use App\Models\CuentasPorCobrar\DocumentosSolicitudModel;
use App\Models\CuentasPorCobrar\SolicitudCreditoModel;
use App\Models\Refacciones\ClientesModel;
use Illuminate\Support\Facades\Storage;

class ServicioSolicitudCredito extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'solicitud credito';
        $this->modelo = new SolicitudCreditoModel();
        $this->servicioManejoArchivos = new ServicioManejoArchivos();
        $this->servicioDocumentosSolicitud = new ServicioDocumentoSolicitud();
    }

    public function getAll()
    {
        $tabla_cliente = ClientesModel::getTableName();
        $tabla_solicitud = SolicitudCreditoModel::getTableName();
        return $this->modelo->select(
            $tabla_solicitud . ".id",
            $tabla_solicitud . ".referencia_nombre",
            $tabla_solicitud . ".referencia_nombre_2",
            $tabla_solicitud . ".referencia_telefono",
            $tabla_solicitud . ".referencia_telefono_2",
            $tabla_solicitud . ".cliente_id",
            $tabla_cliente . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
            $tabla_cliente . '.' . ClientesModel::DIRECCION . ' as direccion_cliente',
            $tabla_cliente . '.' . ClientesModel::ESTADO . ' as estado_cliente',
            $tabla_cliente . '.' . ClientesModel::MUNICIPIO . ' as municipio_cliente',
            $tabla_solicitud . ".buro_credito",
            $tabla_solicitud . ".created_at"
        )->with(['documentos'])
            ->join('clientes', 'cliente_id', '=', $tabla_cliente . '.' . ClientesModel::ID)
            ->get();
    }

    public function getOne($id_solicitud)
    {
        $tabla_cliente = ClientesModel::getTableName();
        $tabla_solicitud = SolicitudCreditoModel::getTableName();
        return $this->modelo->select(
            $tabla_solicitud . ".id",
            $tabla_solicitud . ".referencia_nombre",
            $tabla_solicitud . ".referencia_nombre_2",
            $tabla_solicitud . ".referencia_telefono",
            $tabla_solicitud . ".referencia_telefono_2",
            $tabla_solicitud . ".cliente_id",
            $tabla_cliente . '.' . ClientesModel::NOMBRE . ' as nombre_cliente',
            $tabla_cliente . '.' . ClientesModel::DIRECCION . ' as direccion_cliente',
            $tabla_cliente . '.' . ClientesModel::ESTADO . ' as estado_cliente',
            $tabla_cliente . '.' . ClientesModel::MUNICIPIO . ' as municipio_cliente',
            $tabla_solicitud . ".buro_credito",
            $tabla_solicitud . ".created_at"
        )->with(['documentos'])
            ->join('clientes', 'cliente_id', '=', $tabla_cliente . '.' . ClientesModel::ID)
            ->where($tabla_solicitud . ".id", $id_solicitud)
            ->get();
    }

    public function getReglasGuardar()
    {
        return [
            SolicitudCreditoModel::CLIENTE_ID => 'required|exists:clientes,id',
            SolicitudCreditoModel::BURO_CREDITO => 'required|boolean',
            SolicitudCreditoModel::REFERENCIA_NOMBRE => 'required',
            SolicitudCreditoModel::REFERENCIA_TELEFONO => 'required|unique:' . $this->modelo->getTable() . ',referencia_telefono',
            SolicitudCreditoModel::REFERENCIA_NOMBRE_2 => 'required',
            SolicitudCreditoModel::REFERENCIA_TELEFONOO_2 => 'required|unique:' . $this->modelo->getTable() . ',referencia_telefono_2',
            DocumentosSolicitudModel::IDENTIFICACION => ' required|mimes:pdf',
            DocumentosSolicitudModel::RECIBO_NOMINA => ' required|mimes:pdf',
            DocumentosSolicitudModel::COMPROBANTE_DOMICILIO => ' required|mimes:pdf',
            DocumentosSolicitudModel::SOLICITUD_CREDITO => ' required|mimes:pdf'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            SolicitudCreditoModel::CLIENTE_ID => 'required|exists:clientes,id',
            SolicitudCreditoModel::BURO_CREDITO => 'required|boolean',
            SolicitudCreditoModel::REFERENCIA_NOMBRE => 'required',
            SolicitudCreditoModel::REFERENCIA_TELEFONO => 'required|unique:' . $this->modelo->getTable() . ',referencia_telefono',
            SolicitudCreditoModel::REFERENCIA_NOMBRE_2 => 'required',
            SolicitudCreditoModel::REFERENCIA_TELEFONOO_2 => 'required|unique:' . $this->modelo->getTable() . ',referencia_telefono_2',
            DocumentosSolicitudModel::IDENTIFICACION => ' required|mimes:pdf',
            DocumentosSolicitudModel::RECIBO_NOMINA => ' required|mimes:pdf',
            DocumentosSolicitudModel::COMPROBANTE_DOMICILIO => ' required|mimes:pdf',
            DocumentosSolicitudModel::SOLICITUD_CREDITO => ' required|mimes:pdf'
        ];
    }

    public function guardarArchivos($parametros)
    {

        return $this->servicioDocumentosSolicitud->crear([
            DocumentosSolicitudModel::ID_SOLICITUD => $parametros[DocumentosSolicitudModel::ID_SOLICITUD],
            DocumentosSolicitudModel::RUTA_ARCHIVO => $parametros[DocumentosSolicitudModel::RUTA_ARCHIVO],
            DocumentosSolicitudModel::NOMBRE_ARCHIVO => $parametros[DocumentosSolicitudModel::NOMBRE_ARCHIVO],
            DocumentosSolicitudModel::ID_TIPO_DOCUMENTO => $parametros[DocumentosSolicitudModel::ID_TIPO_DOCUMENTO]
        ]);
    }

    public function store($parametros)
    {
        return $this->crear([
            SolicitudCreditoModel::CLIENTE_ID => $parametros[SolicitudCreditoModel::CLIENTE_ID],
            SolicitudCreditoModel::BURO_CREDITO => $parametros[SolicitudCreditoModel::BURO_CREDITO],
            SolicitudCreditoModel::REFERENCIA_NOMBRE => $parametros[SolicitudCreditoModel::REFERENCIA_NOMBRE],
            SolicitudCreditoModel::REFERENCIA_TELEFONO => $parametros[SolicitudCreditoModel::REFERENCIA_TELEFONO],
            SolicitudCreditoModel::REFERENCIA_NOMBRE_2 => $parametros[SolicitudCreditoModel::REFERENCIA_NOMBRE_2],
            SolicitudCreditoModel::REFERENCIA_TELEFONOO_2 => $parametros[SolicitudCreditoModel::REFERENCIA_TELEFONOO_2]
        ]);
    }


    public function setDirectory($folder_id)
    {
        $carpeta = self::DIRECTORIO_SOLICITUD_CREDITO . DIRECTORY_SEPARATOR . ServicioManejoArchivos::DIRECTORIO_UPLOADS . DIRECTORY_SEPARATOR;
        $directorio = $carpeta . $folder_id . DIRECTORY_SEPARATOR;

        if ($this->servicioManejoArchivos->fileExist($directorio)) {
            return $directorio;
        }

        Storage::makeDirectory($directorio);
        return $directorio;
    }

    public const DIRECTORIO_SOLICITUD_CREDITO = "solicitud_credito";
}
