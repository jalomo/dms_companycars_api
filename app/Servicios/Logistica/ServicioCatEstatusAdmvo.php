<?php

namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatEstatusAdmvoModel;

class ServicioCatEstatusAdmvo extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus administrativo';
        $this->modelo = new CatEstatusAdmvoModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusAdmvoModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusAdmvoModel::ESTATUS => 'required'
        ];
    }
}


