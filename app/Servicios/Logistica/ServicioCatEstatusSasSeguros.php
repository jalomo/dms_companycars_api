<?php

namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatEstatusSasSegurosModel;

class ServicioCatEstatusSasSeguros extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo estatus sas seguros';
        $this->modelo = new CatEstatusSasSegurosModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatEstatusSasSegurosModel::ESTATUS => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatEstatusSasSegurosModel::ESTATUS => 'required'
        ];
    }
}


