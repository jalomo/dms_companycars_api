<?php

namespace App\Servicios\Logistica;
use App\Servicios\Core\ServicioDB;
use App\Models\Logistica\CatAreaReparacionModel;

class ServicioCatAreaReparacion extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'catálogo área reparación';
        $this->modelo = new CatAreaReparacionModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatAreaReparacionModel::AREA_REPARACION => 'required'
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatAreaReparacionModel::AREA_REPARACION => 'required'
        ];
    }
}


