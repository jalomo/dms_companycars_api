<?php

namespace App\Servicios\Core;

use Illuminate\Support\Facades\Schema;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\MensajesConstantes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

abstract class ServicioDB
{
    protected $modelo;
    use MensajesConstantes;

    public function buscarTodos()
    {
        $tabla = $this->modelo->getTable();
        if (Schema::hasColumn($tabla, 'deleted_at'))
            $this->modelo->where('deleted_at','!=', true);

        //return $this->modelo->limit(200)->get();
        return $this->modelo->get();
    }

    public function getById(int $id)
    {
        return $this->modelo->find($id);
    }

    public function store(Request $request)
    {
        return $this->customStore($request, $this->getReglasGuardar());
    }

    public function customStore(Request $request, array $reglas)
    {
        ParametrosHttpValidador::validar($request, $reglas);
        return $this->crear($request->all());
    }

    public function getReglasGuardar()
    {
        return [];
    }

    public function update(Request $request, $id)
    {
        return $this->customUpdate($request, $id, $this->getReglasUpdate());
    }

    public function customUpdate(Request $request, int $id, array $reglas)
    {
        ParametrosHttpValidador::validar($request, $reglas);
        $modelo = $this->modelo->findOrFail($id);
        try {
            $modelo->fill($request->all());
            $modelo = $this->guardarModelo($modelo);
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
        }

        return $modelo;
    }

    public function massUpdateWhereId(String $column_id, Int $id, array $data)
    {
        $modelo = $this->modelo->findOrFail($id);
        try {
            $modelo->where($column_id, $id)->update($data);
            return $modelo;
        } catch (QueryException $e) {
            Log::warning($e->getMessage());
        }
    }

    public static function guardarModelo(Model $modelo)
    {
        $modelo->save();
        return $modelo;
    }

    public function getReglasUpdate()
    {
        return [];
    }

    public function getRecurso()
    {
        return $this->recurso;
    }

    public function crear(array $campos)
    {
        $campos[$this->getPrimaryKey()] = $this->getNextId();
        return $this->modelo->create($campos);
    }

    public function createOrUpdate(array $search_parameter_array, array $arrayData)
    {
        return $this->modelo->firstOrCreate($search_parameter_array, $arrayData);
    }

    public function eliminar(int $id)
    {
        $this->modelo->destroy($id);
    }

    public function getWhere($column_id, $value)
    {
        return $this->modelo->where($column_id, $value)->get();
    }

    public function getPrimaryKey()
    {
        return $this->modelo->getKeyName();
    }

    public function getNextId()
    {
        return $this->modelo->max($this->getPrimaryKey()) + 1;
    }

    public function buscarUltimo()
    {
        $tabla = $this->modelo->getTable();
        if (Schema::hasColumn($tabla, 'activo'))
            $this->modelo->where('activo', true);
            $this->modelo->orderBy($this->getPrimaryKey(),'desc');

        return $this->modelo->limit(1)->get();
    }
}
