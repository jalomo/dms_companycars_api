<?php

namespace App\Servicios\Polizas;

use App\Servicios\Core\ServicioDB;
use App\Models\Polizas\CatTipoPolizasModel;

class ServicioCatTipoPolizas extends ServicioDB
{
    public function __construct()
    {
        $this->recurso = 'tipo de polizas';
        $this->modelo = new CatTipoPolizasModel();
    }

    public function getReglasGuardar()
    {
        return [
            CatTipoPolizasModel::NOMBRE => 'required',
        ];
    }
    public function getReglasUpdate()
    {
        return [
            CatTipoPolizasModel::NOMBRE => 'nullable',
        ];
    }
}
