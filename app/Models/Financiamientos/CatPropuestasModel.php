<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatPropuestasModel extends Modelo
{
    protected $table = 'cat_propuestas';
    const ID = 'id';
    const PROPUESTA = 'propuesta';

    protected $fillable = [
        self::ID,
        self::PROPUESTA
    ];
}
