<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatCompaniaSeguroModel extends Modelo
{
    protected $table = 'cat_compania_seguros';
    const ID = 'id';
    const COMPANIA = 'compania';
    const VALOR = 'valor';

    protected $fillable = [
        self::ID,
        self::COMPANIA,
        self::VALOR
    ];
}
