<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatComisionModel extends Modelo
{
    protected $table = 'cat_comisiones_financiamientos';
    const ID = 'id';
    const COMISION = 'comision';
    const VALOR = 'valor';

    protected $fillable = [
        self::ID,
        self::COMISION,
        self::VALOR
    ];
}
