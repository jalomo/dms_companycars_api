<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;

class FinanciamientosModel extends Modelo
{
    protected $table = 'financiamientos';
    const ID = 'id';
    const ID_ASESOR_VENTAS = 'id_asesor_ventas';
    const ID_CLIENTE = 'id_cliente';
    const FECHA_BURO = 'fecha_buro';
    const SE_INGRESA = 'se_ingresa';
    const ID_PROPUESTA = 'id_propuesta';
    const FECHA_INGRESO = 'fecha_ingreso';
    const HAY_UNIDAD = 'hay_unidad';
    const ID_FINANCIERA1 = 'id_financiera1';
    const ID_FINANCIERA2 = 'id_financiera2';
    const ID_UNIDAD = 'id_unidad';
    const ID_CATALOGO = 'id_catalogo';
    const PRECIO_UNIDAD = 'precio_unidad';
    const ENGANCHE = 'enganche';
    const MF = 'mf';
    const PLAN = 'plan';
    const COMISION_FINANCIERA = 'comision_financiera';
    const COMISION_ASESOR = 'comision_asesor';
    const ID_ESTATUS = 'id_estatus';
    const ID_PERFIL = 'id_perfil';
    const SCORE = 'score';
    const CUENTA_MAS_ALTA = 'cuenta_mas_alta';
    const TIEMPO_CUENTA = 'tiempo_cuenta';
    const MENSUALIDAD = 'mensualidad';
    const EGRESO_BRUTO = 'egreso_bruto';
    const TOTAL = 'total';
    const TOTAL2 = 'total2';
    const INGRESO_INDEP = 'ingreso_indep';
    const INGRESO_NOMINA = 'ingreso_nomina';
    const NIVEL_ENDEUDAMIENTO = 'nivel_endeudamiento';
    const ARRAIGO_CASA = 'arraigo_casa';
    const ARRAIGO_EMPLEO = 'arraigo_empleo';
    const COMENTARIO = 'comentario';
    const MONTO_FINANCIAR = 'monto_financiar';
    const BONO = 'bono';
    const PLAZO = 'plazo';
    const ID_COMISION = 'id_comision';
    const ID_TIPO_SEGURO = 'id_tipo_seguro';
    const ID_COMPANIA = 'id_compania';
    const ID_UDI = 'id_udi';
    const COMISION_AGENCIA = 'comision_agencia';
    const NO_POLIZA = 'no_poliza';
    const INICIO_VIGENCIA = 'inicio_vigencia';
    const VENCIMIENTO_ANUAL = 'vencimiento_anual';
    const PRIMA_TOTAL = 'prima_total';
    const IVA = 'iva';
    const EMISION_POLIZA = 'emision_poliza';
    const PRIMA_NETA = 'prima_neta';
    const UDI_AGENCIA = 'udi_agencia';
    const COMISION_ASESOR_PRIMA = 'comision_asesor_prima';

    //MEDICIÓN TIEMPO
    const FECHA_CONDICIONAMIENTO = 'fecha_condicionamiento';
    const HORA_CONDICIONAMIENTO = 'hora_condicionamiento';
    const FECHA_CUMPLIMIENTO = 'fecha_cumplimiento';
    const HORA_CUMPLIMIENTO = 'hora_cumplimiento';
    const FECHA_APROBACION = 'fecha_aprobacion';
    const HORA_APROBACION = 'hora_aprobacion';
    const FECHA_CONTRATO = 'fecha_contrato';
    const HORA_CONTRATO = 'hora_contrato';
    const ID_ESTATUS_PISO = 'id_estatus_piso';
    const FECHA_COMPRA = 'fecha_compra';
    const HORA_COMPRA = 'hora_compra';
    const COMENTARIOS_COMPRA = 'comentarios_compra';


    const ID_TIPO_SEGURO_FC = 'id_tipo_seguro_fc';
    const ID_COMPANIA_FC = 'id_compania_fc';
    const NO_POLIZA_FC = 'no_poliza_fc';
    const INICIO_VIGENCIA_FC = 'inicio_vigencia_fc';
    const VENCIMIENTO_ANUAL_FC = 'vencimiento_anual_fc';
    const PRIMA_TOTAL_FC = 'prima_total_fc';
    const IVA_FC = 'iva_fc';
    const EMISION_POLIZA_FC = 'emision_poliza_fc';
    const PRIMA_NETA_FC = 'prima_neta_fc';
    const UDI_AGENCIA_FC = 'udi_agencia_fc';
    const COMISION_ASESOR_PRIMA_FC = 'comision_asesor_prima_fc';
    const ID_UDI_FC = 'id_udi_fc';
    //Bitacora compras
    const FECHA_ENVIO = 'fecha_envio';
    const ANIO = 'anio';
    const SERIE = 'serie';
    const ID_USO_SEGURO = 'id_uso_seguro';
    const ACCESORIOS = 'accesorios';
    const EXTENSION_GARANTIA = 'extension_garantia';
    const OBSERVACIONES = 'observaciones';
    protected $fillable = [
        self::ID,
        self::ID_ASESOR_VENTAS,
        self::ID_CLIENTE,
        self::FECHA_BURO,
        self::SE_INGRESA,
        self::ID_PROPUESTA,
        self::FECHA_INGRESO,
        self::HAY_UNIDAD,
        self::ID_FINANCIERA1,
        self::ID_FINANCIERA2,
        self::ID_UNIDAD,
        self::ID_CATALOGO,
        self::PRECIO_UNIDAD,
        self::ENGANCHE,
        self::MF,
        self::PLAN,
        self::COMISION_FINANCIERA,
        self::COMISION_ASESOR,
        self::ID_ESTATUS,
        self::ID_PERFIL,
        self::SCORE,
        self::CUENTA_MAS_ALTA,
        self::TIEMPO_CUENTA,
        self::MENSUALIDAD,
        self::EGRESO_BRUTO,
        self::TOTAL,
        self::TOTAL2,
        self::INGRESO_INDEP,
        self::INGRESO_NOMINA,
        self::NIVEL_ENDEUDAMIENTO,
        self::ARRAIGO_CASA,
        self::ARRAIGO_EMPLEO,
        self::COMENTARIO,
        self::MONTO_FINANCIAR,
        self::ID_COMISION,
        self::ID_TIPO_SEGURO,
        self::ID_COMPANIA,
        self::ID_UDI,
        self::COMISION_AGENCIA,
        self::NO_POLIZA,
        self::INICIO_VIGENCIA,
        self::VENCIMIENTO_ANUAL,
        self::PRIMA_TOTAL,
        self::IVA,
        self::EMISION_POLIZA,
        self::PRIMA_NETA,
        self::UDI_AGENCIA,
        self::COMISION_ASESOR_PRIMA,
        self::ID_TIPO_SEGURO_FC,
        self::ID_COMPANIA_FC,
        self::NO_POLIZA_FC,
        self::INICIO_VIGENCIA_FC,
        self::VENCIMIENTO_ANUAL_FC,
        self::PRIMA_TOTAL_FC,
        self::IVA_FC,
        self::EMISION_POLIZA_FC,
        self::PRIMA_NETA_FC,
        self::UDI_AGENCIA_FC,
        self::COMISION_ASESOR_PRIMA_FC,
        self::ID_UDI_FC,
        self::BONO,
        self::PLAZO,
        self::FECHA_CONDICIONAMIENTO,
        self::HORA_CONDICIONAMIENTO,
        self::FECHA_CUMPLIMIENTO,
        self::HORA_CUMPLIMIENTO,
        self::FECHA_APROBACION,
        self::HORA_APROBACION,
        self::FECHA_CONTRATO,
        self::HORA_CONTRATO,
        self::ID_ESTATUS_PISO,
        self::FECHA_COMPRA,
        self::HORA_COMPRA,
        self::COMENTARIOS_COMPRA,
        self::FECHA_ENVIO,
        self::ANIO,
        self::SERIE,
        self::ID_USO_SEGURO,
        self::ACCESORIOS,
        self::EXTENSION_GARANTIA,
        self::OBSERVACIONES
    ];
}
