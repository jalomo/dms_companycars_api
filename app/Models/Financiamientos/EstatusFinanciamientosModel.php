<?php

namespace App\Models\Financiamientos;

use App\Models\Core\Modelo;

class EstatusFinanciamientosModel extends Modelo
{
    protected $table = 'catalogo_estatus_financiamientos';
    const ID = 'id';
    const ESTATUS = 'estatus';

    protected $fillable = [
        self::ID,
        self::ESTATUS
    ];
}
