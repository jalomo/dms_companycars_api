<?php

namespace App\Models\Facturas;

use App\Models\Core\Modelo;
use App\Models\Refacciones\ProductosModel;

class Factura extends Modelo
{
    protected $table = 'factura';
    const ID = "id";
    const VALIDADO_SAT = "validado_sat";
    const RUTA_PDF_FACTURA = "ruta_pdf_factura";
    const RUTA_XML_FACTURA = "ruta_xml_factura";
    const ARCHIVO_FACTURA = 'archivo_factura';
    const FILE_NAME = 'file_name';

    const LUGAR_EXPEDICION = 'lugar_expedicion';
    const FORMA_PAGO = 'forma_pago';
    const METODO_PAGO = 'metodo_pago';
    const FOLIO = 'folio';
    const SERIE = 'serie';
    const TIPO_COMPROBANTE = 'tipo_comprobante';
    const TIPO_CAMBIO = 'tipo_cambio';
    const MONEDA = 'moneda';
    const TOTAL = 'total';
    const SUB_TOTAL = 'subtotal';
    const FECHA = 'fecha';
    const NO_CERTIFICADO = 'no_certificado';
    const CERTIFICADO = 'certificado';
    const SELLO = 'sello';
    const UUID  = 'uuid';

    // relaciones
    const REL_PRODUCTOS = 'producto';
    const REL_EMISOR = 'emisor';
    const REL_RECEPTOR = 'receptor';
    const REL_CONCEPTOS = 'conceptos';
    const REL_TIMBRE_FISCAL = 'timbre_fiscal';

    const REL_ORDEN_COMPRA = 'orden_compra';

    protected $fillable = [
        self::RUTA_PDF_FACTURA,
        self::RUTA_XML_FACTURA,
        self::LUGAR_EXPEDICION,
        self::FORMA_PAGO,
        self::METODO_PAGO,
        self::FOLIO,
        self::SERIE,
        self::TIPO_COMPROBANTE,
        self::TIPO_CAMBIO,
        self::MONEDA,
        self::TOTAL,
        self::SUB_TOTAL,
        self::FECHA,
        self::SELLO,
        self::NO_CERTIFICADO,
        self::CERTIFICADO,
        self::FILE_NAME,
        self::UUID
    ];

    public function producto()
    {
        return $this->hasMany(ProductosModel::class);
    }

    public function receptor()
    {
        return $this->hasOne(ReceptorFactura::class);
    }

    public function emisor()
    {
        return $this->hasOne(EmisorFactura::class);
    }

    public function conceptos()
    {
        return $this->hasMany(ConceptosFactura::class);
    }

    public function timbre_fiscal()
    {
        return $this->hasOne(TimbreFiscal::class);
    }

    public function orden_compra()
    {
        return $this->hasOne(ReFacturaOrdenCompraModel::class);
    }
}
