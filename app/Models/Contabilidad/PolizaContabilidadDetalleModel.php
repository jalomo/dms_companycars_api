<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class PolizaContabilidadDetalleModel extends Modelo
{
    protected $table = 'polizas_contabilidad_detalle';
    const ID = "id";
    const CONCEPTO = "concepto";
    const COSTO = "costo";
    const ID_CUENTA = "id_cuenta";
    const ID_SUBCUENTA = "id_subcuenta";
    const ID_POLIZA = "id_poliza";



    protected $fillable = [
        self::CONCEPTO,
        self::COSTO,
        self::ID_CUENTA,
        self::ID_SUBCUENTA,
        self::ID_POLIZA,
    ];
}
