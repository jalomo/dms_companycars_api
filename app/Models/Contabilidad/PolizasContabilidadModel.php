<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class PolizasContabilidadModel extends Modelo
{
    protected $table = 'polizas_contabilidad';
    const ID = "id";
    //const NO_CUENTA = "no_cuenta";
    const POLIZA = "poliza";
    const CONCEPTO = "concepto";
    const FORMULO = "formulo";
    const CUENTA = "cuenta";
    const SUBCUENTA = "subcuenta";


    protected $fillable = [
        //self::NO_CUENTA,
        self::POLIZA,
        self::CONCEPTO,
        self::FORMULO,
        self::CUENTA,
        self::SUBCUENTA,
    ];
}
