<?php

namespace App\Models\Contabilidad;

use App\Models\Core\Modelo;

class CatalogoCuentasModel extends Modelo
{
    protected $table = 'catalogo_cuentas';
    const ID = "id";
    const NO_CUENTA = "no_cuenta";
    const NOMBRE_CUENTA = "nombre_cuenta";
    const CVE_AUXILIAR = "cve_auxiliar";
    const TIPO_CUENTA = "tipo_cuenta";
    const CODIGO = "codigo";
    const NATURALEZA = "naturaleza";

    const CAT_VENTAS = 1;
    const CAT_TRASPASO = 2;
    const CAT_COMPRAS = 3;
    const CAT_DEVOLUCION_PROVEEDOR = 4;
    const CAT_DEVOLUCION_VENTA = 5;
    const CAT_VENTA_AUTOS_NUEVOS = 6;
    const CAT_VENTA_AUTOS_SEMINUEVOS = 7;
    const ORDEN_SERVICIO = 8;
    
    protected $fillable = [
        self::NO_CUENTA,
        self::NOMBRE_CUENTA,
        self::CVE_AUXILIAR,
        self::TIPO_CUENTA,
        self::CODIGO,
        self::NATURALEZA,
    ];
}
