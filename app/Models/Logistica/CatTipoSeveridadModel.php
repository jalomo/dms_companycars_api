<?php

namespace App\Models\Logistica;

use App\Models\Core\Modelo;

class CatTipoSeveridadModel extends Modelo
{
    protected $table = 'cat_tipo_severidad';
    const ID = 'id';
    const TIPO_SEVERIDAD = 'tipo_severidad';

    protected $fillable = [
        self::TIPO_SEVERIDAD
    ];
}
