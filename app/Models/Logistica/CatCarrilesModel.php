<?php

namespace App\Models\Logistica;
use App\Models\Core\Modelo;
class CatCarrilesModel extends Modelo
{
    protected $table = 'cat_carriles';
    const ID = 'id';
    const CARRIL = 'carril';
    protected $fillable = [
        self::CARRIL
    ];
}
