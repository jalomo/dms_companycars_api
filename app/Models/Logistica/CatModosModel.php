<?php

namespace App\Models\logistica;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatModosModel extends Modelo
{
    protected $table = 'cat_modos_logistica';
    const ID = 'id';
    const MODO = 'modo';

    protected $fillable = [
        self::MODO
    ];
}
