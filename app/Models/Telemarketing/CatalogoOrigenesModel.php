<?php

namespace App\Models\Telemarketing;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class CatalogoOrigenesModel extends Modelo
{
    protected $table = 'cat_origenes';
    const ID = "id";
    const ORIGEN = "origen";
    protected $fillable = [
        self::ID,
        self::ORIGEN
    ];
}
