<?php

namespace App\Models\Telemarketing;

use App\Models\Core\Modelo;

class VentasWebModel extends Modelo
{
    protected $table = 'ventas_web';
    const ID = "id";
    const ID_CLIENTE = "id_cliente";
    const ID_UNIDAD = "id_unidad";
    const ID_USUARIO_CREO = "id_usuario_creo";
    const ID_ASESOR_WEB_ASIGNADO = "id_asesor_web_asignado";
    const ID_USUARIO_TELEMARKETING = "id_usuario_telemarketing";
    const FECHA_CITA_PROGRAMADA = "fecha_cita_programada";
    const INTENTOS = "intentos";
    const NO_CONTACTAR = "no_contactar";
    const ID_ORIGEN = "id_origen";
    const VENDIDO = "vendido";
    const COMENTARIO_VENTA = "comentario_venta";
    const FECHA_COMENTARIO = "fecha_comentario";
    const INFORMACION_ERRONEA = "informacion_erronea";
    protected $fillable = [
        self::ID_CLIENTE,
        self::ID_UNIDAD,
        self::ID_USUARIO_CREO,
        self::ID_ASESOR_WEB_ASIGNADO,
        self::ID_USUARIO_TELEMARKETING,
        self::FECHA_CITA_PROGRAMADA,
        self::INTENTOS,
        self::NO_CONTACTAR,
        self::ID_ORIGEN,
        self::VENDIDO,
        self::COMENTARIO_VENTA,
        self::FECHA_COMENTARIO,
        self::INFORMACION_ERRONEA
    ];
}
