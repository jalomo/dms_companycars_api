<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class CatModelosModel extends Modelo
{
    protected $table = 'catalogo_modelos';
    const ID = "id";
    const NOMBRE = "nombre";
    const TIEMPO_LAVADO = "tiempo_lavado";

    protected $fillable = [
        self::NOMBRE,
        self::TIEMPO_LAVADO
    ];
}
