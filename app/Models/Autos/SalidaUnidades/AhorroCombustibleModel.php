<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class AhorroCombustibleModel extends Modelo
{
    protected $table = 'ahorro_combustible';
    const ID = "id";
    const IVCT = "ivct";
    const TIVCT = "tivct";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::IVCT,
        self::TIVCT,
        self::SALIDA_UNIDAD_ID
    ];

}
