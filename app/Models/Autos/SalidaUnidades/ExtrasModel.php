<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class ExtrasModel extends Modelo
{
    protected $table = 'extras';
    const ID = "id";
    const CONECTIVIDAD = "conectividad";
    const SYNC = "sync";
    const MYFORD_TOUCH = "myFord_touch";
    const MYFORD_TOUCH_NAVEGACION = "myFord_touch_navigation";
    const SYNC_FORDPASS = "sync_fordpass";
    const APPLE_CARPLAY = "apple_carplay";
    const ANDROID_AUTO = "android_auto";
    const CONSOLA_SMARTPHONE = "consola_smartphone";
    const AUDIO_SHAKER_PRO = "audio_shaker_pro";
    const RADIO_HD = "radio_hd";
    const EQUIPO_SONY = "equipo_sony";
    const PUERTOS_USB = "puertos_usb";
    const BLUETOOH = "bluetooh";
    const WIFI = "wifi";
    const TARJETA_SD = "tarjeta_sd";
    const INVERSOR_CORRIENTE = "inversor_corriente";
    const ESPEJO_ELECTROCROMATICO = "espejo_electrocromatico";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::CONECTIVIDAD,
        self::SYNC,
        self::MYFORD_TOUCH,
        self::MYFORD_TOUCH_NAVEGACION,
        self::SYNC_FORDPASS,
        self::APPLE_CARPLAY,
        self::ANDROID_AUTO,
        self::CONSOLA_SMARTPHONE,
        self::AUDIO_SHAKER_PRO,
        self::RADIO_HD,
        self::EQUIPO_SONY,
        self::PUERTOS_USB,
        self::BLUETOOH,
        self::WIFI,
        self::TARJETA_SD,
        self::INVERSOR_CORRIENTE,
        self::ESPEJO_ELECTROCROMATICO,
        self::SALIDA_UNIDAD_ID
    ];
}
