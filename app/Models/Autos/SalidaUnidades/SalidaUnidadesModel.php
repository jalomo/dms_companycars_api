<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class SalidaUnidadesModel extends Modelo
{
    protected $table = 'salida_unidades';
    const ID = "id";
    const NOMBRE_CLIENTE = "nombre_cliente";
    const ID_CATALOGO = "id_catalogo";
    const NO_ECONOMICO = "no_economico";
    const NO_SERIE = "no_serie";
    const ID_MODELO = "modelo";
    const ID_COLOR = "color";
    const VEHICULO = "vehiculo";
    const NOMBRE_VENDEDOR = "nombre_vendedor";
    const TIPO_VENTA = "tipo_venta";
    const NO_ENCUESTA = "no_encuesta";
    const NO_PEDIDO = "no_pedido";
    const FECHA_REGRESO = "fecha_regreso";

    const NOMBRE_ASESOR = "nombre_asesor";
    const URL_FIRMA_CLIENTE = "url_firma_cliene";
    const URL_FIRMA_ASESOR = "url_firma_asesor";

    protected $fillable = [
        self::NOMBRE_CLIENTE,
        self::ID_CATALOGO,
        self::NO_ECONOMICO,
        self::NO_SERIE,
        self::ID_MODELO,
        self::ID_COLOR,
        self::VEHICULO,
        self::NOMBRE_VENDEDOR,
        self::TIPO_VENTA,
        self::NO_ENCUESTA,
        self::NO_PEDIDO,
        self::FECHA_REGRESO,
        self::NOMBRE_ASESOR,
        self::URL_FIRMA_CLIENTE,
        self::URL_FIRMA_ASESOR
    ];

    public function ahorro_combustible()
    {
        return $this->hasOne(AhorroCombustibleModel::class, AhorroCombustibleModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function comentarios()
    {
        return $this->hasOne(ComentariosModel::class, ComentariosModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function confort()
    {
        return $this->hasOne(ConfortModel::class, ConfortModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function desempeno()
    {
        return $this->hasOne(DesempenoModel::class, DesempenoModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function documentos()
    {
        return $this->hasOne(DocumentosModel::class, DocumentosModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function especiales()
    {
        return $this->hasOne(EspecialesModel::class, EspecialesModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function extras()
    {
        return $this->hasOne(ExtrasModel::class, ExtrasModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function funcionamiento()
    {
        return $this->hasOne(FuncionamientoVehiculoModel::class, FuncionamientoVehiculoModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function iluminacion()
    {
        return $this->hasOne(IluminacionModel::class, IluminacionModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function otras_tecnologias()
    {
        return $this->hasOne(OtrasTecnologiasModel::class, OtrasTecnologiasModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function seguridad_pasiva()
    {
        return $this->hasOne(SeguridadPasivaModel::class, SeguridadPasivaModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function seguridad()
    {
        return $this->hasOne(SeguridadModel::class, SeguridadModel::SALIDA_UNIDAD_ID, self::ID);
    }

    public function vehiculo_hibrido()
    {
        return $this->hasOne(VehiculoHibridoModel::class, VehiculoHibridoModel::SALIDA_UNIDAD_ID, self::ID);
    }
}
