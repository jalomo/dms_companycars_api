<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class VehiculoHibridoModel extends Modelo
{
    protected $table = 'vehiculo_hibrido';
    const ID = "id";
    const VEHICULO_HIBRIDO = "vehiculo_hibrido";
    const MOTOR_GASOLINA = "motor_gasolina";
    const TIPS_MANEJO = "tips_manejo";
    const TRANSMISION_ECVT = "transmision_ecvt";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::VEHICULO_HIBRIDO,
        self::MOTOR_GASOLINA,
        self::TIPS_MANEJO,
        self::TRANSMISION_ECVT,
        self::SALIDA_UNIDAD_ID

    ];
}
