<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class ComentariosModel extends Modelo
{
    protected $table = 'comentario';
    const ID = "id";
    const EXPLICO_ASISTENCIA_FORD = "explico_asistencia_ford";
    const EXPLICO_FORD_PROTECT = "explico_ford_protect";
    const ACCESORIOS_PERSONALIZAR = "accesorios_manejo";
    const PRUEBA_MANEJO = "prueba_manejo";
    const INFORMACION_MANTENIMIENTO = "informacion_mantenimiento";
    const ASESOR_SERVICIO = "asesor_servicio";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::EXPLICO_ASISTENCIA_FORD,
        self::EXPLICO_FORD_PROTECT,
        self::ACCESORIOS_PERSONALIZAR,
        self::PRUEBA_MANEJO,
        self::INFORMACION_MANTENIMIENTO,
        self::ASESOR_SERVICIO,
        self::SALIDA_UNIDAD_ID
    ];
}
