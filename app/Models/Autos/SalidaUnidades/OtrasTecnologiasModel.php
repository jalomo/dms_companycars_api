<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class OtrasTecnologiasModel extends Modelo
{
    protected $table = 'otras_tecnologias';
    const ID = "id";
    const EASY_FUEL = "easy_fuel";
    const TRACK_APPS = "track_apps";
    const AUTO_START_STOP = "auto_start_stop";
    const SISTEMA_CTR_TERRENO = "sistema_ctr_terreno";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::EASY_FUEL,
        self::TRACK_APPS,
        self::AUTO_START_STOP,
        self::SISTEMA_CTR_TERRENO,
        self::SALIDA_UNIDAD_ID
    ];
}
