<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class DocumentosModel extends Modelo
{
    protected $table = 'documentos';
    const ID = "id";
    const CARTA_FACTURA = "carta_factura";
    const POLIZA_GARANTIA = "poliza_garantia";
    const POLIZA_SEGURO = "poliza_seguro";
    const TENENCIA = "tenencia";
    const HERRAMIENTAS = "herramientas";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';

    protected $fillable = [
        self::CARTA_FACTURA,
        self::POLIZA_GARANTIA,
        self::POLIZA_SEGURO,
        self::TENENCIA,
        self::HERRAMIENTAS,
        self::SALIDA_UNIDAD_ID
    ];
}
