<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class IluminacionModel extends Modelo
{
    protected $table = 'iluminacion';
    const ID = "id";
    const ILUMINACION_AMBIENTAL = "iluminacion_ambiental";
    const SISTEMA_ILUMINACION = "sistema_iluminacion";
    const FAROS_BIXENON = "faros_bixenon";
    const HID = "hid";
    const LED = "led";
    const SALIDA_UNIDAD_ID = "salida_unidad_id";

    protected $fillable = [
        self::ILUMINACION_AMBIENTAL,
        self::SISTEMA_ILUMINACION,
        self::FAROS_BIXENON,
        self::HID,
        self::LED,
        self::SALIDA_UNIDAD_ID
    ];
}
