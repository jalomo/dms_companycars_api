<?php

namespace App\Models\Autos\SalidaUnidades;

use App\Models\Core\Modelo;

class ConfortModel extends Modelo
{
    protected $table = 'confort';
    const ID = "id";
    const ACCIONAMIENTO_MANUAL = "accionamiento_manual";
    const ACCIONAMIENTO_ELECTRICO = "accionamiento_electrico";
    const ASIENTO_DELANTERO_MASAJE = "asiento_delantero_asaje";
    const ASIENTO_CALEFACCION = "asiento_calefaccion";
    const EASY_POWER_FOLD = "easy_power_fold";
    const SALIDA_UNIDAD_ID = 'salida_unidad_id';
    const MEMORIA_ASIENTOS = "memoria_asientos";

    protected $fillable = [
        self::ACCIONAMIENTO_MANUAL,
        self::ACCIONAMIENTO_ELECTRICO,
        self::ASIENTO_DELANTERO_MASAJE,
        self::ASIENTO_CALEFACCION,
        self::EASY_POWER_FOLD,
        self::MEMORIA_ASIENTOS,
        self::SALIDA_UNIDAD_ID
    ];
}
