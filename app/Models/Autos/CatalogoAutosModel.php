<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class CatalogoAutosModel extends Modelo
{
    protected $table = 'catalogo_autos';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    const TIEMPO_LAVADO = "tiempo_lavado";
    const CATEGORIA_ID = "categoria_id";
    
    protected $fillable = [
        self::NOMBRE,
        self::CLAVE,
        self::TIEMPO_LAVADO,
        self::CATEGORIA_ID
    ];
}
