<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;

class CategoriaCatAutosModel extends Modelo
{
    protected $table = 'categoria_autos';
    const ID = "id";
    const NOMBRE = "nombre";

    protected $fillable = [
        self::NOMBRE
    ];
}
