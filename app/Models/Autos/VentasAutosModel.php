<?php

namespace App\Models\Autos;

use App\Models\Core\Modelo;
use App\Models\Refacciones\FoliosModel;
use App\Models\Refacciones\ClientesModel;

class VentasAutosModel extends Modelo
{
    protected $table = 'ventas_autos';
    const ID = "id";
    const ID_UNIDAD = "id_unidad";
    const ID_FOLIO = "id_folio";
    const ID_CLIENTE = "id_cliente";
    const ID_ESTATUS = 'id_estatus';
    const ID_TIPO_AUTO = 'id_tipo_auto'; //nuevo - semi nuevo
    const MONEDA = "moneda";
    const ID_PLAN_CREDITO = "id_plan_credito";
    const VENDEDOR_ID = "vendedor_id";
    const SERVICIO_CITA_ID = "servicio_cita_id";

    const FECHA_INICIO = "fecha_fin";
    const FECHA_FIN = "fecha_inicio";
    protected $fillable = [
        self::ID_UNIDAD,
        self::ID_FOLIO,
        self::VENDEDOR_ID,
        self::ID_CLIENTE,
        self::ID_ESTATUS,
        self::ID_TIPO_AUTO,
        self::MONEDA,
        self::SERVICIO_CITA_ID
    ];

    public function folio()
    {
        return $this->belongsTo(FoliosModel::class, FoliosModel::ID, self::ID_FOLIO);
    }

    public function unidad()
    {
        return $this->belongsTo(UnidadesModel::class, UnidadesModel::ID, self::ID_UNIDAD);
    }

    public function cliente()
    {
        return $this->belongsTo(ClientesModel::class, ClientesModel::ID, self::ID_CLIENTE);
    }
}
