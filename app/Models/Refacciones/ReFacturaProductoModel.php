<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ReFacturaProductoModel extends Modelo
{
    protected $table = 're_factura_producto';
    const ID = "id";
    const FACTURA_ID = "factura_id";
    const PRODUCTO_ID = "producto_id";
    const ESTATUS_FACTURA_ID = "estatus_factura_id";
    const ACTIVO = "activo";
    const USER_ID = "user_id";
    
    protected $fillable = [
        self::FACTURA_ID,
        self::PRODUCTO_ID,
        self::ESTATUS_FACTURA_ID,
        self::ACTIVO,
        self::USER_ID,
    ];
}
