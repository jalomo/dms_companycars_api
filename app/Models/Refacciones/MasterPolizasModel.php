<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class MasterPolizasModel extends Modelo
{
    protected $table = 'precio';
    const ID = "id";
    const FOLIO = "folio_id";
    const ESTATUS = "estatus";
    const DESCRIPCION = "descripcion";
    const MONTO ='monto';

    const DEFAULT_PRECIO = 1;

    protected $fillable = [
        self::FOLIO,
    ];
}
