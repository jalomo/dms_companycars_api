<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoClaveClienteModel extends Modelo
{
    protected $table = 'catalogo_clave_cliente';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    
    const CV = 1;
    const CS = 2;
    const CC = 3;
    const CI = 4;
    
    protected $fillable = [
        self::NOMBRE,
        self::CLAVE
    ];
}