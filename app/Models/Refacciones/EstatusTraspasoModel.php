<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class EstatusTraspasoModel extends Modelo
{
    protected $table = 'estatus_traspaso';
    const ID = "id";
    const NOMBRE = "nombre";

    const ESTATUS_REALIZADO = 1;
    const ESTATUS_CANCELADO = 2;

    protected $fillable = [
        self::NOMBRE
    ];
}
