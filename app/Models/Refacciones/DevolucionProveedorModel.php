<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class DevolucionProveedorModel extends Modelo
{
    protected $table = 'devolucion_proveedor';
    const ID = "id";
    const ORDEN_COMPRA_ID = "orden_compra_id";
    const FACTURA = "factura";
    const OBSERVACIONES = "observaciones";
    const FECHA = "fecha";
    const FOLIO_ID = "folio_id";
    
    protected $fillable = [
        self::ORDEN_COMPRA_ID,
        self::FACTURA,
        self::OBSERVACIONES,
        self::FECHA,
        self::FOLIO_ID
    ];
}
