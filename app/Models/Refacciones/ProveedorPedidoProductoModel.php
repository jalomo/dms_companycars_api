<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class ProveedorPedidoProductoModel extends Modelo
{
    protected $table = 'proveedor_pedido_producto';
    const ID = "id";
    const PROVEEDOR_ID = "proveedor_id";
    const CANTIDAD_PROVEEDOR = "cantidad_proveedor";
    const PRODUCTO_PEDIDO_ID = "producto_pedido_id";


    protected $fillable = [
        self::PROVEEDOR_ID,
        self::CANTIDAD_PROVEEDOR,
        self::PRODUCTO_PEDIDO_ID
    ];
}
