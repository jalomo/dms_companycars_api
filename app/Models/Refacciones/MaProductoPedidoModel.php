<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use App\Models\Usuarios\User;

class MaProductoPedidoModel extends Modelo
{
    protected $table = 'ma_producto_pedido';
    const ID = "id";
    const AUTORIZADO = "autorizado";
    const FINALIZADO = "finalizado";
    const ID_USUARIO_AUTORIZO = "id_usuario_autorizo";
    
    protected $fillable = [
        self::AUTORIZADO,
        self::FINALIZADO,
        self::ID_USUARIO_AUTORIZO
    ];

    public function rel_usuario()
    {
        return $this->hasOne(User::class, User::ID, self::ID_USUARIO_AUTORIZO);
    }
}
