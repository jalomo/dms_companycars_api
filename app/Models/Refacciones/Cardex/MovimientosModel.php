<?php

namespace App\Models\Refacciones\Cardex;

use App\Models\Core\Modelo;

class MovimientosModel extends Modelo
{
    protected $table = 'cat_movimientos';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";
    
    const TRASPASO = 1;
    const VENTA_MOSTRADOR = 2;
    const VENTA_TALLER = 3;
    const DEVOLUCION_PROVEEDOR = 4;
    const COMPRAS = 5;
    const DEV_VENTAS = 6;

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE
    ];
}
