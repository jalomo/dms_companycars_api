<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class StockProductosModel extends Modelo
{
    protected $table = 'stock_productos';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const CANTIDAD_ACTUAL = "cantidad_actual";
    const CANTIDAD_ALMACEN_PRIMARIO = "cantidad_almacen_primario";
    const CANTIDAD_ALMACEN_SECUNDARIO = "cantidad_almacen_secundario";
    const CANTIDAD_COMPRAS = "cantidad_compras";
    const CANTIDAD_VENTAS = "cantidad_ventas";
    const TOTAL_PRECIO_COMPRAS = "total_precio_compras";
    const TOTAL_PRECIO_VENTAS = "total_precio_ventas";

    protected $fillable = [
        self::PRODUCTO_ID,
        self::CANTIDAD_ACTUAL,
        self::CANTIDAD_ALMACEN_PRIMARIO,
        self::CANTIDAD_ALMACEN_SECUNDARIO,
        self::CANTIDAD_COMPRAS,
        self::CANTIDAD_VENTAS,
        self::TOTAL_PRECIO_COMPRAS,
        self::TOTAL_PRECIO_VENTAS
    ];
}
