<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class ProductosPedidosModel extends Modelo
{
    protected $table = 'producto_pedidos';
    const ID = "id";
    const PRODUCTO_ID = "producto_id";
    const ESTATUS_ID = "estatus_id";
    const CANTIDAD_SOLICITADA = "cantidad_solicitada";
    const MA_PEDIDO_ID = "ma_pedido_id";
    const CANTIDAD_BACKORDER = "cantidad_backorder";
    const CANTIDAD_CARGADA = "cantidad_cargada";
    const PROVEEDOR_ID = "proveedor_id";


    const ESTATUS_PROCESO = 1;
    const ESTATUS_FINALIZADO = 2;
    const ESTATUS_NO_PEDIDAS = 3;
    const ESTATUS_BACKORDER = 4;

    protected $fillable = [
        self::PRODUCTO_ID,
        self::ESTATUS_ID,
        self::CANTIDAD_SOLICITADA,
        self::MA_PEDIDO_ID,
        self::CANTIDAD_BACKORDER,
        self::CANTIDAD_CARGADA,
        self::PROVEEDOR_ID
    ];
}
