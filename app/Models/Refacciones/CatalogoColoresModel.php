<?php

namespace App\Models\Refacciones;

use App\Models\Core\Modelo;

class CatalogoColoresModel extends Modelo
{
    protected $table = 'catalogo_colores';
    const ID = "id";
    const NOMBRE = "nombre";
    const CLAVE = "clave";

    protected $fillable = [
        self::NOMBRE,
        self::CLAVE
    ];
}
