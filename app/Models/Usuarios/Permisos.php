<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;

class Permisos extends Modelo
{
    protected $table = 'permisos';
    const ID = "id";
    const PERMISO = 'permiso';

    protected $fillable = [
        self::PERMISO,
    ];
}
