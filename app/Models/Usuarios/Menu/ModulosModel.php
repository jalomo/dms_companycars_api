<?php

namespace App\Models\Usuarios\Menu;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class ModulosModel extends Modelo
{
    protected $table = 'modulo';
    const ID = "id";
    const NOMBRE = 'nombre';
    const ORDEN = 'orden';
    const ICONO = 'icono';
    const DEFAULT = 'default';
    const DASHBOARD_ID = 1;
    
    protected $fillable = [
        self::NOMBRE,
        self::ORDEN,
        self::ICONO,
        self::DEFAULT
    ];
}
