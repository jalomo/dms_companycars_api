<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use App\Models\Usuarios\Menu\ModulosModel;
use Illuminate\Database\Eloquent\Model;

class RolesModulosModel extends Modelo
{
    protected $table = 'det_roles_modulos';
    const ID = "id";
    const ROL_ID = "rol_id";
    const MODULO_ID = "modulo_id";
    
    protected $fillable = [
        self::MODULO_ID,
        self::ROL_ID
    ];

    public function roles()
    {
        return $this->hasMany(RolModel::class, RolModel::ID, self::ROL_ID);
    }

    public function modulos()
    {
        return $this->hasMany(ModulosModel::class, ModulosModel::ID, self::MODULO_ID);
    }
}
