<?php

namespace App\Models\Usuarios;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'usuarios';
    const ID = "id";
    const NOMBRE = "nombre";
    const EMAIL = "email";
    const PASSWORD = "password";
    const ROL_ID = "rol_id";
    const ESTATUS_ID = "estatus_id";
    const REMEMBER_TOKEN = 'remember_token';
    const RFC = 'rfc';
    const ACTIVO = 'activo';
    const ULTIMO_ACCESO = 'ultimo_acceso';
    const USUARIO = 'usuario';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const API_TOKEN = 'api_token';
    const IP_ADRESS = 'ip_adress';
    const SESION_ACTIVA = 'session_activa';
    const TELEFONO = 'telefono';
    const APELLIDO_MATERNO = 'apellido_materno';
    const APELLIDO_PATERNO = 'apellido_paterno';

    protected $fillable = [
        self::NOMBRE,
        self::APELLIDO_MATERNO,
        self::APELLIDO_PATERNO,
        self::EMAIL,
        self::PASSWORD,
        self::TELEFONO,
        self::ROL_ID,
        self::ESTATUS_ID,
        self::USUARIO,
        self::ULTIMO_ACCESO,
        self::RFC,
        self::ACTIVO,
        self::API_TOKEN,
        self::IP_ADRESS,
        self::SESION_ACTIVA
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD,
        self::REMEMBER_TOKEN,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    const  ROL_ADMIN = 1;
    const  ROL_USER = 2;
    const  ROL_GERENCIA = 3;
    const  ROL_ADMINISTRATIVOS = 4;
    const  ROL_COBRANZA = 5;
}
