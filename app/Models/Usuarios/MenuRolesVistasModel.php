<?php

namespace App\Models\Usuarios;

use App\Models\Core\Modelo;
use Illuminate\Database\Eloquent\Model;

class MenuRolesVistasModel extends Modelo
{
    protected $table = 'menu_rol_vista';
    const ID = "id";
    const ROL_ID = "rol_id";
    const SECCION_ID = "seccion_id";
    const SUBMENU_ID = "submenu_id";
    const VISTA_ID = "vista_id";
    
    protected $fillable = [
        self::ID,
        self::ROL_ID,
        self::SECCION_ID,
        self::SUBMENU_ID,
        self::VISTA_ID
    ];
}
