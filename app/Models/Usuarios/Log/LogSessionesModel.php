<?php

namespace App\Models\Usuarios\Log;

use App\Models\Core\Modelo;

class LogSessionesModel extends Modelo
{
    protected $table = 'log_sesiones';
    const ID = "id";
    const USUARIO_ID = 'usuario_id';
    const DIRECCION_IP = 'direccion_ip';
    const DESCRIPCION = "descripcion";

    protected $fillable = [
        self::USUARIO_ID,
        self::DIRECCION_IP,
        self::DESCRIPCION
    ];
}
