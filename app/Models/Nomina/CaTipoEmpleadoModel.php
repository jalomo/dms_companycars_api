<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTipoEmpleadoModel extends ModeloNominas
{
    protected $table = 'ca_TipoEmpleado';
     
    
    const ID = "id_TipoEmpleado";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}