<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaGeneroModel extends ModeloNominas
{
    protected $table = 'ca_Genero';
     
    
    const ID = "id_Genero";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}