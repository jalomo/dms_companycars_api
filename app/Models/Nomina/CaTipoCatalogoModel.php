<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTipoCatalogoModel extends ModeloNominas
{
    protected $table = 'ca_TipoCatalogo';
     
    
    const ID = "id_TipoCatalogo";
    const Descripcion = "Descripcion";
    const Campo_1 = "Campo_1";
    const Campo_2 = "Campo_2";
    const Campo_3 = "Campo_3";
   
    protected $fillable = [
        self::Descripcion,
        self::Campo_1,
        self::Campo_2,
        self::Campo_3
    ];
}