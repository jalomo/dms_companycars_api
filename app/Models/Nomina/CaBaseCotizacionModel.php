<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaBaseCotizacionModel extends ModeloNominas
{
    protected $table = 'ca_BaseCotizacion';
     
    
    const ID = "id_BaseCotizacion";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}