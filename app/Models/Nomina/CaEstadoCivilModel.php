<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaEstadoCivilModel extends ModeloNominas
{
    protected $table = 'ca_EstadoCivil';
     
    
    const ID = "id_EstadoCivil";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
    const id_Genero = "id_Genero";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion,
        self::id_Genero
    ];
}