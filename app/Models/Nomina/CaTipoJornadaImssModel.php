<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTipoJornadaImssModel extends ModeloNominas
{
    protected $table = 'ca_TipoJornadaImss';
     
    
    const ID = "id_TipoJornadaImss";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Descripcion
    ];
}