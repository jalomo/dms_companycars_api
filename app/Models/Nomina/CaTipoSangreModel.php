<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaTipoSangreModel extends ModeloNominas
{
    protected $table = 'ca_TipoSangre';
     
    
    const ID = "id_TipoSangre";
    const Tipo = "Tipo";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Tipo,
        self::Descripcion
    ];
}