<?php

namespace App\Models\Nomina;

use App\Models\Core\ModeloNominas;
use Illuminate\Database\Eloquent\Model;

class CaClasificacionesModel extends ModeloNominas
{
    protected $table = 'ca_Clasificaciones';
     
    
    const ID = "id_Clasificacion";
    const Clave = "Clave";
    const Descripcion = "Descripcion";
   
    protected $fillable = [
        self::Clave,
        self::Descripcion
    ];
}