<?php

namespace App\Http\Controllers\Financiamientos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Financiamientos\ServicioFinanciamientos;
use Illuminate\Http\Request;

class FinanciamientosController extends CrudController
{
    public function __construct(){
        $this->servicio = new ServicioFinanciamientos();
    }
    public function getAll(Request $request){
        return Respuesta::json($this->servicio->buscarFinanciamientosPorParametros($request->all()), 200);
    }
    public function updateBitacoraCompras(Request $request, $id)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->reglasUpdateBitacoraCompras());
            $modelo = $this->servicio->updateBitacoraCompras($request->all(), $id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
