<?php

namespace App\Http\Controllers\Sistemas;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorCobrar\ServicioCuentaPorCobrar;
use App\Servicios\Refacciones\ServicioCurl;
use App\Servicios\Usuarios\ServicioUsuarios;
use Illuminate\Http\Request;

class NotificacionesController extends CrudController
{
	public function __construct()
	{
		$this->servicio = new ServicioCuentaPorCobrar();
		$this->servicioCurl = new ServicioCurl();
		$this->servicioUsuarios = new ServicioUsuarios();
	}

	public function getNotificaciones(Request $request)
	{
		try {
			$telefono = $request->get('telefono');
			$usuarios = $this->servicioUsuarios->buscarTodos();
			$dataNotificacion = $this->servicioCurl->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono, false, false);
			$notificaciones = json_decode($dataNotificacion)->data;
			$listado = [];
			foreach ($notificaciones as $notify) {
				$notify->usuario = null;
				foreach ($usuarios as $user) {
					if ($notify->celular2 == $user->telefono) {
						$notify->usuario = ($user->nombre . ' ' . $user->apellido_paterno . ' ' . $user->apellido_materno);
					}
				}
				array_push($listado, $notify);
			}
			return Respuesta::json($listado, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getChat(Request $request)
	{
		try {
			$telefono1 = $request->get('telefono1');
			$telefono2 = $request->get('telefono2');
			ParametrosHttpValidador::validar($request, $this->getReglasChat());

			$usuarios = $this->servicioUsuarios->buscarTodos();
			$dataNotificacion = $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/notificaciones_busqueda', [
				'telefono' => $telefono1,
				'celular2' => $telefono2
			], false);
			$notificaciones = json_decode($dataNotificacion)->data;

			// $dataNotificacionRemitente = $this->servicioCurl->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono1, false, false);
			// $dataNotificacionDestinatario = $this->servicioCurl->curlGet('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/lista_notificaciones/' . $telefono2, false, false);
			// $notificaciones_remitente = json_decode($dataNotificacionRemitente)->data;
			// $notificaciones_destinatario = json_decode($dataNotificacionDestinatario)->data;
			// $notificaciones = array_merge($notificaciones_remitente, $notificaciones_destinatario);
			$listado = [];
			foreach ($notificaciones as $notify) {
				if ($notify->eliminado == 0) {
					if ($notify->celular2 == $telefono2 || $notify->celular2 == $telefono1) {
						$notify->tipo = null;
						$notify->remitente = null;
						$notify->destinatario = null;
						foreach ($usuarios as $user) {
							if ($notify->celular2 == $user->telefono) {
								$notify->destinatario = ($user->nombre . ' ' . $user->apellido_paterno . ' ' . $user->apellido_materno);
							}
							if ($notify->celular == $user->telefono) {
								$notify->remitente = ($user->nombre . ' ' . $user->apellido_paterno . ' ' . $user->apellido_materno);
							}
						}
						array_push($listado, $notify);
					}
				}
			}
			$convArrayKardex = json_decode(json_encode($listado), true);

			usort($convArrayKardex, function ($a, $b) {
				return strcmp($b["fecha_creacion"], $a["fecha_creacion"]);
			});

			return Respuesta::json($convArrayKardex, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function setNotificacion(Request $request)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->getReglasNotificacion());

			$dataNotificacion = $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
				'celular' => $request->get('telefono'),
				'mensaje' => $this->eliminar_tildes($request->get('mensaje')),
				'sucursal' => 'M2137',
				'celular2' => $request->get('celular2'),
				'mensaje_respuesta' => $request->get('mensaje_respuesta')
			], false);

			return Respuesta::json($dataNotificacion, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function getReglasChat()
	{
		return [
			'telefono1' => 'required|numeric|digits:10',
			'telefono2' =>  'required|numeric|digits:10'
		];
	}

	public function getReglasNotificacion()
	{
		return [
			'telefono' => 'required|numeric|digits:10',
			'mensaje' => 'required|string|max:140',
			'celular2' => 'nullable|numeric|digits:10',
			'mensaje_respuesta' => 'nullable|string',
		];
	}


	private function eliminar_tildes($cadena)
	{

		$cadena = ($cadena);

		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);

		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena
		);

		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena
		);

		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena
		);

		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena
		);

		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);

		return $cadena;
	}
}
