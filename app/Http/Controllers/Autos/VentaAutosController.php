<?php

namespace App\Http\Controllers\Autos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioVentaAutos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class VentaAutosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentaAutos();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPrepedidos(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasFiltroPrepedido());
            return Respuesta::json($this->servicio->getPrepedidos($request->all()), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getPrepedidosById($id)
    {
        $modelo = $this->servicio->getPreventaById($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }


    public function index()
    {
        return Respuesta::json($this->servicio->getVentaAutos(), 200);
    }
    
    public function show($id)
    {
        $modelo = $this->servicio->getOne($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
}
