<?php

namespace App\Http\Controllers\Autos;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Autos\ServicioCatalogoAutos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;

class CatalogoAutosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCatalogoAutos();
    }

    public function buscar_id(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusquedaAuto());
            $modelo = $this->servicio->searchIdAuto($request);
            return Respuesta::json($modelo, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function buscar_elementos($id_catalogo)
    {
        $modelo = $this->servicio->searchCatalogos($id_catalogo);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
}
