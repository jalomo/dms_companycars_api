<?php

namespace App\Http\Controllers\Autos\SalidaUnidades;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\SalidaUnidades\ServicioSalidaUnidades;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class SalidaUnidadesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSalidaUnidades();
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->guardar($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function index()
    {
        return Respuesta::json($this->servicio->getAll(), 200);
    }

    public function show($id)
    {
        $modelo = $this->servicio->getOne($id);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }
}
