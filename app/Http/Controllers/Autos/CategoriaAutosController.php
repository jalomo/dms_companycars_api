<?php

namespace App\Http\Controllers\Autos;

use Illuminate\Http\Request;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Autos\ServicioCategoriaAutos;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;

class CategoriaAutosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCategoriaAutos();
    }

}
