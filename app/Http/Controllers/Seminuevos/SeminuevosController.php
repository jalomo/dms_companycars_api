<?php

namespace App\Http\Controllers\Seminuevos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioSeminuevos;
use Illuminate\Http\Request;

class SeminuevosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSeminuevos();
    }
}
