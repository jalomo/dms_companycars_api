<?php

namespace App\Http\Controllers\Seminuevos;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioDocumentacion;
use Illuminate\Http\Request;

class DocumentacionController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDocumentacion();
    }
}
