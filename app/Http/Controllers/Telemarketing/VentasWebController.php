<?php

namespace App\Http\Controllers\Telemarketing;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Telemarketing\ServicioVentasWeb;
use App\Models\Telemarketing\VentasWebModel;
use Illuminate\Http\Request;

class VentasWebController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentasWeb();
    }
    public function store(Request $request)
    {
        try {
            if (!empty($request->get(VentasWebModel::ID_CLIENTE))) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            } else {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarClienteNoExiste());
            }

            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function updateClienteVenta(Request $request, $id)
    {
        try {
            if (!empty($request->get(VentasWebModel::ID_CLIENTE))) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
            } else {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarClienteNoExiste());
            }
            $modelo = $this->servicio->updateClienteVenta($request->all(), $id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getVentas(Request $request)
    {
        return Respuesta::json($this->servicio->buscarVentasPorParametros($request->all()), 200);
    }

    public function updateInfoErronea(Request $request)
    {
        try {
            $modelo = $this->servicio->massUpdateWhereId(VentasWebModel::ID, $request->get('id_venta_web'), ['informacion_erronea' => $request->get('informacion_erronea')]);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function updateComentariosVenta(Request $request, $id)
    {
        try {
            $modelo = $this->servicio->massUpdateWhereId('id', $id, ['vendido' => $request->get('vendido'), 'comentario_venta' => $request->get('comentario_venta'), 'fecha_comentario' => $request->get('fecha_comentario')]);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
    public function siguienteAsesor(Request $request)
    {
        return Respuesta::json($this->servicio->getSiguienteAsesor($request->all()), 200);
    }
}
