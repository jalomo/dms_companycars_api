<?php

namespace App\Http\Controllers\Telemarketing;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Telemarketing\ServicioHistorialComentariosVentas;
use App\Servicios\Telemarketing\ServicioVentasWeb;
use App\Models\Telemarketing\VentasWebModel;
use Illuminate\Http\Request;

class HistorialComentariosVentasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioHistorialComentariosVentas();
        $this->servicioVentasWeb = new ServicioVentasWeb();
    }
    public function getComentariosVentas(Request $request)
    {
        return Respuesta::json($this->servicio->buscarComentariosPorParametros($request->all()), 200);
    }
    
    public function saveComentarioAndUpdate(Request $request)
    {
        try {
            //guarda el historial comentario venta 
            $this->servicio->store($request);
            //actualizar
            $modelo = $this->servicioVentasWeb->massUpdateWhereId(VentasWebModel::ID, $request->get('id_venta_web'), ['informacion_erronea' => 1]);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
