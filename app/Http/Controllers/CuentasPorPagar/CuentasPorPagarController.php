<?php

namespace App\Http\Controllers\CuentasPorPagar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\CuentasPorPagar\ServicioCuentaPorPagar;
use App\Servicios\CuentasPorPagar\ServicioAbonoPorPagar;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Refacciones\ServicioCurl;
use App\Models\CuentasPorCobrar\TipoFormaPagoModel;
use App\Models\CuentasPorPagar\CuentasPorPagarModel;
use App\Models\Usuarios\User as UsuarioModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class CuentasPorPagarController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioCuentaPorPagar();
        $this->servicioCurl = new ServicioCurl();
        $this->servicio_abonos = new ServicioAbonoPorPagar();
        $this->modelo_usuarios = new UsuarioModel();
    }

    public function index()
    {
        return Respuesta::json($this->servicio->getAll(), 200);
    }

    public function show($id)
    {
        $modelo = $this->servicio->getByWhere(['id' => $id]);
        return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
    }

    public function listado(Request $request)
    {
        try {
            return Respuesta::json(array('data' => $this->servicio->getAll($request->all())), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getKardexPagos(Request $request)
    {
        try {
            return Respuesta::json(array('data' => $this->servicio->getKardexPagos($request->all())), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function showByFolioId($folio)
    {
        try{
            
            $modelo = $this->servicio->getByWhere(['folio_id' => $folio]);
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }

    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarTipoContado());
            $importe = $request->get(CuentasPorPagarModel::IMPORTE);
            if ($request->get('tipo_forma_pago_id') == TipoFormaPagoModel::FORMA_CREDITO) {
                ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardarTipoCredito());
                $tasa_interes = $request->get(CuentasPorPagarModel::TASA_INTERES);
                $intereses = ($tasa_interes * ($importe / 100));
                $total_pagar = $importe + $intereses;
                $request->merge([CuentasPorPagarModel::IMPORTE => $importe]);
                $request->merge([CuentasPorPagarModel::TOTAL => $total_pagar]);
                $request->merge([CuentasPorPagarModel::INTERESES => $intereses]);

            } else {
                $request->merge([CuentasPorPagarModel::IMPORTE => $importe]);
                $request->merge([CuentasPorPagarModel::TOTAL => $importe]);
                $request->merge([CuentasPorPagarModel::INTERESES => 0]);
            }
            $data = $this->servicio->crear($request->all());
            $abonos = $this->servicio_abonos->crearAbonosByCuentaId($data->id);
            if (!$abonos){
                DB::rollback();
            }
            DB::commit();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 201, $mensaje);
        } catch (\Throwable $e) {
            DB::rollback();
            return Respuesta::error($e);
        }
    }

    public function getVerificaCuentasMorosas(Request $request)
	{
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
		$cuentas_por_pagar = $this->servicio->getCuentasEnProcesoPago();
		$data = [];
		if ($cuentas_por_pagar && !empty($cuentas_por_pagar)) {
			foreach ($cuentas_por_pagar as $cuenta) {
				$params = [
					'cuenta_por_pagar_id' => $cuenta->id,
					'fecha_actual' => $fecha_actual
				];
				$verificando = $this->servicio_abonos->getVerificaAbonosPendientes($params);
				if (!empty($verificando)) {
					$data[] = $verificando;
				}
			}
		}
		return Respuesta::json($data, 200);
	}

	public function envioAvisoPago(Request $request)
	{
		
		$cuentas_por_pagar = $this->servicio->getCuentasEnProcesoPago();
		$fecha_actual = $request->get('fecha_actual') ? $request->get('fecha_actual') : date('Y-m-d');
        $administrativos = $this->modelo_usuarios
        ->select(UsuarioModel::TELEFONO)
        ->where(UsuarioModel::ROL_ID, UsuarioModel::ROL_ADMINISTRATIVOS)->get();
		$data = [];
		if ($cuentas_por_pagar && count($cuentas_por_pagar) >= 1) {
			foreach ($cuentas_por_pagar as $cuenta) {
				$params = [
					'cuenta_por_pagar_id' => $cuenta->id,
					'fecha_actual' => $fecha_actual
				];
                $abonosAviso = $this->servicio_abonos->getVerificaDiasParaPago($params);
				if (!empty($abonosAviso)) {
					$this->enviarNotificacion($abonosAviso, $administrativos);
					$data[] = $abonosAviso;
				}
			}
		}
		return Respuesta::json($data, 200);
	}
    
    public function enviarNotificacion($data, $administrativos) {
      
        if (!empty($administrativos)) {
            foreach ($administrativos as $administrativo) {
                $mensaje = 'Te recordamos que la factura con folio '. $data->folio .' del proveedor '. $data->proveedor_nombre . ' vence el dia '. date('d/m/y', strtotime($data->fecha_vencimiento));
                $this->servicioCurl->curlPost('https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion', [
                    'celular' => $administrativo->telefono,
                    'mensaje' => $this->eliminar_tildes($mensaje),
                    'sucursal' => 'M2137'
                ], false);
            }
        }
    }
    
    private function eliminar_tildes($cadena){

		$cadena = ($cadena);
	
		//Ahora reemplazamos las letras
		$cadena = str_replace(
			array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
			array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
			$cadena
		);
	
		$cadena = str_replace(
			array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
			array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
			$cadena );
	
		$cadena = str_replace(
			array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
			array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
			$cadena );
	
		$cadena = str_replace(
			array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
			array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
			$cadena );
	
		$cadena = str_replace(
			array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
			array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
			$cadena );
	
		$cadena = str_replace(
			array('ñ', 'Ñ', 'ç', 'Ç'),
			array('n', 'N', 'c', 'C'),
			$cadena
		);
	
		return $cadena;
	}
}
