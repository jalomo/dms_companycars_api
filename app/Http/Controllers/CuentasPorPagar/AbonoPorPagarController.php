<?php

namespace App\Http\Controllers\CuentasPorPagar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\CuentasPorPagar\ServicioAbonoPorPagar;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class AbonoPorPagarController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioAbonoPorPagar();
    }

    public function showByIdOrdenEntrada(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json($modelo, empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
       
    }

    public function listadoAbonosByOrdenEntrada(Request $request)
    {
        try {
            $modelo = $this->servicio->getAbonosPorPagar($request->toArray());
            return Respuesta::json(['data' => $modelo], empty($modelo) ? 404 : 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
       
    }

    public function update(Request $request, $id)
	{
		try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasUpdate());
			$mensaje = __(static::$I0004_RESOURCE_UPDATED, ['parametro' => $this->servicio->getRecurso()]);
			$modelo = $this->servicio->actualiza_abonos($request, $id);
			return Respuesta::json($modelo, 200, $mensaje);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}
}
