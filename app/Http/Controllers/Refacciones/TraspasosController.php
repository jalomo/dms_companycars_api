<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioTraspasos;
use Illuminate\Http\Request;

class TraspasosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioTraspasos();
    }

    public function index()
    {
        try {
            $data = $this->servicio->getAll();
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function show($traspaso_id)
    {
        try {
            $data = $this->servicio->showById($traspaso_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->store($request->all());
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function searchByDates(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasBusqueda());
            $datos_traspaso = $this->servicio->getTraspasosByFechas($request->all());
            $array_data = $this->servicio->addProductosDetailsToArray($datos_traspaso, $request->all());
            return Respuesta::json($array_data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
