<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioVentaProducto;
use Illuminate\Http\Request;

class VentaProductoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioVentaProducto();
    }

    public function detalleVenta()
    {
        try {
            $data = $this->servicio->getDetalleVenta();
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->getDetalleVentaByFolio($folio_id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function getDetalleVentaId($id)
    {
        try {
            $data = $this->servicio->geDetalleVentaById($id);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function totalVentaByFolio($folio_id)
    {
        try {
            $data = $this->servicio->totalVentaByFolio($folio_id);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($data, 200, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $data = $this->servicio->store($request);
            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
