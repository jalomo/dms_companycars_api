<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioDevolucionPieza;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Exceptions\ParametroHttpInvalidoException;
use App\Servicios\Refacciones\ServicioDesgloseProductos;
use Illuminate\Http\Request;

class DevolucionPiezaController extends CrudController
{
    public function __construct()
    {
		$this->servicio = new ServicioDevolucionPieza();
		$this->servicioDesgloseProductos = new ServicioDesgloseProductos();
    }

    public function getByOrdenCompraId($id)
	{
		try {
			$data = $this->servicio->getPiezasDevueltasByOrdenCompraId($id);
			return Respuesta::json($data, 200);
		} catch (\Throwable $e) {
			return Respuesta::error($e);
		}
	}

	public function store(Request $request)
    {
        try {
			ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());

			$stock = $this->servicioDesgloseProductos->getStockByProducto(['producto_id' => $request->get('producto_id')]);
            if (isset($stock) && $stock) {
                if ($request->get('cantidad') > $stock->cantidad_actual) {
                    throw new ParametroHttpInvalidoException([
                        'producto' => __(self::$I0007_INSUFICIENTES_PRODUCTOS_ALMACEN, ["parametro" => $request->get('producto_id')])
                    ]);
                }
            }
            $modelo = $this->servicio->store($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
