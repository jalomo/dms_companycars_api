<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioReOrdenCompraEstatus;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class ReOrdenCompraEstatus extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioReOrdenCompraEstatus();
    }

    public function store(Request $request)
    {
        try {
            
            $insert = $this->servicio->storeReOrdenCompraEstatus($request);
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            
            return Respuesta::json($insert, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
