<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioProductoPedidos;
use Illuminate\Http\Request;

class ProductoPedidosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioProductoPedidos();
    }

    public function getpedidoproductos(Request $request)
    {
        return Respuesta::json(['data' => $this->servicio->filterQuerys($request->only(['proveedor_id','estatus_id','ma_pedido_id']))], 200);
    }

    public function store(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasGuardar());
            $modelo = $this->servicio->store($request->all());
            $mensaje = __(static::$I0003_RESOURCE_REGISTERED, ['parametro' => $this->servicio->getRecurso()]);
            return Respuesta::json($modelo, 201, $mensaje);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
