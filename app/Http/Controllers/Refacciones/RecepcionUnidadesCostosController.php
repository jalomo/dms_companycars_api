<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Models\Refacciones\ServicioRecepcionUnidadadesCostos;
use Illuminate\Http\Request;

class RecepcionUnidadesCostosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioRecepcionUnidadadesCostos();
    }
}
