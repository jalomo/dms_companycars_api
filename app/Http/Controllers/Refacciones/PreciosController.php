<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioPrecios;
use Illuminate\Http\Request;

class PreciosController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioPrecios();
    }
}
