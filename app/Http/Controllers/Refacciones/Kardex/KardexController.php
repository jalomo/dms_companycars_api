<?php

namespace App\Http\Controllers\Refacciones\Kardex;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\Kardex\ServicioKardexProducto;
use App\Servicios\Refacciones\ServicioProductos;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioVentaProducto;
use Illuminate\Http\Request;

class KardexController extends CrudController
{
    public function __construct()
    {
        $this->servicioKardex = new ServicioKardexProducto();
        $this->servicioProducto = new ServicioProductos();     
        $this->servicioVentaProducto = new ServicioVentaProducto();     
    }

    public function showProducto(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicioKardex->getReglasBusquedaProducto());

            $movKardex = $this->servicioKardex->getDataProductoKardex($request->toArray());
            $params = [
                'compra_realizada' => true,
                'producto_id' => $request->get('id')
            ];
            $data['producto'] = current($this->servicioProducto->stockProductos($params));
            $data['kardex'] = $movKardex['kardex'];
            $data['totales'] = $movKardex['totales'];

            return Respuesta::json($data, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
