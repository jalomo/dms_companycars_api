<?php

namespace App\Http\Controllers\Refacciones\PedidoSugerido;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Refacciones\ServicioPedidoSugerido;
use Illuminate\Http\Request;

class PedidoSugeridoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioPedidoSugerido();
    }

    public function sugeridoRapido()
    {
        try {
            $calculoRapido = $this->servicio->pedidosugeridorapido();
            return Respuesta::json($calculoRapido, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function sugeridoLento()
    {
        try {
            $sugeridos = $this->servicio->calcularSugeridoLento();
            return Respuesta::json($sugeridos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function sugeridoObsoleto()
    {
        try {
            $sugeridos = $this->servicio->calcularSugeridoObsoleto();
            return Respuesta::json($sugeridos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function sugeridoPotencialObsoleto()
    {
        try {
            $sugeridos = $this->servicio->calcularSugeridoPotencialObsoleto();
            return Respuesta::json($sugeridos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function sugeridoInactivo()
    {
        try {
            $sugeridos = $this->servicio->calcularSugeridoInactivo();
            return Respuesta::json($sugeridos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function sugeridoEstancado()
    {
        try {
            $sugeridos = $this->servicio->calcularSugeridoEstancado();
            return Respuesta::json($sugeridos, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function categorizaporproducto($producto_id)
    {
        $categoria = $this->servicio->categorizaporproducto($producto_id);
        return Respuesta::json($categoria, 200);
    }

    public function graficapedidosugerido()
    {
        $seccionA =  count($this->servicio->pedidosugeridorapido());
        $seccionB =  count($this->servicio->calcularSugeridoLento());

        $seccionC_obsoleto = count($this->servicio->calcularSugeridoObsoleto());
        $seccionC_potencial = count($this->servicio->calcularSugeridoPotencialObsoleto());
        $seccionC_inactivo = count($this->servicio->calcularSugeridoInactivo());
        $seccionC_estancado = count($this->servicio->calcularSugeridoEstancado());
        $seccionC_suma = $seccionC_obsoleto + $seccionC_potencial + $seccionC_inactivo + $seccionC_estancado;
        $total_items =  $seccionC_suma + $seccionA + $seccionB;

        $porcentajeA = (100 * $seccionA) / $total_items;
        $porcentajeB = (100 * $seccionB) / $total_items;
        $porcentajeC = (100 * $seccionC_suma) / $total_items;
        
        return Respuesta::json([
            'seccion_a' => $porcentajeA,
            'seccion_b' => $porcentajeB,
            'seccion_c' => $porcentajeC,
        ], 200);
    }
}
