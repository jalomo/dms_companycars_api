<?php

namespace App\Http\Controllers\Refacciones;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Refacciones\ServicioContacto;
use App\Servicios\Core\Respuestas\Respuesta;
use Illuminate\Http\Request;

class ContactoController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioContacto();
    }

    public function getContactosByClienteId($cliente_id)
    {
        try {
            return Respuesta::json($this->servicio->contactosByClienteId($cliente_id), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
