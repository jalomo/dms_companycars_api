<?php

namespace App\Http\Controllers\logistica;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Logistica\ServicioFaltantes;
use Illuminate\Http\Request;

class FaltantesController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioFaltantes();
    }
    public function getByParametros(Request $request){
        return Respuesta::json($this->servicio->getFaltantes($request->all()), 200);
    }
}
