<?php

namespace App\Http\Controllers\Logistica;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Logistica\ServicioDaniosBodyshop;
use Illuminate\Http\Request;

class DaniosBodyshopController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioDaniosBodyshop();
    }
    public function getByParametros(Request $request){
        return Respuesta::json($this->servicio->getDanios($request->all()), 200);
    }
}
