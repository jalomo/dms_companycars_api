<?php

namespace App\Http\Controllers\CuentasPorCobrar;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\CuentasPorCobrar\ServicioEstatusCuentas;

class CatEstatusCuentasController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioEstatusCuentas();
    }

}
