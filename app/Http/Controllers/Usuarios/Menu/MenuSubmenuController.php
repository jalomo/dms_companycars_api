<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioSubmenus;
use Illuminate\Http\Request;

class MenuSubmenuController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioSubmenus();
    }

    public function getsubmenusBySeccion(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasSubmenu());
            $menu = $this->servicio->getSubmenus($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
