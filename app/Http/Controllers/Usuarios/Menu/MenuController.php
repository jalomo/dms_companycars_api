<?php

namespace App\Http\Controllers\Usuarios\Menu;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Usuarios\Menu\ServicioMenu;
use Illuminate\Http\Request;

class MenuController extends CrudController
{
    public function __construct()
    {
        $this->servicio = new ServicioMenu();
    }

    public function menu(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, $this->servicio->getReglasMenu());
            $menu = $this->servicio->getmenu($request->all());
            return Respuesta::json($menu, 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

}
