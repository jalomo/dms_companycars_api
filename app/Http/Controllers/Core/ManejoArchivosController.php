<?php

namespace App\Http\Controllers\Core;


use App\Servicios\Core\ParametrosHttp\ParametrosHttpValidador;
use App\Servicios\Core\Respuestas\Respuesta;
use App\Servicios\Core\ServicioManejoArchivos;
use Illuminate\Http\Request;

class ManejoArchivosController extends CrudController
{
    public function __construct()
    {
        $this->servicioManejoArchivos = new ServicioManejoArchivos;
    }

    public function returnFileView(Request $request)
    {
        try {
            ParametrosHttpValidador::validar($request, ['url'=>'required']);
            return Respuesta::json(base64_encode($this->servicioManejoArchivos->view($request->get('url'))), 200);
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }

    public function downloadFile(Request $request)
    {
        try {
            // ParametrosHttpValidador::validar($request, ['url'=>'required']);
            // return Respuesta::json($this->servicioManejoArchivos->download($request->get('url')));
        } catch (\Throwable $e) {
            return Respuesta::error($e);
        }
    }
}
