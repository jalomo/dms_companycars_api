<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioSubcuenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubcuentaController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioSubcuenta();
    }

    public function subcuentaIdCuenta($id_cuenta){
    	if(isset($id_cuenta)){
    		$subcuentas = DB::table('subcuenta')->where('id_cuenta', $id_cuenta)->get();
    		return json_encode($subcuentas );
    	}

    }
}
