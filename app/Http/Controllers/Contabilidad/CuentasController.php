<?php

namespace App\Http\Controllers\Contabilidad;

use App\Http\Controllers\Core\CrudController;
use App\Servicios\Contabilidad\ServicioCuentas;
use Illuminate\Http\Request;

class CuentasController extends CrudController
{
    public function __construct() {
        $this->servicio = new ServicioCuentas();
    }
}
