<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'financiamientos'], function () {
    Route::post('/get-all-info', 'Financiamientos\FinanciamientosController@getAll');
    Route::put('/update-bitacoras/{id}', 'Financiamientos\FinanciamientosController@updateBitacoraCompras');
});
Route::resource('financiamientos', 'Financiamientos\FinanciamientosController');


Route::group(['prefix' => 'financiamientos/historial-estatus'], function () {
    Route::post('/get-comentarios-financiamientos', 'Financiamientos\HistorialEstatusFinanciamientosController@getComentariosFinanciamientos');
});
Route::resource('financiamientos/historial-estatus', 'Financiamientos\HistorialEstatusFinanciamientosController');

Route::resource('financiamientos/catalogos/estatus', 'Financiamientos\EstatusFinanciamientosController');
Route::resource('financiamientos/catalogos/financieras', 'Financiamientos\CatFinancierasController');
Route::resource('financiamientos/catalogos/propuestas', 'Financiamientos\CatPropuestasController');
Route::resource('financiamientos/catalogos/perfiles', 'Financiamientos\CatPerfilesFinanciamientosController');
Route::resource('financiamientos/catalogos/comisiones', 'Financiamientos\catComisionesController');
Route::resource('financiamientos/catalogos/tipo-seguros', 'Financiamientos\catTipoSegurosController');
Route::resource('financiamientos/catalogos/companias-seguros', 'Financiamientos\catCompaniasSegurosController');
Route::resource('financiamientos/catalogos/udi', 'Financiamientos\catUdiController');
Route::resource('financiamientos/catalogos/estatus-piso', 'Financiamientos\catEstatusPlanPisoController');
Route::resource('financiamientos/catalogos/uso-seguros', 'Financiamientos\catUsoSeguroController');
